let hoy = () => {
  let hoy = new Date();

  let año = hoy.getFullYear();
  let mes =
    hoy.getMonth() + 1 > 9 ? hoy.getMonth() + 1 : `0${hoy.getMonth() + 1}`;
  let dia = hoy.getDate() > 9 ? hoy.getDate() : `0${hoy.getDate()}`;

  const fecha = `${año}-${mes}-${dia}`;
  return fecha;
};

export default hoy;
