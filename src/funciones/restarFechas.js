let restarFechas = (desde, hasta) => {
  var fechaInicio = new Date(desde).getTime();
  var fechaFin = new Date(hasta).getTime();

  var dias = (fechaFin - fechaInicio) / (1000 * 60 * 60 * 24);

  return dias;
};

export default restarFechas;
