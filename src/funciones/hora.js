let hora = () => {
  let hoy = new Date();

  let h = hoy.getHours();
  let m = hoy.getMinutes();

  if(h < 10) {
    h = `0${h}`;
  }

  if(m < 10) {
    m = `0${m}`;
  }

  const hora = `${h}:${m}`;
  return hora;
};

export default hora;
