import React from "react";
import "./App.css";
import Contenido from "./Componentes/Contenido";

function App() {
  return (
    <div className="App">
      <Contenido />
    </div>
  );
}

export default App;
