import React, { Component, Fragment } from "react";
import {
  Table,
  Button,
  Form,
  Container,
  Row,
  Col,
  Modal,
  ToggleButton,
  ToggleButtonGroup,
  Spinner,
} from "react-bootstrap";
import { db } from "../firebaseConfig";

class ListadoComprasProductos extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cargando: false,
      productos: [],
      desde: "",
      hasta: "",
      show: false,
      estadoCuenta: "",
      totalAdeudado: "",
      tipoListado: "todos",
      cantidad: 0,
      precioProm: 0,
      importe: 0,
      percepcion: 0,
    };
  }

  traerDocumentos = () => {
    const { desde, hasta, tipoListado } = this.state;
    if (desde && hasta) {
      this.setState({ cargando: true }, async () => {
        let snapProductosTodos = await db
          .collection("productos")
          .orderBy("orden")
          .orderBy("producto")
          .get();
        var productos = [];
        snapProductosTodos.forEach((doc) => {
          productos[doc.id] = doc.data();
          productos[doc.id].iva = parseFloat(productos[doc.id].iva);
          productos[doc.id].cantidad = 0;
          productos[doc.id].importe = 0;
          productos[doc.id].precioProm = 0;
        });

        if (tipoListado === "todos") {
          let snapCompras = await db
            .collection("comprasCabezera")
            .where("fecha", ">=", desde)
            .where("fecha", "<=", hasta)
            .where("compraPago", "==", "compra")
            .orderBy("fecha")
            .orderBy("nDocumento")
            .get();
          var percepcion = 0;
          snapCompras.forEach((doc) => {
            if (doc.data().percepcion) {
              percepcion = percepcion + parseFloat(doc.data().percepcion);
            }
          });
          productos["HUelMYmTE3yRPDywipkq"].importe = percepcion;

          let snapProdComprados = await db
            .collection("comprasProductos")
            .where("fecha", ">=", desde)
            .where("fecha", "<=", hasta)
            .where("compraPago", "==", "compra")
            .orderBy("fecha")
            .orderBy("nDocumento")
            .get();
          let prodComprados = [];
          let cantidad = 0;
          let importe = 0;
          snapProdComprados.forEach((doc) => {
            prodComprados[doc.id] = doc.data();
            let keyProducto = doc.data().idProducto;
            cantidad = cantidad + parseFloat(prodComprados[doc.id].cantidad);
            importe =
              importe +
              parseFloat(prodComprados[doc.id].importe) *
                `1.${productos[keyProducto].iva}`;
          });
          importe = importe + percepcion;

          Object.keys(productos).forEach((id) => {
            Object.keys(prodComprados).forEach((key) => {
              if (id === prodComprados[key].idProducto) {
                productos[id].cantidad =
                  productos[id].cantidad +
                  parseFloat(prodComprados[key].cantidad);
                productos[id].importe =
                  productos[id].importe +
                  parseFloat(prodComprados[key].importe) *
                    `1.${productos[id].iva}`;
              }
            });
          });

          Object.keys(productos).forEach((id) => {
            if (productos[id].cantidad === 0) {
              delete productos[id];
            } else {
              productos[id].precioProm =
                productos[id].importe / productos[id].cantidad;
            }
          });

          let precioProm = importe / cantidad;

          this.setState({
            productos,
            cantidad,
            precioProm,
            importe,
            percepcion,
            cargando: false,
          });
        }

        if (tipoListado === "a") {
          let snapCompras = await db
            .collection("comprasProductos")
            .where("fecha", ">=", desde)
            .where("fecha", "<=", hasta)
            .where("compraPago", "==", "compra")
            .where("aOb", "==", true)
            .orderBy("fecha")
            .orderBy("nDocumento")
            .get();
          let percepcion = 0;
          snapCompras.forEach((doc) => {
            if (doc.data().percepcion) {
              percepcion = percepcion + parseFloat(doc.data().percepcion);
            }
          });
          productos["HUelMYmTE3yRPDywipkq"].importe = percepcion;

          let snapProdComprados = await db
            .collection("comprasProductos")
            .where("fecha", ">=", desde)
            .where("fecha", "<=", hasta)
            .where("compraPago", "==", "compra")
            .where("aOb", "==", true)
            .orderBy("fecha")
            .orderBy("nDocumento")
            .get();
          let prodComprados = [];
          let cantidad = 0;
          let importe = 0;
          snapProdComprados.forEach((doc) => {
            prodComprados[doc.id] = doc.data();
            let keyProducto = doc.data().idProducto;
            cantidad = cantidad + parseFloat(prodComprados[doc.id].cantidad);
            importe =
              importe +
              parseFloat(prodComprados[doc.id].importe) *
                `1.${productos[keyProducto].iva}`;
          });
          importe = importe + percepcion;

          Object.keys(productos).forEach((id) => {
            Object.keys(prodComprados).forEach((key) => {
              if (id === prodComprados[key].idProducto) {
                productos[id].cantidad =
                  productos[id].cantidad +
                  parseFloat(prodComprados[key].cantidad);
                productos[id].importe =
                  productos[id].importe +
                  parseFloat(prodComprados[key].importe) *
                    `1.${productos[id].iva}`;
              }
            });
          });

          Object.keys(productos).forEach((id) => {
            if (productos[id].cantidad === 0) {
              delete productos[id];
            } else {
              productos[id].precioProm =
                productos[id].importe / productos[id].cantidad;
            }
          });

          let precioProm = importe / cantidad;

          this.setState({
            productos,
            cantidad,
            precioProm,
            importe,
            percepcion,
            cargando: false,
          });
        }

        if (tipoListado === "b") {
          let snapCompras = await db
            .collection("comprasProductos")
            .where("fecha", ">=", desde)
            .where("fecha", "<=", hasta)
            .where("compraPago", "==", "compra")
            .where("aOb", "==", false)
            .orderBy("fecha")
            .orderBy("nDocumento")
            .get();
          let percepcion = 0;
          snapCompras.forEach((doc) => {
            if (doc.data().percepcion) {
              percepcion = percepcion + parseFloat(doc.data().percepcion);
            }
          });
          productos["HUelMYmTE3yRPDywipkq"].importe = percepcion;

          let snapProdComprados = await db
            .collection("comprasProductos")
            .where("fecha", ">=", desde)
            .where("fecha", "<=", hasta)
            .where("compraPago", "==", "compra")
            .where("aOb", "==", false)
            .orderBy("fecha")
            .orderBy("nDocumento")
            .get();
          let prodComprados = [];
          let cantidad = 0;
          let importe = 0;
          snapProdComprados.forEach((doc) => {
            prodComprados[doc.id] = doc.data();
            let keyProducto = doc.data().idProducto;
            cantidad = cantidad + parseFloat(prodComprados[doc.id].cantidad);
            importe =
              importe +
              parseFloat(prodComprados[doc.id].importe) *
                `1.${productos[keyProducto].iva}`;
          });
          importe = importe + percepcion;

          Object.keys(productos).forEach((id) => {
            Object.keys(prodComprados).forEach((key) => {
              if (id === prodComprados[key].idProducto) {
                productos[id].cantidad =
                  productos[id].cantidad +
                  parseFloat(prodComprados[key].cantidad);
                productos[id].importe =
                  productos[id].importe +
                  parseFloat(prodComprados[key].importe) *
                    `1.${productos[id].iva}`;
              }
            });
          });

          Object.keys(productos).forEach((id) => {
            if (productos[id].cantidad === 0) {
              delete productos[id];
            } else {
              productos[id].precioProm =
                productos[id].importe / productos[id].cantidad;
            }
          });

          let precioProm = importe / cantidad;

          this.setState({
            productos,
            cantidad,
            precioProm,
            importe,
            percepcion,
            cargando: false,
          });
        }
      });
    }
  };

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleClose = () => {
    this.setState({ show: false });
  };

  handleShow = async (id, clienteDoc, estadoCuenta, totalDoc) => {
    if (estadoCuenta) {
      let snapCliente = await db.collection("clientes").doc(clienteDoc).get();
      let totalAdeudado = snapCliente.data().totalAdeudado;
      if (totalAdeudado) {
        totalAdeudado = parseFloat(totalAdeudado);
      } else {
        totalAdeudado = 0;
      }
      this.setState({
        show: true,
        id: id,
        clienteDoc,
        totalDoc,
        estadoCuenta,
        totalAdeudado,
      });
    } else {
      this.setState({ show: true, id: id, clienteDoc, totalDoc, estadoCuenta });
    }
  };

  borrar = () => {
    this.handleClose();
  };

  detalle = (id, documento) => {
    let claseDoc = "";
    if (id === this.state.idFactura) {
      this.setState({ idFactura: "", claseDoc });
    } else {
      if (documento === "Factura Contado") {
        claseDoc = "table-danger";
      }
      if (documento === "Boleta Contado") {
        claseDoc = "table-warning";
      }
      if (documento === "Factura Crédito") {
        claseDoc = "table-active";
      }
      if (documento === "Boleta Crédito") {
        claseDoc = "table-info";
      }
      this.setState({ idFactura: id, claseDoc });
    }
  };

  handleTipoListado = (val) => {
    this.setState({ tipoListado: val }, () => {
      this.traerDocumentos();
    });
  };

  render() {
    const { productos, cantidad, precioProm, importe, percepcion } = this.state;
    return (
      <div>
        <Container>
          <Row>
            <Col>
              <h1 style={{ padding: "15px" }}>Listado Compras Productos</h1>
            </Col>
            <Col>
              <ToggleButtonGroup
                style={{
                  height: "42px",
                  marginBottom: "25px",
                  marginTop: "25px",
                }}
                type="radio"
                name="tipoListado"
                value={this.state.tipoListado}
                onChange={this.handleTipoListado}
              >
                <ToggleButton
                  variant="outline-primary"
                  name="radio"
                  value="todos"
                >
                  Todos
                </ToggleButton>
                <ToggleButton variant="outline-success" name="radio" value="a">
                  A
                </ToggleButton>
                <ToggleButton variant="outline-danger" name="radio" value="b">
                  B
                </ToggleButton>
              </ToggleButtonGroup>
            </Col>
          </Row>
          <Row>
            <Col>
              <Form.Group>
                <Form.Label>Desde</Form.Label>
                <input
                  name="desde"
                  className="form-control text-center"
                  type="date"
                  onChange={(e) => this.handleChange(e)}
                  value={this.state.desde}
                />
              </Form.Group>
            </Col>
            <Col>
              <Form.Group>
                <Form.Label>Hasta</Form.Label>
                <input
                  name="hasta"
                  className="form-control text-center"
                  type="date"
                  onChange={(e) => this.handleChange(e)}
                  value={this.state.hasta}
                />
              </Form.Group>
            </Col>
          </Row>
          <Row>
            <Col>
              <Button
                variant="success"
                onClick={() => {
                  this.traerDocumentos();
                }}
              >
                Buscar
              </Button>
            </Col>
          </Row>
        </Container>
        {this.state.cargando ? (
          <div
            style={{
              width: "100%",
              height: "200px",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Spinner animation="grow" variant="primary" />
          </div>
        ) : (
          <div style={{ width: "100%", overflow: "scroll", marginTop: "20px" }}>
            <Table striped bordered condensed hover responsive>
              <thead>
                <tr>
                  <th>Producto</th>
                  <th>Cantidad</th>
                  <th>Precio prom</th>
                  <th>Importe</th>
                </tr>
              </thead>
              <tbody>
                {Object.keys(productos).length > 0 ? (
                  Object.keys(productos).map((id) => {
                    return (
                      <Fragment>
                        <tr
                          key={id}
                          className={
                            this.state.idFactura === id
                              ? this.state.claseDoc
                              : ""
                          }
                        >
                          <td>{productos[id].producto}</td>
                          <td>
                            {productos[id].cantidad.toLocaleString("de-DE")}
                          </td>
                          <td>
                            {productos[id].precioProm.toLocaleString("de-DE")}
                          </td>
                          <td>
                            {productos[id].importe.toLocaleString("de-DE")}
                          </td>
                        </tr>
                      </Fragment>
                    );
                  })
                ) : (
                  <tr>
                    <td colSpan="8">
                      <p>NO se encontraron resultados</p>
                    </td>
                  </tr>
                )}
              </tbody>
            </Table>
          </div>
        )}
        {Object.keys(productos).length > 0 ? (
          <div>
            <Table>
              <tbody>
                <tr>
                  <td />
                  <td />
                  <td />
                  <td />
                  <td colSpan="2">
                    <strong>Cantidad KG</strong>
                  </td>
                  <td>
                    <strong>{cantidad.toLocaleString("de-DE")}</strong>
                  </td>
                  <td />
                </tr>
              </tbody>
              <tbody>
                <tr>
                  <td />
                  <td />
                  <td />
                  <td />
                  <td colSpan="2">
                    <strong>Precio Prom.</strong>
                  </td>
                  <td>
                    <strong>{precioProm.toLocaleString("de-DE")}</strong>
                  </td>
                  <td />
                </tr>
              </tbody>
              <tbody>
                <tr>
                  <td />
                  <td />
                  <td />
                  <td />
                  <td colSpan="2">
                    <strong>Percepción</strong>
                  </td>
                  <td>
                    <strong>{percepcion.toLocaleString("de-DE")}</strong>
                  </td>
                  <td />
                </tr>
              </tbody>
              <tbody>
                <tr>
                  <td />
                  <td />
                  <td />
                  <td />
                  <td colSpan="2">
                    <strong>Importe Total</strong>
                  </td>
                  <td>
                    <strong>{importe.toLocaleString("de-DE")}</strong>
                  </td>
                  <td />
                </tr>
              </tbody>
            </Table>
          </div>
        ) : null}
        <Modal show={this.state.show} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>ATENCIÓN!!!</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            Estás seguro que quieres ELIMINAR este documento?!
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleClose}>
              Cancelar
            </Button>
            <Button
              variant="danger"
              onClick={() => {
                this.borrar();
              }}
            >
              ELIMINAR
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

export default ListadoComprasProductos;
