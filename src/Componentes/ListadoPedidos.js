import React, { Component } from "react";
import {
  Button,
  Form,
  Container,
  Row,
  Col,
  ListGroup,
  Modal,
  Spinner,
} from "react-bootstrap";
import { db } from "../firebaseConfig";
import jsPDF from "jspdf";
import { FaFilePdf, FaCheck } from "react-icons/fa";
import { MdClose, MdDelete } from "react-icons/md";
import hoy from "../funciones/hoy";

class ListadoPedidos extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cargando: false,
      hoy: "",
      pedidos: [],
      productos: [],
      fecha: "",
      pendienteEntregado: false,
      pedidosTodos: false,
      totalPedidos: 0,
      cargandoPedidos: false,
      cargandoProductos: false,
      show: false,
      reparto: "",
    };
  }

  componentDidMount() {
    this.calcularHoy();
    this.traerReparto();
  }

  traerReparto = () => {
    let reparto = this.props.vendedor.reparto;
    this.setState({ reparto });
  };

  calcularHoy = () => {
    let fecha = hoy();
    let today = hoy;
    this.setState({ fecha, hoy: today });
  };

  traerPedidos = () => {
    const { fecha, pendienteEntregado, reparto } = this.state;

    let ref = "";
    if (pendienteEntregado) {
      ref = ">=";
    } else {
      ref = "<=";
    }
    if (fecha) {
      this.setState({ cargando: true }, async () => {
        if (reparto === "") {
          db.collection("pedidosCabezera")
            .where("fechaEntrega", ref, fecha)
            .where("pendienteEntregado", "==", pendienteEntregado)
            .orderBy("fechaEntrega")
            .orderBy("clienteNombre")
            .onSnapshot((snap) => {
              var totalPedidos = 0;
              let pedidos = [];
              snap.forEach((doc) => {
                pedidos[doc.id] = doc.data();
                totalPedidos = totalPedidos + 1;
              });
              this.setState({
                pedidos,
                totalPedidos,
                cargando: false,
              });
            });
        } else {
          db.collection("pedidosCabezera")
            .where("fechaEntrega", ref, fecha)
            .where("pendienteEntregado", "==", pendienteEntregado)
            .where("zona", "==", reparto)
            .orderBy("fechaEntrega")
            .orderBy("clienteNombre")
            .onSnapshot((snap) => {
              var totalPedidos = 0;
              let pedidos = [];
              snap.forEach((doc) => {
                pedidos[doc.id] = doc.data();
                totalPedidos = totalPedidos + 1;
              });
              this.setState({
                pedidos,
                totalPedidos,
                cargando: false,
              });
            });
        }
      });
    }
  };

  handleChange = (e) => {
    if (e.target.name === "reparto") {
      this.setState({ [e.target.name]: e.target.value }, () => {
        this.traerPedidos();
      });
    } else {
      this.setState({ [e.target.name]: e.target.value });
    }
  };

  traerPedidosProductos = (id) => {
    let { banderaPedido } = this.state;
    if (banderaPedido === id) {
      this.setState({ banderaPedido: "" });
    } else {
      this.setState(
        { banderaPedido: id, cargandoProductos: true },
        async () => {
          let snapProductos = await db
            .collection("pedidosProductos")
            .where("idCabezera", "==", id)
            .get();
          let productos = [];
          snapProductos.forEach((doc) => {
            productos[doc.id] = doc.data();
          });
          this.setState({ productos, cargandoProductos: false });
        }
      );
    }
  };

  handleChangeCheck = (e) => {
    if (e.target.id === "pendienteEntregado") {
      let pendienteEntregado = e.target.checked;
      this.setState({ pendienteEntregado }, () => {
        this.traerPedidos();
      });
    }
  };

  imprimir = (pedidos, productosFinal) => {
    let doc = new jsPDF("p", "mm", [58, 180]);
    let {
      fecha,
      documento,
      nDocumento,
      vendedor,
      nombreLugar,
      fechaEntrega,
      total,
      iva,
      subtotal,
      clienteNombre,
      rut,
      nCarpeta,
    } = pedidos;

    const { empresa, telefono } = this.props.empresa;

    doc.setFontSize(5);
    doc.text(6, 2, `Distri ${empresa}`);
    doc.setFontSize(2);
    doc.text(10, 3.5, `Tel: ${telefono}`);

    doc.setFontSize(2.5);
    doc.text(0.3, 5, `Fecha: ${fecha}`);
    doc.text(0.3, 6, `Documento: ${documento}`);
    doc.text(15, 6, `Nº: ${nDocumento}`);
    doc.text(0.3, 8, `Cliente: ${clienteNombre}`);
    if (documento === "Factura Contado" || documento === "Factura Crédito") {
      doc.text(0.3, 9, `Rut: ${rut}`);
      doc.text(12.5, 9, `Nº Carpeta: ${nCarpeta}`);
    }
    doc.text(0.3, 10, `Vendedor: ${vendedor}`);
    doc.text(0.3, 12, `Aprontar en: ${nombreLugar}`);
    doc.text(0.3, 14, `Fecha Entrega: ${fechaEntrega}`);

    doc.text(8, 17, `PEDIDO`);

    let co = 19;

    doc.setFontSize(2.3);
    doc.text(0.1, co, `Cant`);
    doc.text(6, co, `Producto`);
    doc.text(14.5, co, `Precio`);
    doc.text(17.4, co, `Importe`);
    co = co + 0.2;
    doc.setLineWidth(0.08);
    doc.line(0.3, co, 20, co);
    doc.setFontSize(2.3);
    let obs = false;
    Object.keys(productosFinal).forEach((id) => {
      co = co + 1.3;
      doc.text(0.25, co, `${productosFinal[id].cantidad}`);
      if (productosFinal[id].observaciones) {
        obs = true;
        doc.text(2.9, co, `${productosFinal[id].producto}*`);
      } else {
        doc.text(2.9, co, `${productosFinal[id].producto}`);
      }
      doc.text(
        14.9,
        co,
        `${productosFinal[id].precio.toLocaleString("de-DE")}`
      );
      doc.text(
        17.8,
        co,
        `${productosFinal[id].importe.toLocaleString("de-DE")}`
      );
    });

    doc.setFontSize(2.5);
    co = co + 3;
    if (documento === "Factura Contado" || documento === "Factura Crédito") {
      doc.text(11.5, co, `Subtotal: `);
      doc.text(15.5, co, subtotal.toLocaleString("de-DE"));
      co = co + 1.5;
      doc.text(13, co, `IVA: `);
      doc.text(15.5, co, iva.toLocaleString("de-DE"));
    }

    doc.setFontSize(3);
    co = co + 1.5;
    doc.text(11.5, co, `TOTAL: `);
    doc.text(15.5, co, total.toLocaleString("de-DE"));

    if (obs) {
      co = co + 2;
      doc.text(2.9, co, `*Observaciones: `);
      co = co + 1.5;
      doc.setFontSize(2);
      Object.keys(productosFinal).forEach((id) => {
        if (productosFinal[id].observaciones) {
          doc.text(1, co, `- ${productosFinal[id].producto}:`);
          co = co + 1;
          doc.text(0.5, co, productosFinal[id].observaciones);
          co = co + 1.3;
        }
      });
    }

    doc.save(`${clienteNombre}-${nDocumento}.pdf`);
  };

  entregado = (idPedido) => {
    let { productos, pedidos } = this.state;
    let cabezera = { ...pedidos[idPedido] };
    console.log(cabezera);
    cabezera.pendienteEntregado = true;
    this.setState({ banderaPedido: "" }, () => {
      db.collection("pedidosCabezera")
        .doc(idPedido)
        .update({
          pendienteEntregado: true,
        })
        .then(() => {
          console.log("pedido entregado");
          Object.keys(productos).forEach((id) => {
            db.collection("pedidosProductos")
              .doc(id)
              .update({
                cabezera: { ...cabezera },
              })
              .then(() => console.log("producto entregado"))
              .catch((error) => console.log(error));
          });
        })
        .catch((error) => console.log(error));
    });
  };

  pendiente = (idPedido) => {
    let { productos, pedidos } = this.state;
    let cabezera = { ...pedidos[idPedido] };
    cabezera.pendienteEntregado = false;
    this.setState({ banderaPedido: "" }, () => {
      db.collection("pedidosCabezera")
        .doc(idPedido)
        .update({
          pendienteEntregado: false,
        })
        .then(() => {
          console.log("pedido pendiente");
          Object.keys(productos).forEach((id) => {
            db.collection("pedidosProductos")
              .doc(id)
              .update({
                cabezera: { ...cabezera },
              })
              .then(() => console.log("producto pendiente"))
              .catch((error) => console.log(error));
          });
        })
        .catch((error) => console.log(error));
    });
  };

  handleClose = () => {
    this.setState({ show: false });
  };

  handleShow = (idBorrar) => {
    this.setState({
      show: true,
      idBorrar,
    });
  };

  borrar = async () => {
    let { idBorrar } = this.state;
    let snapProductos = await db
      .collection("pedidosProductos")
      .where("idCabezera", "==", idBorrar)
      .get();
    snapProductos.forEach((doc) => {
      let key = doc.id;
      db.collection("pedidosProductos")
        .doc(key)
        .delete()
        .then(() => console.log("producto pedido borrado"))
        .catch((error) => console.log(error));
    });
    db.collection("pedidosCabezera")
      .doc(idBorrar)
      .delete()
      .then(() => {
        console.log("pedido borrado");
        this.setState({ show: false, idBorrar: "" });
      })
      .catch((error) => console.log(error));
  };

  render() {
    const {
      pedidos,
      productos,
      totalPedidos,
      pendienteEntregado,
      hoy,
    } = this.state;
    return (
      <div>
        <Container>
          <Row>
            <Col>
              <h1 style={{ padding: "15px" }}>Listado Pedidos</h1>
            </Col>
          </Row>
          <Row>
            <Col xs={6}>
              <Form.Group>
                <Form.Label>Fecha</Form.Label>
                <input
                  name="fecha"
                  className="form-control text-center"
                  type="date"
                  onChange={(e) => this.handleChange(e)}
                  value={this.state.fecha}
                />
              </Form.Group>
            </Col>
            <Col xs={6} style={{ marginTop: "35px", textAlign: "left" }}>
              <Form onChange={(e) => this.handleChangeCheck(e)}>
                <Form.Check
                  type="switch"
                  id="pendienteEntregado"
                  label={pendienteEntregado ? "Entregados" : "Pendientes"}
                  checked={pendienteEntregado}
                />
              </Form>
            </Col>
          </Row>
          <Row>
            <Col>
              <Form.Group>
                <Form.Label>Reparto</Form.Label>
                <select
                  className="form-control"
                  onChange={(e) => this.handleChange(e)}
                  name="reparto"
                  value={this.state.reparto}
                  aria-label="Small"
                >
                  <option value="">Todos...</option>
                  <option value="Montevideo">Montevideo</option>
                  <option value="La Costa">La Costa</option>
                </select>
              </Form.Group>
            </Col>
          </Row>
          <Row style={{ marginTop: "35px" }}>
            <Col>
              <Button
                variant="success"
                onClick={() => {
                  this.traerPedidos();
                }}
              >
                Buscar
              </Button>
            </Col>
          </Row>
          {this.state.cargando ? (
            <Row>
              <div
                style={{
                  width: "100%",
                  height: "200px",
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <Spinner animation="grow" variant="primary" />
              </div>
            </Row>
          ) : (
            <div>
              <Row style={{ marginTop: "20px", marginBottom: "20px" }}>
                <Col xs={3}>
                  <strong>Fecha Entrega</strong>
                </Col>
                <Col xs={4}>
                  <strong>Cliente</strong>
                </Col>
                <Col xs={3}>
                  <strong>Dirección</strong>
                </Col>
              </Row>

              <ListGroup>
                {Object.keys(pedidos).map((id) => {
                  return (
                    <ListGroup.Item
                      action
                      variant={
                        pedidos[id].pendienteEntregado ? "success" : "danger"
                      }
                      style={pedidos[id].obs ? { color: "red" } : null}
                    >
                      <Row onClick={() => this.traerPedidosProductos(id)}>
                        <Col
                          xs={3}
                          style={
                            new Date(pedidos[id].fechaEntrega).getTime() <
                            new Date(hoy).getTime()
                              ? {
                                  textAlign: "left",
                                  color: "red",
                                  fontWeight: "bold",
                                }
                              : { textAlign: "left" }
                          }
                        >
                          {pedidos[id].fechaEntrega}
                        </Col>
                        {pedidos[id].obs ? (
                          <Col xs={4} style={{ textAlign: "left" }}>
                            {`*${pedidos[id].clienteNombre}`}
                          </Col>
                        ) : (
                          <Col xs={4} style={{ textAlign: "left" }}>
                            {pedidos[id].clienteNombre}
                          </Col>
                        )}
                        <Col xs={3} style={{ textAlign: "right" }}>
                          <strong>{pedidos[id].direccion}</strong>
                        </Col>
                        {this.props.vendedor.permisosVendedor ? (
                          <Col xs={2}>
                            <MdDelete
                              size="1.5em"
                              color="red"
                              onClick={() => this.handleShow(id)}
                            />
                          </Col>
                        ) : null}
                      </Row>
                      {this.state.banderaPedido === id ? (
                        this.state.cargandoProductos ? (
                          <div>
                            <Spinner animation="border" variant="warning" />
                          </div>
                        ) : (
                          <ListGroup variant="flush">
                            <ListGroup.Item
                              variant={
                                pedidos[id].pendienteEntregado
                                  ? "success"
                                  : "danger"
                              }
                            >
                              <Row>
                                <Col xs={4}>
                                  <Button
                                    variant="outline-dark"
                                    onClick={() =>
                                      this.imprimir(pedidos[id], productos)
                                    }
                                  >
                                    <FaFilePdf size="1.5em" />
                                  </Button>
                                </Col>
                                <Col xs={8}>
                                  {`Tel: ${pedidos[id].telefono}`}
                                </Col>
                              </Row>
                            </ListGroup.Item>
                            {Object.keys(productos).map((key) => {
                              return (
                                <ListGroup.Item
                                  key={key}
                                  variant={
                                    pedidos[id].pendienteEntregado
                                      ? "success"
                                      : "danger"
                                  }
                                >
                                  <Row>
                                    <Col xs={2}>{productos[key].cantidad}</Col>
                                    {productos[key].observaciones ? (
                                      <Col
                                        xs={4}
                                      >{`${productos[key].producto}*`}</Col>
                                    ) : (
                                      <Col xs={4}>
                                        {productos[key].producto}
                                      </Col>
                                    )}
                                    <Col xs={2}>
                                      {`$${productos[key].precio.toLocaleString(
                                        "de-DE"
                                      )}`}
                                    </Col>
                                    <Col xs={4}>
                                      {`$${productos[
                                        key
                                      ].importe.toLocaleString("de-DE")}`}
                                    </Col>
                                  </Row>
                                </ListGroup.Item>
                              );
                            })}
                            {pedidos[id].obs ? (
                              <ListGroup.Item
                                variant={
                                  pedidos[id].pendienteEntregado
                                    ? "success"
                                    : "danger"
                                }
                              >
                                <Row>
                                  <Col>
                                    <strong>Observaciones:</strong>
                                  </Col>
                                </Row>
                                <Row>
                                  {Object.keys(productos).map((key) => {
                                    return productos[key].observaciones ? (
                                      <Col>
                                        {`- ${productos[key].producto}: ${productos[key].observaciones}`}
                                      </Col>
                                    ) : null;
                                  })}
                                </Row>
                              </ListGroup.Item>
                            ) : null}
                            <ListGroup.Item
                              variant={
                                pedidos[id].pendienteEntregado
                                  ? "success"
                                  : "danger"
                              }
                            >
                              <Row>
                                <Col xs={4}>
                                  {pedidos[id].pendienteEntregado ? (
                                    <MdClose
                                      size="1.8em"
                                      color="red"
                                      onClick={() => this.pendiente(id)}
                                    />
                                  ) : (
                                    <FaCheck
                                      size="1.8em"
                                      color="green"
                                      onClick={() => this.entregado(id)}
                                    />
                                  )}
                                </Col>
                                <Col xs={8}>
                                  <strong>{`Total: $${pedidos[
                                    id
                                  ].total.toLocaleString("de-DE")}`}</strong>
                                </Col>
                              </Row>
                            </ListGroup.Item>
                          </ListGroup>
                        )
                      ) : null}
                    </ListGroup.Item>
                  );
                })}
                <ListGroup.Item
                  variant={pendienteEntregado ? "success" : "danger"}
                >
                  <Row>
                    <Col style={{ textAlign: "right" }}>
                      <strong>
                        Cantidad de Pedidos:{" "}
                        {totalPedidos.toLocaleString("de-DE")}
                      </strong>
                    </Col>
                  </Row>
                </ListGroup.Item>
              </ListGroup>
            </div>
          )}
        </Container>

        <Modal show={this.state.show} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>ATENCIÓN!!!</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            Estás seguro que quieres ELIMINAR este documento?!
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleClose}>
              Cancelar
            </Button>
            <Button
              variant="danger"
              onClick={() => {
                this.borrar();
              }}
            >
              ELIMINAR
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

export default ListadoPedidos;
