import React, { Component } from "react";
import { fire, db } from "../firebaseConfig";
import { Spinner } from "react-bootstrap";
import Cabezera from "./Cabezera";
import Login from "./Login";

class Contenido extends Component {
  constructor(props) {
    super(props);
    this.state = {
      usuario: null,
      cargando: true,
      empresa: {},
    };
  }

  componentDidMount() {
    this.autenticador();
    this.empresa();
  }

  empresa = async () => {
    let snapEmpresa = await db.collection("empresa").doc("info").get();
    let empresa = snapEmpresa.data();
    this.setState({ empresa });
  };

  desloguear = () => {
    this.setState({ usuario: null });
  };

  autenticador = () => {
    fire.auth().onAuthStateChanged(async (user) => {
      if (user) {
        let email = user.email;
        let snapUsuario = await db
          .collection("vendedores")
          .where("email", "==", email)
          .get();
        let usuario = {};
        let idUsuario = "";
        snapUsuario.forEach((doc) => {
          if (email === doc.data().email) {
            idUsuario = doc.id;
            usuario = doc.data();
          }
        });
        this.setState({ usuario, idUsuario, cargando: false });
      } else {
        this.setState({ usuario: null, cargando: false });
      }
    });
  };

  render() {
    const { usuario, idUsuario, cargando, empresa } = this.state;
    return cargando ? (
      <div
        style={{
          width: "100%",
          height: "200px",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Spinner animation="grow" variant="primary" />
      </div>
    ) : (
      <div style={{ backgroundColor: 'lightgreen' }}>
        {usuario ? (
          <Cabezera
            usuario={usuario}
            idUsuario={idUsuario}
            desloguear={this.desloguear}
            empresa={empresa}
          />
        ) : (
          <Login />
        )}
      </div>
    );
  }
}

export default Contenido;
