import React, { Component, Fragment } from "react";
import {
  Table,
  Button,
  Spinner,
  Form,
  Container,
  Row,
  Col,
  Tab,
  Nav,
  Modal,
  ListGroup,
} from "react-bootstrap";
import { db } from "../firebaseConfig";
import Detalle from "./Detalle";

class Arqueo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cargando: true,
      documentos: [],
      productos: [],
      vendedores: [],
      fecha: "",
      vendedor: {},
      idVendedor: "",
      vendedorNombre: "",
      banderaVendedor: false,
      show: false,
      id: "",
      total: 0,
      efectivo: 0,
      cheque: 0,
      transferencia: 0,
      otros: 0,
      credito: 0,
      idFactura: "",
      claseDoc: "",
      clienteDoc: "",
      totalDoc: "",
      estadoCuenta: "",
      deudaActual: "",
    };
  }

  componentDidMount() {
    this.traerVendedores();
    this.traerVendedor();
    this.hoy();
  }

  hoy = () => {
    let hoy = new Date();

    let año = hoy.getFullYear();
    let mes =
      hoy.getMonth() + 1 > 9 ? hoy.getMonth() + 1 : `0${hoy.getMonth() + 1}`;
    let dia = hoy.getDate() > 9 ? hoy.getDate() : `0${hoy.getDate()}`;

    const fecha = `${año}-${mes}-${dia}`;
    this.setState({ fecha });
  };

  banderaVendedor = () => {
    let { permisosVendedor } = this.props.vendedor;
    if (permisosVendedor) {
      let { banderaVendedor } = this.state;
      banderaVendedor = !banderaVendedor;
      this.setState({ banderaVendedor, idVendedor: "", vendedor: "" });
    }
  };

  traerVendedores = async () => {
    let snapVendedores = await db.collection("vendedores").get();
    let vendedores = [];
    snapVendedores.forEach((doc) => {
      vendedores[doc.id] = doc.data();
    });
    this.setState({ vendedores, cargando: false });
  };

  traerVendedor = () => {
    let idVendedor = this.props.idVendedor;
    let vendedor = this.props.vendedor;
    let vendedorNombre = this.props.vendedor.vendedor;
    this.setState({ idVendedor, vendedor, vendedorNombre });
  };

  traerProductos = async () => {
    let snapProductosTodos = await db
      .collection("productos")
      .orderBy("orden")
      .orderBy("producto")
      .get();
    let productos = [];
    snapProductosTodos.forEach((doc) => {
      productos[doc.id] = doc.data();
      productos[doc.id].cantidad = 0;
      productos[doc.id].cantidadSalida = 0;
      productos[doc.id].cantidadEntrada = 0;
      productos[doc.id].cantidadDiferencia = 0;
    });

    return productos;
  };

  traerProductosVentas = async () => {
    const { fecha, idVendedor } = this.state;
    let snapProductos = await db
      .collection("ventasProductos")
      .where("fecha", "==", fecha)
      .where("idVendedor", "==", idVendedor)
      .get();
    let productosVentas = [];
    snapProductos.forEach((doc) => {
      productosVentas[doc.id] = doc.data();
    });

    return productosVentas;
  };

  traerProductosSalida = async () => {
    const { fecha, idVendedor } = this.state;
    let snapProductos = await db
      .collection("salidasEntradas")
      .where("fecha", "==", fecha)
      .where("idVendedor", "==", idVendedor)
      .where("tipoDe", "==", "salida")
      .get();
    let productosSalida = [];
    snapProductos.forEach((doc) => {
      productosSalida[doc.id] = doc.data();
    });

    return productosSalida;
  };

  traerProductosEntrada = async () => {
    const { fecha, idVendedor } = this.state;
    let snapProductos = await db
      .collection("salidasEntradas")
      .where("fecha", "==", fecha)
      .where("idVendedor", "==", idVendedor)
      .where("tipoDe", "==", "entrada")
      .get();
    let productosEntrada = [];
    snapProductos.forEach((doc) => {
      productosEntrada[doc.id] = doc.data();
    });

    return productosEntrada;
  };

  traerDocumentos = () => {
    const { fecha, idVendedor } = this.state;
    const documentos = [];
    let efectivo = 0;
    let cheque = 0;
    let transferencia = 0;
    let otros = 0;
    let credito = 0;
    let total = 0;
    let totalRecibo = 0;
    this.setState(
      {
        documentos,
        efectivo,
        cheque,
        transferencia,
        otros,
        credito,
        total,
        totalRecibo,
        cargando: true,
      },
      async () => {
        let productos = await this.traerProductos();
        console.log(productos);
        let productosVentas = await this.traerProductosVentas();
        console.log(productosVentas);
        let productosSalida = await this.traerProductosSalida();
        let productosEntrada = await this.traerProductosEntrada();
        let snapShot = await db
          .collection("ventasCabezera")
          .where("fecha", "==", fecha)
          .where("idVendedor", "==", idVendedor)
          .orderBy("nDocumento")
          .get();
        snapShot.forEach((doc) => {
          documentos[doc.id] = doc.data();
          if (!documentos[doc.id].estadoCuenta) {
            total = total + documentos[doc.id].total;
            if (documentos[doc.id].formaPago === "Efectivo") {
              efectivo = efectivo + documentos[doc.id].total;
            }
            if (documentos[doc.id].formaPago === "Cheque") {
              cheque = cheque + documentos[doc.id].total;
            }
            if (documentos[doc.id].formaPago === "Transferencia") {
              transferencia = transferencia + documentos[doc.id].total;
            }
            if (documentos[doc.id].formaPago === "otros") {
              otros = otros + documentos[doc.id].total;
            }
          } else {
            if (documentos[doc.id].documento === "Recibo") {
              documentos[doc.id].totalVer = documentos[doc.id].total * -1;
              totalRecibo = totalRecibo - documentos[doc.id].total;
              if (documentos[doc.id].formaPago === "Efectivo") {
                efectivo = efectivo - documentos[doc.id].total;
              }
              if (documentos[doc.id].formaPago === "Cheque") {
                cheque = cheque - documentos[doc.id].total;
              }
              if (documentos[doc.id].formaPago === "Transferencia") {
                transferencia = transferencia - documentos[doc.id].total;
              }
              if (documentos[doc.id].formaPago === "otros") {
                otros = otros - documentos[doc.id].total;
              }
            } else {
              credito = credito + documentos[doc.id].total;
              total = total + documentos[doc.id].total;
            }
          }
        });

        Object.keys(productos).forEach((id) => {
          Object.keys(productosVentas).forEach((key) => {
            if (id === productosVentas[key].idProducto) {
              productos[id].cantidad =
                productos[id].cantidad +
                parseFloat(productosVentas[key].cantidad);
            }
          });
        });

        Object.keys(productos).forEach((id) => {
          Object.keys(productosSalida).forEach((key) => {
            if (id === productosSalida[key].idProducto) {
              productos[id].cantidadSalida =
                productos[id].cantidadSalida +
                parseFloat(productosSalida[key].cantidad);
            }
          });
        });

        Object.keys(productos).forEach((id) => {
          Object.keys(productosEntrada).forEach((key) => {
            if (id === productosEntrada[key].idProducto) {
              productos[id].cantidadEntrada =
                productos[id].cantidadEntrada +
                parseFloat(productosEntrada[key].cantidad);
            }
          });
        });

        Object.keys(productos).forEach((id) => {
          productos[id].cantidadDiferencia =
            productos[id].cantidadEntrada -
            (productos[id].cantidadSalida - productos[id].cantidad);
        });

        Object.keys(productos).forEach((id) => {
          if (
            productos[id].cantidad === 0 &&
            productos[id].cantidadSalida === 0 &&
            productos[id].cantidadEntrada === 0
          ) {
            delete productos[id];
          }
        });

        let docsOrdenados = await this.ordenarArqueo(documentos, "nDocumentos");

        this.setState({
          documentos: docsOrdenados,
          efectivo,
          cheque,
          transferencia,
          otros,
          credito,
          total,
          totalRecibo,
          productos,
          cargando: false,
        });
      }
    );
  };

  ordenarArqueo = (documentos, nDocumento) => {
    documentos.sort(function (a, b) {
      return a[nDocumento] > b[nDocumento];
    });
    return documentos;
  };

  handleChange = (e) => {
    if (e.target.name === "idVendedor") {
      let idVendedor = e.target.value;
      let vendedorNombre = e.target.selectedOptions[0].textContent;
      this.setState({ idVendedor, vendedorNombre, banderaVendedor: false });
    } else {
      this.setState({ [e.target.name]: e.target.value });
    }
  };

  borrar = async () => {
    let { id, clienteDoc, totalDoc, estadoCuenta, deudaActual } = this.state;

    let snapProductosABorrar = await db
      .collection("ventasProductos")
      .where("idCabezera", "==", id)
      .get();
    let productosABorrar = [];
    snapProductosABorrar.forEach((doc) => {
      productosABorrar[doc.id] = doc.data();
    });

    db.collection("ventasProductos")
      .where("idCabezera", "==", id)
      .get()
      .then((query) => {
        query.forEach((doc) => {
          db.collection("ventasProductos")
            .doc(doc.id)
            .delete()
            .then(() => console.log("linea borrada"))
            .catch((error) => console.log(error));
        });
        db.collection("ventasCabezera")
          .doc(id)
          .delete()
          .then(() => {
            this.setState({ show: false }, console.log("cabezera borrada"));
          })
          .catch((error) => console.log(error));
      })
      .then(() => {
        if (estadoCuenta) {
          let nuevaDeudaActual = deudaActual - totalDoc;
          db.collection("clientes")
            .doc(clienteDoc)
            .update({ deudaActual: nuevaDeudaActual })
            .then(() => console.log("Total Adeudado CORREGIDO"))
            .catch((error) => console.log(error));
        }
      })
      .then(() => this.traerDocumentos())
      .catch((error) => console.log(error));
  };

  handleClose = () => {
    this.setState({ show: false });
  };

  handleShow = async (id, clienteDoc, estadoCuenta, totalDoc) => {
    if (estadoCuenta) {
      let snapCliente = await db.collection("clientes").doc(clienteDoc).get();
      let deudaActual = snapCliente.data().deudaActual;
      if (deudaActual) {
        deudaActual = parseFloat(deudaActual);
      } else {
        deudaActual = 0;
      }
      this.setState({
        show: true,
        id: id,
        clienteDoc,
        totalDoc,
        estadoCuenta,
        deudaActual,
      });
    } else {
      this.setState({
        show: true,
        id: id,
        clienteDoc,
        totalDoc,
        estadoCuenta,
      });
    }
  };

  detalle = (id, documento) => {
    let claseDoc = "";
    if (id === this.state.idFactura) {
      this.setState({ idFactura: "", claseDoc });
    } else {
      if (documento === "Recibo") {
        claseDoc = "table-success";
        this.setState({ idFactura: id, claseDoc });
      } else {
        if (documento === "Factura Contado") {
          claseDoc = "table-danger";
        }
        if (documento === "Boleta Contado") {
          claseDoc = "table-warning";
        }
        if (documento === "Factura Crédito") {
          claseDoc = "table-active";
        }
        if (documento === "Boleta Crédito") {
          claseDoc = "table-info";
        }
        this.setState({ idFactura: id, claseDoc });
      }
    }
  };

  render() {
    const {
      documentos,
      productos,
      total,
      totalRecibo,
      efectivo,
      cheque,
      transferencia,
      otros,
      credito,
      vendedores,
      vendedorNombre,
    } = this.state;
    return (
      <div>
        <Container>
          <Row>
            <Col>
              <h1 style={{ padding: "15px" }}>Arqueo</h1>
            </Col>
          </Row>
          <Row>
            <Col>
              <Form.Group>
                <Form.Label>Fecha</Form.Label>
                <input
                  name="fecha"
                  className="form-control text-center"
                  type="date"
                  onChange={(e) => this.handleChange(e)}
                  value={this.state.fecha}
                  disabled={!this.props.vendedor.permisosVendedor}
                />
              </Form.Group>
            </Col>
            <Col>
              <Form.Group>
                <Form.Label>Vendedor</Form.Label>
                {!this.state.banderaVendedor ? (
                  <p onClick={() => this.banderaVendedor()}>
                    <strong>{vendedorNombre}</strong>
                  </p>
                ) : (
                  <select
                    className="form-control"
                    onChange={(e) => this.handleChange(e)}
                    name="idVendedor"
                    value={this.state.idVendedor}
                    aria-label="Small"
                  >
                    <option value="">Seleccione Vendedor...</option>
                    {Object.keys(vendedores).map((id) => {
                      return (
                        <option value={id}>{vendedores[id].vendedor}</option>
                      );
                    })}
                  </select>
                )}
              </Form.Group>
            </Col>
          </Row>
          <Row>
            <Col>
              <Button
                variant="success"
                onClick={() => {
                  this.traerDocumentos();
                }}
              >
                Buscar
              </Button>
            </Col>
          </Row>
        </Container>
        {this.state.cargando ? (
          <div
            style={{
              width: "100%",
              height: "200px",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Spinner animation="grow" variant="primary" />
          </div>
        ) : (
          <div style={{ marginTop: "20px" }}>
            <Tab.Container id="listado" defaultActiveKey="productos">
              <Nav fill variant="tabs">
                <Nav.Item>
                  <Nav.Link eventKey="productos">Productos</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link eventKey="documentos">Documentos</Nav.Link>
                </Nav.Item>
              </Nav>
              <Tab.Content>
                <Tab.Pane eventKey="documentos">
                  <div
                    style={{
                      width: "100%",
                      overflow: "scroll",
                      marginTop: "20px",
                    }}
                  >
                    <Table striped bordered condensed hover responsive>
                      <thead>
                        <tr>
                          <th>Fecha</th>
                          <th>Cliente</th>
                          <th>Documento</th>
                          <th>Nº</th>
                          <th>Importe</th>
                          <th>Forma de Pago</th>
                        </tr>
                      </thead>
                      <tbody>
                        {Object.keys(documentos).length > 0 ? (
                          Object.keys(documentos).map((id) => {
                            return (
                              <Fragment>
                                <tr
                                  key={id}
                                  className={
                                    this.state.idFactura === id
                                      ? this.state.claseDoc
                                      : ""
                                  }
                                >
                                  <td
                                    onClick={() =>
                                      this.detalle(id, documentos[id].documento)
                                    }
                                  >
                                    {documentos[id].fecha}
                                  </td>
                                  <td
                                    onClick={() =>
                                      this.detalle(id, documentos[id].documento)
                                    }
                                  >
                                    {documentos[id].clienteNombre}
                                  </td>
                                  <td
                                    onClick={() =>
                                      this.detalle(id, documentos[id].documento)
                                    }
                                  >
                                    {documentos[id].documento}
                                  </td>
                                  <td
                                    onClick={() =>
                                      this.detalle(id, documentos[id].documento)
                                    }
                                  >
                                    {documentos[id].nDocumento}
                                  </td>
                                  {this.state.idFactura === id ? (
                                    <td
                                      onClick={() =>
                                        this.detalle(
                                          id,
                                          documentos[id].documento
                                        )
                                      }
                                    >
                                      <strong>
                                        {documentos[id].documento === "Recibo"
                                          ? documentos[
                                              id
                                            ].totalVer.toLocaleString("de-DE")
                                          : documentos[id].total.toLocaleString(
                                              "de-DE"
                                            )}
                                      </strong>
                                    </td>
                                  ) : (
                                    <td
                                      onClick={() =>
                                        this.detalle(
                                          id,
                                          documentos[id].documento
                                        )
                                      }
                                    >
                                      {documentos[id].documento === "Recibo"
                                        ? documentos[
                                            id
                                          ].totalVer.toLocaleString("de-DE")
                                        : documentos[id].total.toLocaleString(
                                            "de-DE"
                                          )}
                                    </td>
                                  )}
                                  <td
                                    onClick={() =>
                                      this.detalle(id, documentos[id].documento)
                                    }
                                  >
                                    {documentos[id].formaPago}
                                  </td>
                                  {this.props.vendedor.permisosVendedor ? (
                                    <td>
                                      <Button
                                        variant="danger"
                                        onClick={() => {
                                          this.handleShow(
                                            id,
                                            documentos[id].cliente,
                                            documentos[id].estadoCuenta,
                                            documentos[id].total
                                          );
                                        }}
                                      >
                                        Eliminar
                                      </Button>
                                    </td>
                                  ) : null}
                                </tr>
                                {this.state.idFactura === id ? (
                                  <Detalle
                                    idFactura={id}
                                    claseDoc={this.state.claseDoc}
                                    documentos={documentos[id]}
                                  />
                                ) : null}
                              </Fragment>
                            );
                          })
                        ) : (
                          <tr>
                            <td colSpan="8">
                              <p>NO se encontraron resultados</p>
                            </td>
                          </tr>
                        )}
                      </tbody>
                    </Table>
                  </div>
                </Tab.Pane>
                <Tab.Pane eventKey="productos">
                  <div
                    style={{
                      width: "100%",
                      overflow: "scroll",
                      marginTop: "20px",
                    }}
                  >
                    <ListGroup variant="flush">
                      <ListGroup.Item>
                        <Row>
                          <Col xs={4}>
                            <strong>Producto</strong>
                          </Col>
                          <Col xs={2}>
                            <strong>Salida</strong>
                          </Col>
                          <Col xs={2}>
                            <strong>Venta</strong>
                          </Col>
                          <Col xs={2}>
                            <strong>Entrada</strong>
                          </Col>
                          <Col xs={2}>
                            <strong>Dif</strong>
                          </Col>
                        </Row>
                      </ListGroup.Item>
                      {Object.keys(productos).length > 0 ? (
                        Object.keys(productos).map((id) => {
                          return (
                            <ListGroup.Item
                              variant={
                                productos[id].cantidadDiferencia === 0
                                  ? "success"
                                  : productos[id].cantidadDiferencia > 0
                                  ? "primary"
                                  : "danger"
                              }
                            >
                              <Row>
                                <Col xs={4} style={{ textAlign: "left" }}>
                                  {productos[id].producto}
                                </Col>
                                <Col xs={2}>
                                  {productos[id].cantidadSalida.toLocaleString(
                                    "de-DE"
                                  )}
                                </Col>
                                <Col xs={2}>
                                  {productos[id].cantidad.toLocaleString(
                                    "de-DE"
                                  )}
                                </Col>
                                <Col xs={2}>
                                  {productos[id].cantidadEntrada.toLocaleString(
                                    "de-DE"
                                  )}
                                </Col>
                                <Col xs={2}>
                                  <strong>
                                    {productos[
                                      id
                                    ].cantidadDiferencia.toLocaleString(
                                      "de-DE"
                                    )}
                                  </strong>
                                </Col>
                              </Row>
                            </ListGroup.Item>
                          );
                        })
                      ) : (
                        <ListGroup.Item>
                          NO se encontraron resultados
                        </ListGroup.Item>
                      )}
                    </ListGroup>
                  </div>
                </Tab.Pane>
              </Tab.Content>
            </Tab.Container>
          </div>
        )}
        {Object.keys(documentos).length > 0 ? (
          <div>
            <Table>
              <tbody>
                <tr>
                  <td />
                  <td />
                  <td />
                  <td />
                  <td colSpan="2">
                    <strong>Total Efectivo</strong>
                  </td>
                  <td>
                    <strong>{efectivo.toLocaleString("de-DE")}</strong>
                  </td>
                  <td />
                </tr>
                <tr>
                  <td />
                  <td />
                  <td />
                  <td />
                  <td colSpan="2">
                    <strong>Total Cheque</strong>
                  </td>
                  <td>
                    <strong>{cheque.toLocaleString("de-DE")}</strong>
                  </td>
                  <td />
                </tr>
                <tr>
                  <td />
                  <td />
                  <td />
                  <td />
                  <td colSpan="2">
                    <strong>Total Transferencia</strong>
                  </td>
                  <td>
                    <strong>{transferencia.toLocaleString("de-DE")}</strong>
                  </td>
                  <td />
                </tr>
                <tr>
                  <td />
                  <td />
                  <td />
                  <td />
                  <td colSpan="2">
                    <strong>Total Otros</strong>
                  </td>
                  <td>
                    <strong>{otros.toLocaleString("de-DE")}</strong>
                  </td>
                  <td />
                </tr>
                <tr>
                  <td />
                  <td />
                  <td />
                  <td />
                  <td colSpan="2">
                    <strong>Total Crédito</strong>
                  </td>
                  <td>
                    <strong>{credito.toLocaleString("de-DE")}</strong>
                  </td>
                  <td />
                </tr>
                <tr>
                  <td />
                  <td />
                  <td />
                  <td />
                  <td colSpan="2">
                    <strong>Vendido</strong>
                  </td>
                  <td>
                    <strong>{total.toLocaleString("de-DE")}</strong>
                  </td>
                  <td />
                </tr>
                <tr>
                  <td />
                  <td />
                  <td />
                  <td />
                  <td colSpan="2">
                    <strong>Recibos Cobrados</strong>
                  </td>
                  <td>
                    <strong>{totalRecibo.toLocaleString("de-DE")}</strong>
                  </td>
                  <td />
                </tr>
              </tbody>
            </Table>
          </div>
        ) : null}
        <Modal show={this.state.show} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>ATENCIÓN!!!</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            Estás seguro que quieres ELIMINAR este documento?!
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleClose}>
              Cancelar
            </Button>
            <Button
              variant="danger"
              onClick={() => {
                this.borrar();
              }}
            >
              ELIMINAR
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

export default Arqueo;
