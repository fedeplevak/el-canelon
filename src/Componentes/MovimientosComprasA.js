import React, { Component, Fragment } from "react";
import {
  Table,
  Button,
  Spinner,
  Container,
  Row,
  Col,
  Modal
} from "react-bootstrap";
import { db } from "../firebaseConfig";
import DetalleCompras from "./DetalleCompras";
import { nullLiteral } from "@babel/types";

class MovimientosCompras extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: "",
      proveedorDoc: "",
      totalDoc: "",
      estadoCuenta: "",
      deudaActual: ""
    };
  }

  borrar = async () => {
    let { id, proveedorDoc, totalDoc, estadoCuenta, deudaActual } = this.state;

    /* let snapProductosStock = await db.collection("productos").get();
    let productosStock = [];
    snapProductosStock.forEach(doc => {
      productosStock[doc.id] = doc.data();
      if (!productosStock[doc.id].stock) {
        productosStock[doc.id].stock = 0;
      }
    }); */

    let snapProductosABorrar = await db
      .collection("comprasProductos")
      .where("idCabezera", "==", id)
      .get();
    let productosABorrar = [];
    snapProductosABorrar.forEach(doc => {
      productosABorrar[doc.id] = doc.data();
    });

    /* Object.keys(productosABorrar).map(id => {
      Object.keys(productosStock).map(key => {
        if (productosABorrar[id].idProducto == key) {
          productosABorrar[id].stock =
            parseFloat(productosStock[key].stock) +
            productosABorrar[id].cantidad;
        }
      });
    }); */

    /* Object.keys(productosABorrar).map(id => {
      let stock = productosABorrar[id].stock;
      db.collection("productos")
        .doc(productosABorrar[id].idProducto)
        .update({
          stock: stock
        })
        .then(() => console.log("Stock Corregido"))
        .catch(error => console.log(error));
    }); */

    db.collection("comprasProductos")
      .where("idCabezera", "==", id)
      .get()
      .then(query => {
        query.forEach(doc => {
          db.collection("comprasProductos")
            .doc(doc.id)
            .delete()
            .then(() => console.log("linea borrada"))
            .catch(error => console.log(error));
        });
        db.collection("comprasCabezera")
          .doc(id)
          .delete()
          .then(() => {
            this.setState({ show: false }, console.log("cabezera borrada"));
          })
          .catch(error => console.log(error));
      })
      .then(() => {
        if (estadoCuenta) {
          let nuevaDeudaActual = deudaActual - totalDoc;
          db.collection("proveedores")
            .doc(proveedorDoc)
            .update({ deudaActual: nuevaDeudaActual })
            .then(() => console.log("Total Adeudado CORREGIDO"))
            .catch(error => console.log(error));
        }
      })
      .then(() => this.props.estadoCuenta())
      .catch(error => console.log(error));
  };

  handleClose = () => {
    this.setState({ show: false });
  };

  handleShow = async (id, proveedorDoc, estadoCuenta, totalDoc) => {
    if (estadoCuenta) {
      let snapProveedor = await db
        .collection("proveedores")
        .doc(proveedorDoc)
        .get();
      let deudaActual = snapProveedor.data().deudaActual;
      if (deudaActual) {
        deudaActual = parseFloat(deudaActual);
      } else {
        deudaActual = 0;
      }
      this.setState({
        show: true,
        id: id,
        proveedorDoc,
        totalDoc,
        estadoCuenta,
        deudaActual
      });
    } else {
      this.setState({
        show: true,
        id: id,
        proveedorDoc,
        totalDoc,
        estadoCuenta
      });
    }
  };

  detalle = (id, documento) => {
    let claseDoc = "";
    if (id === this.state.idFactura) {
      this.setState({ idFactura: "", claseDoc });
    } else {
      if (documento === "Recibo") {
        claseDoc = "table-success";
        this.setState({ idFactura: id, claseDoc });
      } else {
        if (documento === "Factura Contado") {
          claseDoc = "table-danger";
        }
        if (documento === "Boleta Contado") {
          claseDoc = "table-warning";
        }
        if (documento === "Factura Crédito") {
          claseDoc = "table-active";
        }
        if (documento === "Boleta Crédito") {
          claseDoc = "table-info";
        }
        this.setState({ idFactura: id, claseDoc });
      }
    }
  };

  render() {
    const { documentos, cargando } = this.props;
    const { permisosVendedor } = this.props.vendedor;
    return (
      <div>
        <Container>
          <Row>
            <Col>
              <h1 style={{ padding: "15px" }}>Movimientos</h1>
            </Col>
          </Row>
        </Container>
        {cargando ? (
          <div
            style={{
              width: "100%",
              height: "200px",
              display: "flex",
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <Spinner animation="grow" variant="primary" />
          </div>
        ) : (
          <div style={{ width: "100%", overflow: "scroll", marginTop: "20px" }}>
            <Table striped bordered condensed hover responsive>
              <thead>
                <tr>
                  <th>Fecha</th>
                  <th>Proveedor</th>
                  <th>Documento</th>
                  <th>Nº</th>
                  <th>Percepción</th>
                  <th>Importe</th>
                  <th>Forma de Pago</th>
                  {permisosVendedor ? <th /> : null}
                </tr>
              </thead>
              <tbody>
                {Object.keys(documentos).length > 0 ? (
                  Object.keys(documentos).map(id => {
                    return (
                      <Fragment>
                        <tr
                          key={id}
                          className={
                            this.state.idFactura == id
                              ? this.state.claseDoc
                              : ""
                          }
                        >
                          <td
                            onClick={() =>
                              this.detalle(id, documentos[id].documento)
                            }
                          >
                            {documentos[id].fecha}
                          </td>
                          <td
                            onClick={() =>
                              this.detalle(id, documentos[id].documento)
                            }
                          >
                            {documentos[id].proveedorNombre}
                          </td>
                          <td
                            onClick={() =>
                              this.detalle(id, documentos[id].documento)
                            }
                          >
                            {documentos[id].documento}
                          </td>
                          <td
                            onClick={() =>
                              this.detalle(id, documentos[id].documento)
                            }
                          >
                            {documentos[id].nDocumento}
                          </td>
                          <td
                            onClick={() =>
                              this.detalle(id, documentos[id].documento)
                            }
                          >
                            {documentos[id].compraPago === "compra" ?
                             documentos[id].percepcion.toLocaleString("de-DE") 
                            : ""}
                          </td>
                          {this.state.idFactura == id ? (
                            <td
                              onClick={() =>
                                this.detalle(id, documentos[id].documento)
                              }
                            >
                              <strong>
                                {documentos[id].total.toLocaleString("de-DE")}
                              </strong>
                            </td>
                          ) : (
                            <td
                              onClick={() =>
                                this.detalle(id, documentos[id].documento)
                              }
                            >
                              {documentos[id].total.toLocaleString("de-DE")}
                            </td>
                          )}
                          <td
                            onClick={() =>
                              this.detalle(id, documentos[id].documento)
                            }
                          >
                            {documentos[id].formaPago}
                          </td>
                          {permisosVendedor ? (
                            <td>
                              <Button
                                variant="danger"
                                onClick={() => {
                                  this.handleShow(
                                    id,
                                    documentos[id].proveedor,
                                    documentos[id].estadoCuenta,
                                    documentos[id].total
                                  );
                                }}
                              >
                                Eliminar
                              </Button>
                            </td>
                          ) : null}
                        </tr>
                        {this.state.idFactura === id ? (
                          <DetalleCompras
                            idFactura={id}
                            claseDoc={this.state.claseDoc}
                            documentos={documentos[id]}
                            empresa={this.props.empresa}
                          />
                        ) : null}
                      </Fragment>
                    );
                  })
                ) : (
                  <tr>
                    <td colSpan="8">
                      <p>NO se encontraron resultados</p>
                    </td>
                  </tr>
                )}
              </tbody>
            </Table>
          </div>
        )}
        <Modal show={this.state.show} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>ATENCIÓN!!!</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            Estás seguro que quieres ELIMINAR este documento?!
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleClose}>
              Cancelar
            </Button>
            <Button
              variant="danger"
              onClick={() => {
                this.borrar();
              }}
            >
              ELIMINAR
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

export default MovimientosCompras;
