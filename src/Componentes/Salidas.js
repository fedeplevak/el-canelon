import React, { Component } from "react";
import { Button, Form, Container, Row, Col, ListGroup } from "react-bootstrap";
import { db } from "../firebaseConfig";
import { TiDelete } from "react-icons/ti";

class Salidas extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cargando: false,
      vendedores: [],
      lugares: [],
      productos: [],
      salidas: [],
      productosTotales: [],
      fecha: "",
      idVendedor: "",
      idLugar: "",
      idProducto: "",
      nombreVendedor: "",
      nombreLugar: "",
      nombreProducto: "",
    };
  }

  componentDidMount() {
    this.hoy();
    this.seleccionarVendedor();
    this.traerVendedores();
    this.traerLugares();
    this.traerProductos();
    this.traerSalidas();
  }

  seleccionarVendedor = () => {
    let idVendedor = this.props.idUsuario;
    let nombreVendedor = this.props.usuario.vendedor;
    this.setState({ idVendedor, nombreVendedor });
  };

  traerVendedores = async () => {
    let snapVendedores = await db.collection("vendedores").get();
    let vendedores = [];
    snapVendedores.forEach((doc) => {
      vendedores[doc.id] = doc.data();
    });
    this.setState({ vendedores });
  };

  traerLugares = async () => {
    let snapLugares = await db.collection("lugares").get();
    let lugares = [];
    snapLugares.forEach((doc) => {
      lugares[doc.id] = doc.data();
    });
    this.setState({ lugares });
  };

  traerProductos = async () => {
    let snapProductos = await db.collection("productos").orderBy("orden").get();
    let productos = [];
    snapProductos.forEach((doc) => {
      productos[doc.id] = doc.data();
      productos[doc.id].cantidadTotal = 0;
    });
    this.setState({ productos });
  };

  hoy = () => {
    let hoy = new Date();

    let año = hoy.getFullYear();
    let mes =
      hoy.getMonth() + 1 > 9 ? hoy.getMonth() + 1 : `0${hoy.getMonth() + 1}`;
    let dia = hoy.getDate() > 9 ? hoy.getDate() : `0${hoy.getDate()}`;

    const fecha = `${año}-${mes}-${dia}`;
    this.setState({ fecha });
  };

  handleChange = (e) => {
    if (e.target.name === "fecha") {
      this.setState({ fecha: e.target.value }, () => {
        this.traerSalidas();
      });
    }
    if (e.target.name === "idVendedor") {
      let idVendedor = e.target.value;
      let nombreVendedor = e.target.selectedOptions[0].textContent;
      this.setState({ idVendedor, nombreVendedor }, () => {
        this.traerSalidas();
      });
    }
    if (e.target.name === "idLugar") {
      let idLugar = e.target.value;
      let nombreLugar = e.target.selectedOptions[0].textContent;
      this.setState({ idLugar, nombreLugar });
    }
    if (e.target.name === "idProducto") {
      let idProducto = e.target.value;
      let nombreProducto = e.target.selectedOptions[0].textContent;
      this.setState({ idProducto, nombreProducto });
    } else {
      this.setState({ [e.target.name]: e.target.value });
    }
  };

  handleClose = () => {
    this.setState({ show: false });
  };

  handleShow = async (id, clienteDoc, estadoCuenta, totalDoc) => {
    if (estadoCuenta) {
      let snapCliente = await db.collection("clientes").doc(clienteDoc).get();
      let totalAdeudado = snapCliente.data().totalAdeudado;
      if (totalAdeudado) {
        totalAdeudado = parseFloat(totalAdeudado);
      } else {
        totalAdeudado = 0;
      }
      this.setState({
        show: true,
        id: id,
        clienteDoc,
        totalDoc,
        estadoCuenta,
        totalAdeudado,
      });
    } else {
      this.setState({ show: true, id: id, clienteDoc, totalDoc, estadoCuenta });
    }
  };

  borrar = async (id) => {
    let { idLugar, salidas, productos } = this.state;

    if (this.props.usuario.permisosVendedor) {
      let idProducto = salidas[id].idProducto;
      let cantidad = parseFloat(salidas[id].cantidad);
      let refArticulo = db
        .collection("lugares")
        .doc(idLugar)
        .collection("productos")
        .doc(idProducto);
      let articulo = {};
      let snapArticulo = await refArticulo.get();
      if (snapArticulo.exists) {
        articulo = snapArticulo.data();
      } else {
        articulo = { ...productos[idProducto] };
      }
      let nuevoStock = 0;
      if (articulo.stock !== undefined) {
        nuevoStock = parseFloat(articulo.stock) + cantidad;
      } else {
        nuevoStock = cantidad;
      }
      articulo.stock = nuevoStock;
      console.log(articulo);

      refArticulo.set(articulo);

      db.collection("salidasEntradas")
        .doc(id)
        .delete()
        .then(() => console.log("borrado"))
        .catch((error) => console.log(error));
    }
  };

  agregarSalida = async () => {
    let {
      fecha,
      idLugar,
      nombreLugar,
      idVendedor,
      nombreVendedor,
      idProducto,
      nombreProducto,
      cantidad,
      productos,
    } = this.state;
    if (fecha && idLugar && idVendedor && idProducto && cantidad) {
      let refArticulo = db
        .collection("lugares")
        .doc(idLugar)
        .collection("productos")
        .doc(idProducto);
      let articulo = {};
      let snapArticulo = await refArticulo.get();
      if (snapArticulo.exists) {
        articulo = snapArticulo.data();
      } else {
        articulo = { ...productos[idProducto] };
      }
      let nuevoStock = 0;
      if (articulo.stock !== undefined) {
        nuevoStock = parseFloat(articulo.stock) - parseFloat(cantidad);
      } else {
        nuevoStock = parseFloat(cantidad) * -1;
      }
      articulo.stock = nuevoStock;
      console.log(articulo);

      refArticulo.set(articulo);

      db.collection("salidasEntradas")
        .add({
          fecha: fecha,
          idLugar: idLugar,
          nombreLugar: nombreLugar,
          idVendedor: idVendedor,
          nombreVendedor: nombreVendedor,
          idProducto: idProducto,
          nombreProducto: nombreProducto,
          cantidad: cantidad,
          tipoDe: "salida",
          ...productos[idProducto],
        })
        .then(() => {
          this.traerSalidas();
          this.setState({ cantidad: "" });
        })
        .catch((error) => console.log(error));
    }
  };

  traerSalidas = () => {
    let { fecha, idVendedor } = this.state;
    db.collection("salidasEntradas")
      .where("fecha", "==", fecha)
      .where("idVendedor", "==", idVendedor)
      .where("tipoDe", "==", "salida")
      .orderBy("orden")
      .onSnapshot((snap) => {
        let salidas = [];
        snap.forEach((doc) => {
          salidas[doc.id] = doc.data();
        });
        this.setState({ salidas }, () => {
          this.productosTotales();
        });
      });
  };

  productosTotales = () => {
    let { productos, salidas } = this.state;
    let productosTotales = [];

    Object.keys(productos).forEach((id) => {
      productosTotales[id] = { ...productos[id] };
    });

    Object.keys(productosTotales).forEach((id) => {
      Object.keys(salidas).forEach((key) => {
        if (id === salidas[key].idProducto) {
          productosTotales[id].cantidadTotal =
            productosTotales[id].cantidadTotal +
            parseFloat(salidas[key].cantidad);
        }
      });
    });
    Object.keys(productosTotales).forEach((id) => {
      if (productosTotales[id].cantidadTotal === 0) {
        delete productosTotales[id];
      }
    });
    this.setState({ productosTotales });
  };

  render() {
    const {
      vendedores,
      lugares,
      productos,
      salidas,
      productosTotales,
      fecha,
      idLugar,
      idVendedor,
      idProducto,
      nombreVendedor,
      cantidad,
    } = this.state;
    return (
      <div style={{ width: "100%" }}>
        <Container>
          <h3 style={{ marginTop: "15px" }}>Salidas</h3>
          <Row>
            <Col>
              <Form.Group>
                <Form.Label style={{ color: fecha ? null : "red" }}>
                  Fecha
                </Form.Label>
                <input
                  name="fecha"
                  className="form-control text-center"
                  type="date"
                  onChange={(e) => this.handleChange(e)}
                  value={fecha}
                  disabled={!this.props.usuario.permisosVendedor}
                />
              </Form.Group>
            </Col>
          </Row>
          <Row>
            <Col>
              <Form.Group>
                <Form.Label style={{ color: idLugar ? null : "red" }}>
                  Lugar Desde:
                </Form.Label>
                <select
                  className="form-control"
                  onChange={(e) => this.handleChange(e)}
                  name="idLugar"
                  value={this.state.idLugar}
                  aria-label="Small"
                >
                  <option value="">Seleccionar Lugar...</option>
                  {Object.keys(lugares).map((id) => {
                    return <option value={id}>{lugares[id].nombre}</option>;
                  })}
                </select>
              </Form.Group>
            </Col>
          </Row>
          <Row>
            <Col>
              {!this.props.usuario.permisosVendedor ? (
                <p>
                  <strong>{nombreVendedor}</strong>
                </p>
              ) : idVendedor ? (
                <p
                  onClick={
                    this.props.usuario.permisosVendedor
                      ? () => {
                          this.setState({ idVendedor: "", nombreVendedor: "" });
                        }
                      : null
                  }
                >
                  <strong>{nombreVendedor}</strong>
                </p>
              ) : (
                <Form.Group>
                  <Form.Label style={{ color: idVendedor ? null : "red" }}>
                    Vendedor
                  </Form.Label>
                  <select
                    className="form-control"
                    onChange={(e) => this.handleChange(e)}
                    name="idVendedor"
                    value={this.state.idVendedor}
                    aria-label="Small"
                  >
                    <option value="">Seleccionar Vendedor...</option>
                    {Object.keys(vendedores).map((id) => {
                      return (
                        <option value={id}>{vendedores[id].vendedor}</option>
                      );
                    })}
                  </select>
                </Form.Group>
              )}
            </Col>
          </Row>
          <Row>
            <Col>
              <Form.Group>
                <Form.Label style={{ color: idProducto ? null : "red" }}>
                  Productos
                </Form.Label>
                <select
                  className="form-control"
                  onChange={(e) => this.handleChange(e)}
                  name="idProducto"
                  value={this.state.idProducto}
                  aria-label="Small"
                >
                  <option value="">Seleccionar Producto...</option>
                  {Object.keys(productos).map((id) => {
                    return <option value={id}>{productos[id].producto}</option>;
                  })}
                </select>
              </Form.Group>
            </Col>
          </Row>
          <Row>
            <Col>
              <Form.Group>
                <Form.Label style={{ color: cantidad ? null : "red" }}>
                  Cantidad
                </Form.Label>
                <input
                  name="cantidad"
                  className="form-control text-center"
                  type="number"
                  onChange={(e) => this.handleChange(e)}
                  value={this.state.cantidad}
                />
              </Form.Group>
            </Col>
          </Row>
          {fecha && idLugar && idVendedor && idProducto && cantidad ? (
            <Row>
              <Col>
                <Button
                  variant="success"
                  onClick={() => {
                    this.agregarSalida();
                  }}
                >
                  Agregar
                </Button>
              </Col>
            </Row>
          ) : null}
          <h3 style={{ marginTop: "15px" }}>Salida</h3>
          <h3 style={{ marginTop: "15px" }}>{this.state.nombreVendedor}</h3>
          <ListGroup variant="flush" style={{ marginTop: "15px" }}>
            <ListGroup.Item style={{ paddingLeft: "0px", paddingRight: "0px" }}>
              <Row>
                <Col xs={2}>
                  <strong>Cantidad</strong>
                </Col>
                <Col xs={5}>
                  <strong>Producto</strong>
                </Col>
                <Col xs={3}>
                  <strong>Lugar</strong>
                </Col>
              </Row>
            </ListGroup.Item>
            {Object.keys(salidas).map((id) => {
              return (
                <ListGroup.Item>
                  <Row>
                    <Col xs={2}>
                      {salidas[id].cantidad.toLocaleString("de-DE")}
                    </Col>
                    <Col xs={5} style={{ textAlign: "left" }}>
                      {salidas[id].nombreProducto}
                    </Col>
                    <Col xs={3}>{salidas[id].nombreLugar}</Col>
                    {this.props.usuario.permisosVendedor ? (
                      <Col xs={2}>
                        <TiDelete
                          color="red"
                          size="1.5em"
                          onClick={() => this.borrar(id)}
                        />
                      </Col>
                    ) : null}
                  </Row>
                </ListGroup.Item>
              );
            })}
          </ListGroup>
          <h3 style={{ marginTop: "15px" }}>Productos Totales</h3>
          <ListGroup variant="flush" style={{ marginTop: "15px" }}>
            <ListGroup.Item>
              <Row>
                <Col xs={4}>
                  <strong>Cantidad</strong>
                </Col>
                <Col xs={8}>
                  <strong>Producto</strong>
                </Col>
              </Row>
            </ListGroup.Item>
            {Object.keys(productosTotales).map((id) => {
              return (
                <ListGroup.Item variant="success">
                  <Row>
                    <Col xs={4}>
                      {productosTotales[id].cantidadTotal.toLocaleString(
                        "de-DE"
                      )}
                    </Col>
                    <Col xs={8} style={{ textAlign: "left" }}>
                      {productosTotales[id].producto}
                    </Col>
                  </Row>
                </ListGroup.Item>
              );
            })}
          </ListGroup>
        </Container>
      </div>
    );
  }
}

export default Salidas;
