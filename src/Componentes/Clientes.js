import React, { Component } from "react";
import Spinner from "react-bootstrap/Spinner";
import { db } from "../firebaseConfig";
import {
  Table,
  Button,
  ToggleButton,
  ToggleButtonGroup,
  Container,
  Row,
  Col,
  Modal,
  Form,
  ListGroup,
} from "react-bootstrap";
import { MdDelete, MdPersonAdd, MdClose } from "react-icons/md";
import { FiEdit } from "react-icons/fi";
import { BsListCheck } from "react-icons/bs";

class Clientes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cargando: true,
      clientes: [],
      idCliente: "",
      productos: [],
      banderaProductos: false,
      productosActivos: [],
      show: false,
      editar: "",
      nombre: "",
      razonSocial: "",
      rut: "",
      direccion: "",
      nCarpeta: "",
      zona: "",
      telefono: "",
      mail: "",
      deudaActual: "",
      observaciones: "",
      nombreEditar: "",
      razonSocialEditar: "",
      rutEditar: "",
      direccionEditar: "",
      nCarpetaEditar: "",
      zonaEditar: "",
      telefonoEditar: "",
      mailEditar: "",
      deudaActualEditar: "",
      observacionesEditar: "",
      banderaNuevoCliente: false,
      clientesMostrar: [],
      buscado: "",
      cargandoCliente: false,
      cargandoClienteEditar: false,
      banderaCliente: "",
      banderaEditar: false,
      producto: "",
      precio: "",
      showPro: false,
      cargandoProductosClientes: false,
      productosClientes: [],
      clientesPasar: []
    };
  }

  componentDidMount() {
    this.traerClientes();
    this.traerProductos();
  }

  traerClientes = () => {
    db.collection("clientes")
      .orderBy("nombre")
      .onSnapshot((snapShot) => {
        const clientes = [];
        snapShot.forEach((doc) => {
          clientes[doc.id] = doc.data();
          if (!doc.data().deudaActual) {
            clientes[doc.id].deudaActual = 0;
          }
          if (!doc.data().deudaCambio) {
            clientes[doc.id].deudaCambio = 0;
          }
        });
        this.checkBuscador(clientes);
        this.setState({ clientes, cargando: false });
      });
  };

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleChangeCheck = (e) => {
    let activo = e.target.checked;
    let id = e.target.id;
    db.collection("clientes")
      .doc(id)
      .update({
        activo: activo,
      })
      .then(() => console.log("handle check cambiado"))
      .catch((error) => console.log(error));
  };

  banderaCliente = (id) => {
    let { banderaCliente } = this.state;
    if (banderaCliente === id) {
      this.setState({
        banderaCliente: "",
        banderaEditar: false,
        nombreEditar: "",
        razonSocialEditar: "",
        rutEditar: "",
        direccionEditar: "",
        nCarpetaEditar: "",
        zonaEditar: "",
        telefonoEditar: "",
        mailEditar: "",
        deudaActualEditar: "",
        observacionesEditar: "",
      });
    } else {
      this.setState({
        banderaCliente: id,
        banderaEditar: false,
        nombreEditar: "",
        razonSocialEditar: "",
        rutEditar: "",
        direccionEditar: "",
        nCarpetaEditar: "",
        zonaEditar: "",
        telefonoEditar: "",
        mailEditar: "",
        deudaActualEditar: "",
        observacionesEditar: "",
      });
    }
  };

  editar = (
    nombreEditar,
    razonSocialEditar,
    rutEditar,
    nCarpetaEditar,
    direccionEditar,
    zonaEditar,
    telefonoEditar,
    mailEditar,
    deudaActualEditar,
    observacionesEditar
  ) => {
    this.setState({
      banderaEditar: true,
      nombreEditar,
      razonSocialEditar,
      rutEditar,
      nCarpetaEditar,
      direccionEditar,
      zonaEditar,
      telefonoEditar,
      mailEditar,
      deudaActualEditar,
      observacionesEditar,
    });
  };

  borrar = (id) => {
    db.collection("clientes")
      .doc(id)
      .delete()
      .then(() => {
        this.handleClose();
      })
      .catch((error) => console.log(error));
  };

  nuevoCliente = () => {
    let { banderaNuevoCliente } = this.state;
    if (banderaNuevoCliente) {
      this.setState({ banderaNuevoCliente: false });
    } else {
      this.setState({ banderaNuevoCliente: true });
    }
  };

  guardarNuevo = () => {
    let {
      nombre,
      razonSocial,
      rut,
      nCarpeta,
      direccion,
      zona,
      telefono,
      mail,
      deudaActual,
      deudaCambio,
      observaciones,
    } = this.state;
    deudaActual = parseFloat(deudaActual);
    if (!deudaActual) {
      deudaActual = 0;
    }
    if (nombre && zona) {
      this.setState({ cargandoCliente: true }, () => {
        db.collection("clientes")
          .add({
            activo: true,
            nombre: nombre,
            razonSocial: razonSocial,
            rut: rut,
            nCarpeta: nCarpeta,
            direccion: direccion,
            zona: zona,
            telefono: telefono,
            mail: mail,
            deudaActual: deudaActual,
            observaciones: observaciones,
          })
          .then(() => {
            this.setState({
              banderaNuevoCliente: false,
              nombre: "",
              razonSocial: "",
              rut: "",
              nCarpeta: "",
              direccion: "",
              zona: "",
              telefono: "",
              mail: "",
              deudaActual: "",
              observaciones: "",
              cargandoCliente: false,
            });
          })
          .catch((error) => {
            this.setState({ cargandoCliente: false });
            console.log(error);
          });
      });
    }
  };

  guardarEditar = () => {
    let {
      nombreEditar,
      razonSocialEditar,
      rutEditar,
      nCarpetaEditar,
      direccionEditar,
      zonaEditar,
      telefonoEditar,
      mailEditar,
      deudaActualEditar,
      observacionesEditar,
      banderaCliente,
    } = this.state;
    deudaActualEditar = parseFloat(deudaActualEditar);
    if (!deudaActualEditar) {
      deudaActualEditar = 0;
    }
    db.collection("clientes")
      .doc(banderaCliente)
      .update({
        nombre: nombreEditar,
        razonSocial: razonSocialEditar,
        rut: rutEditar,
        nCarpeta: nCarpetaEditar,
        direccion: direccionEditar,
        zona: zonaEditar,
        telefono: telefonoEditar,
        mail: mailEditar,
        deudaActual: deudaActualEditar,
        observaciones: observacionesEditar,
      })
      .then(() => {
        console.log("subido con éxito");
        this.setState({ banderaEditar: false });
      })
      .catch((error) => console.log(error));
  };

  handleClose = () => {
    this.setState({ show: false, id: "" });
  };

  handleShow = (id) => {
    this.setState({ show: true, id: id });
  };

  handleClosePro = () => {
    this.setState({ showPro: false, id: "" });
  };

  handleShowPro = (id) => {
    this.traerProductosCliente(id);
    this.setState({ showPro: true, id: id });
  };

  traerProductosCliente = (id) => {
    this.setState({ cargandoProductosClientes: true }, () => {
      db.collection("clientes")
        .doc(id)
        .collection("listaPrecios")
        .orderBy("orden")
        .onSnapshot((snap) => {
          let productosClientes = [];
          snap.forEach((doc) => {
            productosClientes[doc.id] = doc.data();
          });
          this.setState({
            productosClientes,
            cargandoProductosClientes: false,
          });
        });
    });
  };

  traerProductos = () => {
    db.collection("productos")
      .orderBy("orden")
      .onSnapshot((snap) => {
        let productos = [];
        snap.forEach((doc) => {
          productos[doc.id] = doc.data();
        });
        this.setState({ productos, banderaProductos: true });
      });
  };

  handlePrecio = (e) => {
    let pro = this.state.productos;
    pro[e.target.name].precio = e.target.value;
    this.setState({ productos: pro });
  };

  agregarProducto = () => {
    let { id, productos, producto, precio } = this.state;
    if (producto && precio) {
      precio = parseFloat(precio);
      let pro = { ...productos[producto] };
      pro.precio = precio;
      pro.idProducto = producto;
      db.collection("clientes")
        .doc(id)
        .collection("listaPrecios")
        .doc(producto)
        .set(pro)
        .then(() => {
          console.log("precio producto actualizado");
          this.setState({
            producto: "",
            precio: "",
          });
        })
        .catch((error) => console.log(error));
    }
  };

  eliminarProducto = (idProducto) => {
    const id = this.state.id;
    db.collection("clientes")
      .doc(id)
      .collection("listaPrecios")
      .doc(idProducto)
      .delete()
      .then(() => console.log("precio borrado"))
      .catch((error) => console.log(error));
  };

  checkBuscador = (clientes) => {
    let { buscado } = this.state;
    let clientesMostrar = [];
    buscado = buscado.toLocaleLowerCase();

    Object.keys(clientes).forEach((id) => {
      if (
        clientes[id].nombre.toLowerCase().indexOf(buscado) >= 0 ||
        clientes[id].razonSocial.toLowerCase().indexOf(buscado) >= 0 ||
        buscado === ""
      ) {
        clientesMostrar[id] = { ...clientes[id] };
      }
    });
    this.setState({ clientesMostrar });
  };

  handleChangeBuscador = (e) => {
    let buscado = e.target.value;
    let { clientes } = this.state;

    this.setState({ buscado }, () => {
      this.checkBuscador(clientes);
    });
  };

  render() {
    const {
      clientesMostrar,
      productosActivos,
      productos,
      banderaProductos,
      productosClientes,
    } = this.state;
    const { permisosVendedor } = this.props.usuario;
    return this.state.cargando ? (
      <div
        style={{
          width: "100%",
          height: "200px",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Spinner animation="grow" variant="primary" />
      </div>
    ) : (
      <div className="container">
        <Container style={{ marginTop: "30px" }}>
          <h1>Clientes</h1>
          <Row>
            <Col xs={3}>
              {this.state.banderaNuevoCliente ? (
                <Button
                  variant="outline-danger"
                  style={{ borderColor: "white" }}
                  onClick={this.nuevoCliente}
                >
                  <MdClose color="red" size="2em" />
                </Button>
              ) : (
                <Button
                  variant="outline-success"
                  style={{ borderColor: "white" }}
                  onClick={this.nuevoCliente}
                >
                  <MdPersonAdd color="green" size="2em" />
                </Button>
              )}
            </Col>

            <Col xs={7} style={{ marginTop: "5px" }}>
              <input
                className="form-control"
                value={this.state.buscado}
                onChange={this.handleChangeBuscador}
                type="text"
                name="buscador"
                placeholder="Buscador..."
              />
            </Col>
          </Row>
          {this.state.banderaNuevoCliente ? (
            <Form>
              <Form.Group controlId="formNombre">
                <Form.Label style={this.state.nombre ? null : { color: "red" }}>
                  Cliente
                </Form.Label>
                <input
                  className="form-control"
                  value={this.state.nombre}
                  onChange={this.handleChange}
                  type="text"
                  name="nombre"
                  placeholder="Nombre"
                />
              </Form.Group>
              <Form.Group controlId="formRazonSocial">
                <Form.Label>Razon Social</Form.Label>
                <input
                  className="form-control"
                  value={this.state.razonSocial}
                  onChange={this.handleChange}
                  type="text"
                  name="razonSocial"
                  placeholder="Razon Social"
                />
              </Form.Group>
              <Form.Group controlId="formRut">
                <Form.Label>Rut</Form.Label>
                <input
                  className="form-control"
                  value={this.state.rut}
                  onChange={this.handleChange}
                  type="number"
                  name="rut"
                  placeholder="Rut"
                />
              </Form.Group>
              <Form.Group controlId="formDireccion">
                <Form.Label>Direccion</Form.Label>
                <input
                  className="form-control"
                  value={this.state.direccion}
                  onChange={this.handleChange}
                  type="text"
                  name="direccion"
                  placeholder="Direccion"
                />
              </Form.Group>
              <Form.Group controlId="formnCarpeta">
                <Form.Label>N° Carpeta</Form.Label>
                <input
                  className="form-control"
                  value={this.state.nCarpeta}
                  onChange={this.handleChange}
                  type="number"
                  name="nCarpeta"
                  placeholder="Carpeta"
                />
              </Form.Group>
              <Form.Group>
                <Form.Label style={this.state.zona ? null : { color: "red" }}>
                  Zona
                </Form.Label>
                <select
                  style={{ marginTop: "20px" }}
                  className="form-control"
                  onChange={(e) => this.handleChange(e)}
                  name="zona"
                  value={this.state.zona}
                  aria-label="Small"
                >
                  <option value="">Seleccione zona...</option>
                  <option value="Montevideo">Montevideo</option>
                  <option value="La Costa">La Costa</option>
                </select>
              </Form.Group>
              <Form.Group controlId="formTelefono">
                <Form.Label>Telefono</Form.Label>
                <input
                  className="form-control"
                  value={this.state.telefono}
                  onChange={this.handleChange}
                  type="number"
                  name="telefono"
                  placeholder="Telefono"
                />
              </Form.Group>
              <Form.Group controlId="formMail">
                <Form.Label>e-mail</Form.Label>
                <input
                  className="form-control"
                  value={this.state.mail}
                  onChange={this.handleChange}
                  type="text"
                  name="mail"
                  placeholder="e-mail"
                />
              </Form.Group>
              <Form.Group controlId="formDeudaActual">
                <Form.Label>Deuda Actual</Form.Label>
                <input
                  className="form-control"
                  value={this.state.deudaActual}
                  onChange={this.handleChange}
                  type="number"
                  name="deudaActual"
                  placeholder="Deuda Actual"
                />
              </Form.Group>
              <Form.Group controlId="textareaObservaciones">
                <Form.Label>Observaciones</Form.Label>
                <Form.Control
                  as="textarea"
                  rows="3"
                  type="text"
                  name="observaciones"
                  value={this.state.observaciones}
                  onChange={this.handleChange}
                />
              </Form.Group>
              {!this.state.cargandoCliente ? (
                this.state.nombre && this.state.zona ? (
                  <Button
                    variant="primary"
                    type="button"
                    onClick={this.guardarNuevo}
                  >
                    Agregar
                  </Button>
                ) : null
              ) : (
                <Button variant="light" disabled>
                  <Spinner
                    as="span"
                    animation="grow"
                    size="sm"
                    role="status"
                    aria-hidden="true"
                  />
                  Cargando...
                </Button>
              )}
            </Form>
          ) : null}
          <ListGroup variant="flush" style={{ marginTop: "15px" }}>
            {Object.keys(clientesMostrar).map((id) => {
              return (
                <ListGroup.Item
                  key={id}
                  variant={this.state.banderaCliente === id ? "success" : null}
                >
                  <Row>
                    <Col xs={2}>
                      <Form onChange={(e) => this.handleChangeCheck(e)}>
                        <Form.Check
                          type="switch"
                          id={id}
                          label=""
                          checked={clientesMostrar[id].activo}
                        />
                      </Form>
                    </Col>
                    <Col
                      onClick={() => this.banderaCliente(id)}
                      xs={8}
                      style={
                        clientesMostrar[id].activo ? null : { color: "red" }
                      }
                    >
                      <p
                        id={id}
                      >{`${clientesMostrar[id].nombre} (${clientesMostrar[id].razonSocial})`}</p>
                    </Col>
                    <Col xs={2}>
                      <MdDelete
                        color="red"
                        size="1.5em"
                        onClick={() => this.handleShow(id)}
                      />
                    </Col>
                  </Row>
                  {this.state.banderaCliente === id ? (
                    <ListGroup variant="flush">
                      <ListGroup.Item variant="success">
                        <Row>
                          <Col xs={4}>
                            <Button
                              variant="outline-dark"
                              onClick={() =>
                                this.editar(
                                  clientesMostrar[id].nombre,
                                  clientesMostrar[id].razonSocial,
                                  clientesMostrar[id].rut,
                                  clientesMostrar[id].nCarpeta,
                                  clientesMostrar[id].direccion,
                                  clientesMostrar[id].zona,
                                  clientesMostrar[id].telefono,
                                  clientesMostrar[id].mail,
                                  clientesMostrar[id].deudaActual,
                                  clientesMostrar[id].observaciones
                                )
                              }
                            >
                              <FiEdit size="1.5em" />
                            </Button>
                          </Col>
                          <Col xs={{ span: 4, offset: 4 }}>
                            <Button
                              variant="outline-dark"
                              onClick={() => this.handleShowPro(id)}
                            >
                              <BsListCheck size="1.5em" />
                            </Button>
                          </Col>
                        </Row>
                        <Row style={{ marginTop: "20px", textAlign: "left" }}>
                          <Col xs={12}>
                            <p>
                              <strong>Rut:</strong>
                              {` ${clientesMostrar[id].rut}`}
                            </p>
                          </Col>
                          <Col xs={12}>
                            <p>
                              <strong>Direccion:</strong>
                              {` ${clientesMostrar[id].direccion}`}
                            </p>
                          </Col>
                          <Col xs={6}>
                            <p>
                              <strong>Telefono:</strong>
                              {` ${clientesMostrar[id].telefono}`}
                            </p>
                          </Col>
                          <Col xs={6}>
                            <p>
                              <strong>e-mail:</strong>
                              {` ${clientesMostrar[id].mail}`}
                            </p>
                          </Col>
                          <Col xs={6}>
                            <p>
                              <strong>N° Carpeta:</strong>
                              {` ${clientesMostrar[id].nCarpeta}`}
                            </p>
                          </Col>
                          <Col xs={6}>
                            <p>
                              <strong>Total Adeudado:</strong>
                              {` $${clientesMostrar[
                                id
                              ].deudaActual.toLocaleString("de-DE")}`}
                            </p>
                          </Col>
                          <Col xs={12}>
                            <p>
                              <strong>Zona:</strong>
                              {` ${clientesMostrar[id].zona}`}
                            </p>
                          </Col>
                          <Col xs={12}>
                            <p>
                              <strong>Observaciones:</strong>
                              {` ${clientesMostrar[id].observaciones}`}
                            </p>
                          </Col>
                        </Row>
                        {this.state.banderaEditar ? (
                          <div style={{ marginTop: "20px" }}>
                            <h5>Editar</h5>
                            <Form>
                              <Form.Group controlId="formNombreEditar">
                                <Form.Label
                                  style={
                                    this.state.nombreEditar
                                      ? null
                                      : { color: "red" }
                                  }
                                >
                                  Cliente
                                </Form.Label>
                                <input
                                  className="form-control"
                                  value={this.state.nombreEditar}
                                  onChange={this.handleChange}
                                  type="text"
                                  name="nombreEditar"
                                  placeholder="Nombre"
                                />
                              </Form.Group>
                              <Form.Group controlId="formRazonSocialEditar">
                                <Form.Label>Razon Social</Form.Label>
                                <input
                                  className="form-control"
                                  value={this.state.razonSocialEditar}
                                  onChange={this.handleChange}
                                  type="text"
                                  name="razonSocialEditar"
                                  placeholder="Razon Social"
                                />
                              </Form.Group>
                              <Form.Group controlId="formRutEditar">
                                <Form.Label>Rut</Form.Label>
                                <input
                                  className="form-control"
                                  value={this.state.rutEditar}
                                  onChange={this.handleChange}
                                  type="number"
                                  name="rutEditar"
                                  placeholder="Rut"
                                />
                              </Form.Group>
                              <Form.Group controlId="formDireccionEditar">
                                <Form.Label>Direccion</Form.Label>
                                <input
                                  className="form-control"
                                  value={this.state.direccionEditar}
                                  onChange={this.handleChange}
                                  type="text"
                                  name="direccionEditar"
                                  placeholder="Direccion"
                                />
                              </Form.Group>
                              <Form.Group controlId="formnCarpetaEditar">
                                <Form.Label>N° Carpeta</Form.Label>
                                <input
                                  className="form-control"
                                  value={this.state.nCarpetaEditar}
                                  onChange={this.handleChange}
                                  type="number"
                                  name="nCarpetaEditar"
                                  placeholder="Carpeta"
                                />
                              </Form.Group>
                              <Form.Group controlId="formZonaEditar">
                                <Form.Label
                                  style={
                                    this.state.zonaEditar
                                      ? null
                                      : { color: "red" }
                                  }
                                >
                                  Zona
                                </Form.Label>
                                <select
                                  style={{ marginTop: "20px" }}
                                  className="form-control"
                                  onChange={(e) => this.handleChange(e)}
                                  name="zonaEditar"
                                  value={this.state.zonaEditar}
                                  aria-label="Small"
                                >
                                  <option value="">Seleccione zona...</option>
                                  <option value="Montevideo">Montevideo</option>
                                  <option value="La Costa">La Costa</option>
                                </select>
                              </Form.Group>
                              <Form.Group controlId="formTelefonoEditar">
                                <Form.Label>Telefono</Form.Label>
                                <input
                                  className="form-control"
                                  value={this.state.telefonoEditar}
                                  onChange={this.handleChange}
                                  type="number"
                                  name="telefonoEditar"
                                  placeholder="Telefono"
                                />
                              </Form.Group>
                              <Form.Group controlId="formMailEditar">
                                <Form.Label>e-mail</Form.Label>
                                <input
                                  className="form-control"
                                  value={this.state.mailEditar}
                                  onChange={this.handleChange}
                                  type="text"
                                  name="mailEditar"
                                  placeholder="e-mail"
                                />
                              </Form.Group>
                              <Form.Group controlId="formDeudaActualEditar">
                                <Form.Label>Deuda Actual</Form.Label>
                                <input
                                  className="form-control"
                                  value={this.state.deudaActualEditar}
                                  onChange={this.handleChange}
                                  type="number"
                                  name="deudaActualEditar"
                                  placeholder="Deuda Actual"
                                />
                              </Form.Group>
                              <Form.Group controlId="textareaObservacionesEditar">
                                <Form.Label>Observaciones</Form.Label>
                                <Form.Control
                                  as="textarea"
                                  rows="3"
                                  type="text"
                                  name="observacionesEditar"
                                  value={this.state.observacionesEditar}
                                  onChange={this.handleChange}
                                />
                              </Form.Group>
                              {!this.state.cargandoClienteEditar ? (
                                this.state.nombreEditar &&
                                this.state.zonaEditar ? (
                                  <Button
                                    variant="warning"
                                    type="button"
                                    onClick={this.guardarEditar}
                                  >
                                    Editar
                                  </Button>
                                ) : null
                              ) : (
                                <Button variant="light" disabled>
                                  <Spinner
                                    as="span"
                                    animation="grow"
                                    size="sm"
                                    role="status"
                                    aria-hidden="true"
                                  />
                                  Cargando...
                                </Button>
                              )}
                            </Form>
                          </div>
                        ) : null}
                      </ListGroup.Item>
                    </ListGroup>
                  ) : null}
                </ListGroup.Item>
              );
            })}
          </ListGroup>
        </Container>

        <Modal show={this.state.showPro} onHide={this.handleClosePro}>
          <Modal.Header closeButton>
            <Modal.Title>Productos</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Row>
              <Form style={{ width: "100%" }}>
                <Col xs={12}>
                  <Form.Group>
                    <Form.Label>Producto</Form.Label>
                    <select
                      className="form-control"
                      onChange={(e) => this.handleChange(e)}
                      name="producto"
                      value={this.state.producto}
                      aria-label="Small"
                    >
                      <option value="">Seleccione producto...</option>
                      {Object.keys(productos).map((id) => {
                        return (
                          <option value={id}>{productos[id].producto}</option>
                        );
                      })}
                    </select>
                  </Form.Group>
                  <Form.Group>
                    <Form.Label>Precio</Form.Label>
                    <input
                      name="precio"
                      className="form-control"
                      type="number"
                      placeholder="Precio"
                      onChange={(e) => this.handleChange(e)}
                      value={this.state.precio}
                    />
                  </Form.Group>
                </Col>
                {this.state.producto && this.state.precio > 0 ? (
                  <Col xs={12} style={{ textAlign: "right" }}>
                    <Button
                      variant="outline-success"
                      style={{ marginBottom: "15px" }}
                      onClick={this.agregarProducto}
                    >
                      Agregar
                    </Button>
                  </Col>
                ) : null}
              </Form>
            </Row>

            {this.state.cargandoProductosClientes ? (
              <div
                style={{
                  width: "100%",
                  height: "200px",
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <Spinner animation="grow" variant="primary" />
              </div>
            ) : (
              <ListGroup variant="flush">
                <ListGroup.Item>
                  <Row>
                    <Col xs={8}>
                      <strong>Producto</strong>
                    </Col>
                    <Col xs={4}>
                      <strong>Precio</strong>
                    </Col>
                  </Row>
                </ListGroup.Item>
                {Object.keys(productosClientes).map((id) => {
                  return (
                    <ListGroup.Item>
                      <Row>
                        <Col xs={6}>{productosClientes[id].producto}</Col>
                        <Col xs={4}>
                          {`$${productosClientes[id].precio.toLocaleString(
                            "de-DE"
                          )}`}
                        </Col>
                        <Col xs={2}>
                          <Button
                            variant="outline-danger"
                            onClick={() => this.eliminarProducto(id)}
                          >
                            <MdClose color="red" size="1.5em" />
                          </Button>
                        </Col>
                      </Row>
                    </ListGroup.Item>
                  );
                })}
              </ListGroup>
            )}
          </Modal.Body>
          <Modal.Footer>
            <Button variant="outline-success" onClick={this.handleClosePro}>
              OK
            </Button>
          </Modal.Footer>
        </Modal>

        <Modal show={this.state.show} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>ATENCIÓN!!!</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            Estás seguro que quieres ELIMINAR este cliente?!
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleClose}>
              Cancelar
            </Button>
            <Button
              variant="danger"
              onClick={() => {
                this.borrar(this.state.id);
              }}
            >
              ELIMINAR
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

export default Clientes;
