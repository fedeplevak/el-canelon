import React, { Component, Fragment } from "react";
import {
  Table,
  Button,
  Spinner,
  Container,
  Row,
  Col,
  Modal,
  Form,
  Tab,
  Nav,
} from "react-bootstrap";
import jsPDF from "jspdf";
import { db } from "../firebaseConfig";

class EstadoCuentaCompras extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cargando: false,
      fecha: "",
      desde: "",
      deudaInicial: "",
      deudaActual: "",
      documentos: [],
    };
  }

  componentDidMount() {
    this.hoy();
  }

  hoy = () => {
    let hoy = new Date();

    let año = hoy.getFullYear();
    let mes =
      hoy.getMonth() + 1 > 9 ? hoy.getMonth() + 1 : `0${hoy.getMonth() + 1}`;
    let dia = hoy.getDate() > 9 ? hoy.getDate() : `0${hoy.getDate()}`;

    const fecha = `${año}-${mes}-${dia}`;
    this.setState({ fecha });
  };

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  estadoCuentaImprimir = () => {
    let doc = new jsPDF("p", "mm", [58, 180]);

    const { fecha } = this.state;

    const { vendedorNombre } = this.props;

    const { documentos, deudaInicial, proveedorNombre } = this.props.estado;

    const { empresa, telefono } = this.props.empresa;

    if (documentos.length > 0) {
      doc.setFontSize(5);
      doc.text(6, 2, `Distri ${empresa}`);
      doc.setFontSize(2);
      doc.text(10, 3.5, `Tel: ${telefono}`);

      doc.setFontSize(2.5);
      doc.text(0.3, 5, `Fecha: ${fecha}`);
      doc.text(0.3, 8, `Proveedor: ${proveedorNombre}`);
      doc.text(0.3, 10, `Vendedor: ${vendedorNombre.vendedor}`);

      doc.text(6, 13, `Estado de Cuenta`);

      let co = 16;

      doc.setFontSize(2.3);
      doc.text(1, co, `Fecha`);
      doc.text(4.9, co, `Nº Doc`);
      doc.text(8.15, co, `Vendedor`);
      doc.text(12.25, co, `Importe`);
      doc.text(17, co, `Saldo`);
      co = co + 0.2;
      doc.setLineWidth(0.08);
      doc.line(0.3, co, 20, co);
      doc.setFontSize(2.3);
      co = co + 1.3;
      doc.text(
        10.5,
        co,
        `Deuda Inicial: ${deudaInicial.toLocaleString("de-DE")}`
      );
      co = co + 0.6;
      Object.keys(documentos).forEach((id) => {
        co = co + 1.3;
        doc.text(0.25, co, `${documentos[id].fecha}`);
        doc.text(5, co, `${documentos[id].nDocumento}`);
        doc.text(8.25, co, `${documentos[id].vendedor}`);
        doc.text(12.25, co, `${documentos[id].total.toLocaleString("de-DE")}`);
        doc.text(15.75, co, `${documentos[id].saldo.toLocaleString("de-DE")}`);
      });

      doc.save(`${proveedorNombre}-${fecha}.pdf`);
    }
  };

  estadoCuentaDesde = () => {
    this.setState({ cargando: true }, async () => {
      let { proveedor } = this.props.estado;
      let { desde } = this.state;

      if (!desde) {
        let snapProveedor = await db
          .collection("proveedores")
          .doc(proveedor)
          .get();
        let deudaInicial = snapProveedor.data().deudaCambio;
        if (!deudaInicial) {
          deudaInicial = 0;
        }

        let snap = await db
          .collection("comprasCabezera")
          .where("proveedor", "==", proveedor)
          .where("estadoCuenta", "==", true)
          .orderBy("fecha", "desc")
          .orderBy("documento", "desc")
          .orderBy("nDocumento", "desc")
          .get();

        let documentos = [];
        snap.forEach((doc) => {
          documentos.splice(0, 0, doc.data());
        });

        let deudaActual = await this.deudaActual(documentos, deudaInicial);

        let docs = await this.saldo(documentos, deudaInicial);

        this.setState(
          {
            deudaInicial,
            deudaActual,
            documentos: docs,
            cargando: false,
          },
          () => {
            db.collection("proveedores")
              .doc(proveedor)
              .update({
                deudaActual: deudaActual,
              })
              .then(() => console.log("deuda actual actualizada"))
              .catch((error) => console.log(error));
          }
        );
      } else {
        let snapProveedor = await db
          .collection("proveedores")
          .doc(proveedor)
          .get();
        let deudaActual = snapProveedor.data().deudaActual;
        if (!deudaActual) {
          deudaActual = 0;
        }

        let snap = await db
          .collection("comprasCabezera")
          .where("proveedor", "==", proveedor)
          .where("fecha", ">=", desde)
          .where("estadoCuenta", "==", true)
          .orderBy("fecha", "desc")
          .orderBy("documento", "desc")
          .orderBy("nDocumento", "desc")
          .get();

        let documentos = [];
        snap.forEach((doc) => {
          documentos.splice(0, 0, doc.data());
        });

        let deudaInicial = await this.deudaInicial(documentos, deudaActual);

        let docs = await this.saldo(documentos, deudaInicial);

        this.setState({
          deudaInicial,
          deudaActual,
          documentos: docs,
          cargando: false,
        });
      }
    });
  };

  saldo = (documentos, deudaInicial) => {
    let saldo = deudaInicial;
    Object.keys(documentos).forEach((id) => {
      let total = parseFloat(documentos[id].total);
      saldo = saldo + total;
      documentos[id].saldo = saldo;
    });
    return documentos;
  };

  deudaInicial = (documentos, deudaActual) => {
    let deudaInicial = deudaActual;
    Object.keys(documentos).forEach((id) => {
      let total = parseFloat(documentos[id].total);
      deudaInicial = deudaInicial - total;
    });
    return deudaInicial;
  };

  deudaActual = (documentos, deudaInicial) => {
    let deudaActual = deudaInicial;
    Object.keys(documentos).forEach((id) => {
      let total = parseFloat(documentos[id].total);
      deudaActual = deudaActual + total;
    });
    return deudaActual;
  };

  estadoCuentaImprimirCompleto = () => {
    let doc = new jsPDF("p", "mm", [58, 180]);

    const { fecha, documentos, deudaInicial } = this.state;

    const { vendedorNombre } = this.props;

    const { proveedorNombre } = this.props.estado;

    const { empresa, telefono } = this.props.empresa;

    if (documentos.length > 0) {
      doc.setFontSize(5);
      doc.text(6, 2, `Distri ${empresa}`);
      doc.setFontSize(2);
      doc.text(10, 3.5, `Tel: ${telefono}`);

      doc.setFontSize(2.5);
      doc.text(0.3, 5, `Fecha: ${fecha}`);
      doc.text(0.3, 8, `Proveedor: ${proveedorNombre}`);
      doc.text(0.3, 10, `Vendedor: ${vendedorNombre.vendedor}`);

      doc.text(6, 13, `Estado de Cuenta`);

      let co = 16;

      doc.setFontSize(2.3);
      doc.text(1, co, `Fecha`);
      doc.text(4.9, co, `Nº Doc`);
      doc.text(8.15, co, `Vendedor`);
      doc.text(12.25, co, `Importe`);
      doc.text(17, co, `Saldo`);
      co = co + 0.2;
      doc.setLineWidth(0.08);
      doc.line(0.3, co, 20, co);
      doc.setFontSize(2.3);
      co = co + 1.3;
      doc.text(
        10.5,
        co,
        `Deuda Inicial: ${deudaInicial.toLocaleString("de-DE")}`
      );
      co = co + 0.6;
      let alto = 0;
      Object.keys(documentos).forEach((id) => {
        alto = alto + 1;
        if (alto > 34) {
          doc.addPage();
          alto = 0;
          co = 1;
        }
        co = co + 1.3;
        doc.text(0.25, co, `${documentos[id].fecha}`);
        doc.text(5, co, `${documentos[id].nDocumento}`);
        doc.text(8.25, co, `${documentos[id].vendedor}`);
        doc.text(12.25, co, `${documentos[id].total.toLocaleString("de-DE")}`);
        doc.text(15.75, co, `${documentos[id].saldo.toLocaleString("de-DE")}`);
      });

      doc.save(`${proveedorNombre}-${fecha}.pdf`);
    }
  };

  render() {
    const {
      documentos,
      cargandoEstadoCuenta,
      deudaInicial,
      proveedor,
    } = this.props.estado;

    const docs = this.state.documentos;
    const deudaInicialCompleto = this.state.deudaInicial;

    const { cargando } = this.state;

    return (
      <div>
        <div className="container" style={{ marginTop: "15px" }}>
          <Tab.Container id="listado" defaultActiveKey="basico">
            <Nav fill variant="tabs">
              <Nav.Item>
                <Nav.Link eventKey="basico">Básico</Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link eventKey="completo">Completo</Nav.Link>
              </Nav.Item>
            </Nav>
            <Tab.Content>
              <Tab.Pane eventKey="basico">
                <Container>
                  <Row>
                    <Col>
                      <h1 style={{ padding: "15px" }}>Estado de Cuenta</h1>
                    </Col>
                    <Col>
                      {!cargandoEstadoCuenta ? (
                        <Button
                          variant="outline-primary"
                          disabled={documentos.length > 0 ? false : true}
                          style={{ height: "42px", marginTop: "29px" }}
                          onClick={this.estadoCuentaImprimir}
                        >
                          Imprimir
                        </Button>
                      ) : null}
                    </Col>
                  </Row>
                </Container>
                {cargandoEstadoCuenta ? (
                  <div
                    style={{
                      width: "100%",
                      height: "200px",
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <Spinner animation="grow" variant="primary" />
                  </div>
                ) : (
                  <div
                    style={{
                      width: "100%",
                      overflow: "scroll",
                      marginTop: "20px",
                    }}
                  >
                    <Table striped bordered condensed hover responsive>
                      <thead>
                        <tr>
                          <th>Fecha</th>
                          <th>Proveedor</th>
                          <th>Documento</th>
                          <th>Nº</th>
                          <th>Importe</th>
                          <th>Saldo</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td />
                          <td />
                          <td />
                          <td />
                          <td>Deuda Inicial</td>
                          <td>
                            <strong>
                              {deudaInicial.toLocaleString("de-DE")}
                            </strong>
                          </td>
                        </tr>
                        {Object.keys(documentos).length > 0 ? (
                          Object.keys(documentos).map((id) => {
                            return (
                              <Fragment>
                                <tr
                                  key={id}
                                  className={
                                    this.state.idFactura === id
                                      ? this.state.claseDoc
                                      : ""
                                  }
                                >
                                  <td>{documentos[id].fecha}</td>
                                  <td>{documentos[id].proveedorNombre}</td>
                                  <td>{documentos[id].documento}</td>
                                  <td>{documentos[id].nDocumento}</td>
                                  <td>
                                    {documentos[id].total.toLocaleString(
                                      "de-DE"
                                    )}
                                  </td>
                                  <td>
                                    <strong>
                                      {documentos[id].saldo.toLocaleString(
                                        "de-DE"
                                      )}
                                    </strong>
                                  </td>
                                </tr>
                              </Fragment>
                            );
                          })
                        ) : (
                          <tr>
                            <td colSpan="8">
                              <p>NO se encontraron resultados</p>
                            </td>
                          </tr>
                        )}
                      </tbody>
                    </Table>
                  </div>
                )}
              </Tab.Pane>

              <Tab.Pane eventKey="completo">
                <Container>
                  <Row>
                    <Col>
                      <h1 style={{ padding: "15px" }}>Estado de Cuenta</h1>
                    </Col>
                    <Col>
                      {!cargandoEstadoCuenta ? (
                        <Button
                          variant="outline-primary"
                          disabled={documentos.length > 0 ? false : true}
                          style={{ height: "42px", marginTop: "29px" }}
                          onClick={this.estadoCuentaImprimirCompleto}
                        >
                          Imprimir
                        </Button>
                      ) : null}
                    </Col>
                  </Row>
                  <Row>
                    <Col>
                      <Form.Group>
                        <Form.Label>Desde</Form.Label>
                        <input
                          name="desde"
                          className="form-control text-center"
                          placeholder="Todo"
                          type="date"
                          onChange={(e) => this.handleChange(e)}
                          value={this.state.desde}
                        />
                      </Form.Group>
                    </Col>
                    <Col>
                      <Button
                        variant="success"
                        onClick={this.estadoCuentaDesde}
                        style={{ marginTop: "31px" }}
                        disabled={!proveedor ? true : false}
                      >
                        Buscar
                      </Button>
                    </Col>
                  </Row>
                </Container>
                {cargando ? (
                  <div
                    style={{
                      width: "100%",
                      height: "200px",
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <Spinner animation="grow" variant="primary" />
                  </div>
                ) : (
                  <div
                    style={{
                      width: "100%",
                      overflow: "scroll",
                      marginTop: "20px",
                    }}
                  >
                    <Table striped bordered condensed hover responsive>
                      <thead>
                        <tr>
                          <th>Fecha</th>
                          <th>Proveedor</th>
                          <th>Documento</th>
                          <th>Nº</th>
                          <th>Importe</th>
                          <th>Saldo</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td />
                          <td />
                          <td />
                          <td />
                          <td>Deuda Inicial</td>
                          <td>
                            <strong>
                              {deudaInicialCompleto.toLocaleString("de-DE")}
                            </strong>
                          </td>
                        </tr>
                        {Object.keys(docs).length > 0 ? (
                          Object.keys(docs).map((id) => {
                            return (
                              <Fragment>
                                <tr
                                  key={id}
                                  id={id}
                                  className={
                                    this.state.idFactura === id
                                      ? this.state.claseDoc
                                      : ""
                                  }
                                >
                                  <td>{docs[id].fecha}</td>
                                  <td>{docs[id].proveedorNombre}</td>
                                  <td>{docs[id].documento}</td>
                                  <td>{docs[id].nDocumento}</td>
                                  <td>
                                    {docs[id].total.toLocaleString("de-DE")}
                                  </td>
                                  <td>
                                    <strong>
                                      {docs[id].saldo.toLocaleString("de-DE")}
                                    </strong>
                                  </td>
                                </tr>
                              </Fragment>
                            );
                          })
                        ) : (
                          <tr>
                            <td colSpan="8">
                              <p>NO se encontraron resultados</p>
                            </td>
                          </tr>
                        )}
                      </tbody>
                    </Table>
                  </div>
                )}
              </Tab.Pane>
            </Tab.Content>
          </Tab.Container>
        </div>

        <Modal show={this.state.show} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>ATENCIÓN!!!</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            Estás seguro que quieres ELIMINAR este documento?!
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleClose}>
              Cancelar
            </Button>
            <Button
              variant="danger"
              onClick={() => {
                this.borrar();
              }}
            >
              ELIMINAR
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

export default EstadoCuentaCompras;
