import React, { Component } from "react";
import { db } from "../firebaseConfig";
import {
  Container,
  Table,
  ListGroup,
  Col,
  Row,
  Spinner,
} from "react-bootstrap";
import hoy from "../funciones/hoy";

class ListadoCreditos extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hoy: "",
      clientes: [],
      totalCredito: 0,
      cargandoClientes: true,
    };
  }

  componentDidMount() {
    this.calcularHoy();
    this.traerClientes();
  }

  calcularHoy = async () => {
    let today = await hoy();
    this.setState({ desde: today, hasta: today });
    return today;
  };

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  traerClientes = () => {
    db.collection("clientes")
      .orderBy("deudaActual", "desc")
      .orderBy("nombre")
      .onSnapshot((snap) => {
        let clientes = [];
        let totalCredito = 0;
        snap.forEach((doc) => {
          clientes[doc.id] = doc.data();
          if (!clientes[doc.id].deudaActual) {
            clientes[doc.id].deudaActual = 0;
          }
          totalCredito =
            totalCredito + parseFloat(clientes[doc.id].deudaActual);
        });
        this.setState({ clientes, totalCredito, cargandoClientes: false });
      });
  };

  render() {
    let { clientes, cargandoClientes, totalCredito } = this.state;
    return (
      <Container>
        <Row>
          <Col>
            <h1 style={{ marginTop: "30px" }}>Listado Créditos</h1>
          </Col>
        </Row>

        {cargandoClientes ? (
          <div
            style={{
              width: "100%",
              height: "200px",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Spinner animation="grow" variant="primary" />
          </div>
        ) : (
          <div>
            <div style={{ textAlign: "right", marginTop: "30px" }}>
              <Row>
                <Col>
                  <h5>
                    {`Créditos Totales: $${totalCredito.toLocaleString(
                      "de-DE"
                    )}`}
                  </h5>
                </Col>
              </Row>
            </div>

            <ListGroup variant="flush" style={{ marginTop: "15px" }}>
              <Table responsive>
                <thead>
                  <tr>
                    <th>Cliente</th>
                    <th style={{ textAlign: "right" }}>Total Adeudado</th>
                  </tr>
                </thead>
                <tbody>
                  {Object.keys(clientes).length > 0 ? (
                    Object.keys(clientes).map((id) => {
                      return (
                        <tr key={id} id={id}>
                          <td style={{ textAlign: "left" }}>
                            {clientes[id].nombre}
                          </td>
                          <td style={{ textAlign: "right" }}>
                            {`$${clientes[id].deudaActual.toLocaleString(
                              "de-DE"
                            )}`}
                          </td>
                        </tr>
                      );
                    })
                  ) : (
                    <tr>
                      <td colSpan="2">
                        <p>NO se encontraron resultados!</p>
                      </td>
                    </tr>
                  )}
                </tbody>
              </Table>
            </ListGroup>
          </div>
        )}
      </Container>
    );
  }
}
export default ListadoCreditos;
