import React, { Component } from "react";
import { db } from "../firebaseConfig";
import {
  Container,
  Table,
  ListGroup,
  Form,
  Col,
  Row,
  Spinner,
} from "react-bootstrap";
import hoy from "../funciones/hoy";
import hora from "../funciones/hora";
import { FaEdit, FaCheck } from "react-icons/fa";
import { MdClose } from "react-icons/md";

class Stock extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hoy: "",
      hora: "",
      lugares: [],
      idLugar: "",
      cantidadTotal: 0,
      cargandoProductos: true,
      buscado: "",
      productos: [],
      productosMostrar: [],
      banderaEditar: false,
    };
  }

  componentDidMount() {
    this.calcularHoy();
    this.traerLugares();
  }

  calcularHoy = async () => {
    let today = await hoy();
    let hour = await hora();
    this.setState({ hoy: today, hora: hour });
    return today;
  };

  traerLugares = () => {
    db.collection("lugares").onSnapshot((snap) => {
      let lugares = [];
      snap.forEach((doc) => {
        lugares[doc.id] = doc.data();
      });
      this.setState({ lugares, cargandoProductos: false });
    });
  };

  handleChange = (e) => {
    if (e.target.name === "idLugar") {
      let valor = e.target.value;
      this.setState(
        { [e.target.name]: e.target.value, cargandoProductos: true },
        () => {
          if (valor) {
            this.traerProductos();
          } else {
            this.setState({
              productos: [],
              productosMostrar: [],
              cantidadTotal: 0,
              cargandoProductos: false,
            });
          }
        }
      );
    } else {
      this.setState({ [e.target.name]: e.target.value });
    }
  };

  traerProductos = async () => {
    let { idLugar } = this.state;
    if (idLugar) {
      let snapProductos = await db
        .collection("productos")
        .orderBy("orden")
        .orderBy("producto")
        .get();
      let productos = [];
      snapProductos.forEach((doc) => {
        productos[doc.id] = doc.data();
        productos[doc.id].stock = 0;
        productos[doc.id].stockNuevo = 0;
      });

      db.collection("lugares")
        .doc(idLugar)
        .collection("productos")
        .orderBy("orden")
        .orderBy("producto")
        .onSnapshot((snap) => {
          let productosStock = [];
          let cantidadTotal = 0;
          snap.forEach((doc) => {
            let key = doc.id;
            productosStock[doc.id] = doc.data();
            cantidadTotal =
              cantidadTotal + parseFloat(productosStock[doc.id].stock);
            Object.keys(productos).forEach((id) => {
              if (id === key) {
                productos[id].stock = parseFloat(productosStock[key].stock);
                productos[id].stockNuevo = parseFloat(
                  productosStock[key].stock
                );
              }
            });
          });
          this.checkBuscador(productos);
          this.setState({ productos, cantidadTotal, cargandoProductos: false });
        });
    }
  };

  checkBuscador = (productos) => {
    let { buscado } = this.state;
    let productosMostrar = [];
    buscado = buscado.toLocaleLowerCase();

    Object.keys(productos).forEach((id) => {
      if (
        productos[id].producto.toLowerCase().indexOf(buscado) >= 0 ||
        productos[id].codigo.toLowerCase().indexOf(buscado) >= 0 ||
        buscado === ""
      ) {
        productosMostrar[id] = { ...productos[id] };
      }
    });
    this.setState({ productosMostrar });
  };

  handleChangeBuscador = (e) => {
    let buscado = e.target.value;
    let { productos } = this.state;

    this.setState({ buscado }, () => {
      this.checkBuscador(productos);
    });
  };

  banderaEditar = () => {
    let { banderaEditar } = this.state;
    this.setState({ banderaEditar: !banderaEditar });
  };

  handleChangeStock = (e, id) => {
    let { productos } = this.state;
    let valor = e.target.value;
    productos[id].stockNuevo = valor;
    this.checkBuscador(productos);
  };

  confirmarStock = () => {
    this.setState({ cargandoProductos: true }, () => {
      let { productos, idLugar } = this.state;
      let contador = 0;
      Object.keys(productos).forEach((id) => {
        if (productos[id].stock !== productos[id].stockNuevo) {
          contador = contador + 1;
          let stockNuevo = parseFloat(productos[id].stockNuevo);
          db.collection("lugares")
            .doc(idLugar)
            .collection("productos")
            .doc(id)
            .set({
              ...productos[id],
              stock: stockNuevo,
            })
            .then((ref) => {
              this.setState({ banderaEditar: false, cargandoProductos: false });
              console.log(`stock de ${id} actualizado`);
            })
            .catch((error) => console.log(error));
        }
      });
      if (contador === 0) {
        this.setState({ banderaEditar: false, cargandoProductos: false });
      }
    });
  };

  render() {
    let {
      productosMostrar,
      cargandoProductos,
      lugares,
      cantidadTotal,
    } = this.state;
    return (
      <Container>
        <Row>
          <Col>
            <h1 style={{ marginTop: "30px" }}>Stock</h1>
          </Col>
        </Row>

        <Row>
          <Col>
            <Form.Group>
              <Form.Label>Lugar</Form.Label>
              <select
                style={{ marginTop: "20px" }}
                className="form-control"
                onChange={(e) => this.handleChange(e)}
                name="idLugar"
                value={this.state.idLugar}
                aria-label="Small"
              >
                <option value="">Selecciona Lugar...</option>
                {Object.keys(lugares).map((id) => {
                  return <option value={id}>{lugares[id].nombre}</option>;
                })}
              </select>
            </Form.Group>
            <Form.Group>
              <Form.Label>Buscador</Form.Label>
              <input
                className="form-control"
                value={this.state.buscado}
                onChange={this.handleChangeBuscador}
                type="text"
                name="buscado"
                placeholder="Buscador..."
              />
            </Form.Group>
          </Col>
        </Row>
        {cargandoProductos ? (
          <div
            style={{
              width: "100%",
              height: "200px",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Spinner animation="grow" variant="primary" />
          </div>
        ) : (
          <div>
            <div style={{ marginTop: "30px" }}>
              <Row>
                <Col xs={6} style={{ textAlign: "left" }}>
                  <h5 onClick={this.calcularHoy}>
                    {`${this.state.hoy.split('-').reverse().join('/')} ${this.state.hora}`}
                  </h5>
                </Col>
                <Col xs={6} style={{ textAlign: "right" }}>
                  <h5>
                    {`Cantidad Total: ${cantidadTotal.toLocaleString("de-DE")}`}
                  </h5>
                </Col>
              </Row>
            </div>

            <ListGroup variant="flush" style={{ marginTop: "15px" }}>
              <Table responsive>
                <thead>
                  <tr>
                    <th>
                      {this.props.vendedor.permisosVendedor &&
                      Object.keys(productosMostrar).length > 0 ? (
                        this.state.banderaEditar ? (
                          <FaCheck
                            color="green"
                            size="1.5em"
                            onClick={this.confirmarStock}
                          />
                        ) : null
                      ) : null}
                    </th>
                    <th>Productos</th>
                    <th style={{ textAlign: "right" }}>Stock</th>
                    <th>
                      {this.props.vendedor.permisosVendedor &&
                      Object.keys(productosMostrar).length > 0 ? (
                        !this.state.banderaEditar ? (
                          <FaEdit size="1.5em" onClick={this.banderaEditar} />
                        ) : (
                          <MdClose
                            color="red"
                            size="1.5em"
                            onClick={this.banderaEditar}
                          />
                        )
                      ) : null}
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {Object.keys(productosMostrar).length > 0 ? (
                    Object.keys(productosMostrar).map((id) => {
                      return (
                        <tr key={id} id={id}>
                          <td colSpan={2} style={{ textAlign: "left" }}>
                            {productosMostrar[id].producto}
                          </td>
                          {!this.state.banderaEditar ? (
                            <td style={{ textAlign: "right" }}>
                              {`${productosMostrar[id].stock.toLocaleString(
                                "de-DE"
                              )}`}
                            </td>
                          ) : (
                            <td colSpan={2} style={{ textAlign: "right" }}>
                              <input
                                style={
                                  productosMostrar[id].stock !==
                                  productosMostrar[id].stockNuevo
                                    ? { textAlign: "right", color: "red" }
                                    : { textAlign: "right" }
                                }
                                className="form-control"
                                value={productosMostrar[id].stockNuevo}
                                onChange={(e) => this.handleChangeStock(e, id)}
                                type="number"
                                name={id}
                                placeholder="Stock..."
                              />
                            </td>
                          )}
                        </tr>
                      );
                    })
                  ) : (
                    <tr>
                      <td colSpan="2">
                        <p>NO se encontraron resultados!</p>
                      </td>
                    </tr>
                  )}
                </tbody>
              </Table>
            </ListGroup>
          </div>
        )}
      </Container>
    );
  }
}
export default Stock;
