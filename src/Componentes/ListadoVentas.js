import React, { Component, Fragment } from "react";
import {
  Table,
  Button,
  Form,
  Container,
  Row,
  Col,
  Modal,
  ToggleButton,
  ToggleButtonGroup,
  Spinner,
} from "react-bootstrap";
import { db } from "../firebaseConfig";
import jsPDF from "jspdf";
import Detalle from "./Detalle";
import { FaPrint } from 'react-icons/fa';

class ListadoVentas extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cargando: false,
      documentos: [],
      vendedores: [],
      idVendedor: "",
      reparto: "",
      desde: "",
      hasta: "",
      show: false,
      id: "",
      total: 0,
      iva: 0,
      efectivo: 0,
      cheque: 0,
      transferencia: 0,
      otros: 0,
      credito: 0,
      idFactura: "",
      claseDoc: "",
      clienteDoc: "",
      totalDoc: "",
      estadoCuenta: "",
      totalAdeudado: "",
      tipoListado: "todos",
      docContado: [],
      totalContado: 0,
      ivaContado: 0,
      subTotalContado: 0,
      docCredito: [],
      totalCredito: 0,
      ivaCredito: 0,
      subTotalCredito: 0,
    };
  }

  componentDidMount() {
    this.traerVendedores();
  }

  traerVendedores = async () => {
    let snapVendedores = await db.collection("vendedores").get();
    let vendedores = [];
    snapVendedores.forEach((doc) => {
      vendedores[doc.id] = doc.data();
    });
    this.setState({ vendedores });
  };

  traerDocumentos = () => {
    const { desde, hasta, tipoListado, idVendedor, reparto } = this.state;
    if (desde && hasta) {
      if (idVendedor) {
        if (reparto) {
          this.setState({ cargando: true }, () => {
            if (tipoListado === "todos") {
              db.collection("ventasCabezera")
                .where("fecha", ">=", desde)
                .where("fecha", "<=", hasta)
                .where("ventaCobro", "==", "venta")
                .where("idVendedor", "==", idVendedor)
                .where("zona", "==", reparto)
                .orderBy("fecha")
                .orderBy("nDocumento")
                .onSnapshot((snapShot) => {
                  const documentos = [];
                  let total = 0;
                  let iva = 0;
                  let efectivo = 0;
                  let cheque = 0;
                  let transferencia = 0;
                  let otros = 0;
                  let credito = 0;
                  snapShot.forEach((doc) => {
                    documentos[doc.id] = doc.data();
                    total = total + documentos[doc.id].total;
                    if (documentos[doc.id].iva) {
                      iva = iva + documentos[doc.id].iva;
                    }
                    if (!documentos[doc.id].estadoCuenta) {
                      if (documentos[doc.id].formaPago === "Efectivo") {
                        efectivo = efectivo + documentos[doc.id].total;
                      }
                      if (documentos[doc.id].formaPago === "Cheque") {
                        cheque = cheque + documentos[doc.id].total;
                      }
                      if (documentos[doc.id].formaPago === "Transferencia") {
                        transferencia =
                          transferencia + documentos[doc.id].total;
                      }
                      if (documentos[doc.id].formaPago === "otros") {
                        otros = otros + documentos[doc.id].total;
                      }
                    } else {
                      credito = credito + documentos[doc.id].total;
                    }
                  });
                  this.setState({
                    documentos,
                    efectivo,
                    cheque,
                    transferencia,
                    otros,
                    credito,
                    total,
                    iva,
                    cargando: false,
                  });
                });
            }

            if (tipoListado === "a") {
              db.collection("ventasCabezera")
                .where("fecha", ">=", desde)
                .where("fecha", "<=", hasta)
                .where("ventaCobro", "==", "venta")
                .where("aOb", "==", true)
                .where("idVendedor", "==", idVendedor)
                .where("zona", "==", reparto)
                .orderBy("fecha")
                .orderBy("nDocumento")
                .onSnapshot((snapShot) => {
                  const documentos = [];
                  let total = 0;
                  let iva = 0;
                  let efectivo = 0;
                  let cheque = 0;
                  let transferencia = 0;
                  let otros = 0;
                  let credito = 0;
                  snapShot.forEach((doc) => {
                    documentos[doc.id] = doc.data();
                    total = total + documentos[doc.id].total;
                    if (documentos[doc.id].iva) {
                      iva = iva + documentos[doc.id].iva;
                    }
                    if (!documentos[doc.id].estadoCuenta) {
                      if (documentos[doc.id].formaPago === "Efectivo") {
                        efectivo = efectivo + documentos[doc.id].total;
                      }
                      if (documentos[doc.id].formaPago === "Cheque") {
                        cheque = cheque + documentos[doc.id].total;
                      }
                      if (documentos[doc.id].formaPago === "Transferencia") {
                        transferencia =
                          transferencia + documentos[doc.id].total;
                      }
                      if (documentos[doc.id].formaPago === "otros") {
                        otros = otros + documentos[doc.id].total;
                      }
                    } else {
                      credito = credito + documentos[doc.id].total;
                    }
                  });
                  this.setState({
                    documentos,
                    efectivo,
                    cheque,
                    transferencia,
                    otros,
                    credito,
                    total,
                    iva,
                    cargando: false,
                  });
                });
            }

            if (tipoListado === "b") {
              db.collection("ventasCabezera")
                .where("fecha", ">=", desde)
                .where("fecha", "<=", hasta)
                .where("ventaCobro", "==", "venta")
                .where("aOb", "==", false)
                .where("idVendedor", "==", idVendedor)
                .where("zona", "==", reparto)
                .orderBy("fecha")
                .orderBy("nDocumento")
                .onSnapshot((snapShot) => {
                  const documentos = [];
                  let total = 0;
                  let iva = 0;
                  let efectivo = 0;
                  let cheque = 0;
                  let transferencia = 0;
                  let otros = 0;
                  let credito = 0;
                  snapShot.forEach((doc) => {
                    documentos[doc.id] = doc.data();
                    total = total + documentos[doc.id].total;
                    if (documentos[doc.id].iva) {
                      iva = iva + documentos[doc.id].iva;
                    }
                    if (!documentos[doc.id].estadoCuenta) {
                      if (documentos[doc.id].formaPago === "Efectivo") {
                        efectivo = efectivo + documentos[doc.id].total;
                      }
                      if (documentos[doc.id].formaPago === "Cheque") {
                        cheque = cheque + documentos[doc.id].total;
                      }
                      if (documentos[doc.id].formaPago === "Transferencia") {
                        transferencia =
                          transferencia + documentos[doc.id].total;
                      }
                      if (documentos[doc.id].formaPago === "otros") {
                        otros = otros + documentos[doc.id].total;
                      }
                    } else {
                      credito = credito + documentos[doc.id].total;
                    }
                  });
                  this.setState({
                    documentos,
                    efectivo,
                    cheque,
                    transferencia,
                    otros,
                    credito,
                    total,
                    iva,
                    cargando: false,
                  });
                });
            }
          });
        } else {
          this.setState({ cargando: true }, () => {
            if (tipoListado === "todos") {
              db.collection("ventasCabezera")
                .where("fecha", ">=", desde)
                .where("fecha", "<=", hasta)
                .where("ventaCobro", "==", "venta")
                .where("idVendedor", "==", idVendedor)
                .orderBy("fecha")
                .orderBy("nDocumento")
                .onSnapshot((snapShot) => {
                  const documentos = [];
                  let total = 0;
                  let iva = 0;
                  let efectivo = 0;
                  let cheque = 0;
                  let transferencia = 0;
                  let otros = 0;
                  let credito = 0;
                  snapShot.forEach((doc) => {
                    documentos[doc.id] = doc.data();
                    total = total + documentos[doc.id].total;
                    if (documentos[doc.id].iva) {
                      iva = iva + documentos[doc.id].iva;
                    }
                    if (!documentos[doc.id].estadoCuenta) {
                      if (documentos[doc.id].formaPago === "Efectivo") {
                        efectivo = efectivo + documentos[doc.id].total;
                      }
                      if (documentos[doc.id].formaPago === "Cheque") {
                        cheque = cheque + documentos[doc.id].total;
                      }
                      if (documentos[doc.id].formaPago === "Transferencia") {
                        transferencia =
                          transferencia + documentos[doc.id].total;
                      }
                      if (documentos[doc.id].formaPago === "otros") {
                        otros = otros + documentos[doc.id].total;
                      }
                    } else {
                      credito = credito + documentos[doc.id].total;
                    }
                  });
                  this.setState({
                    documentos,
                    efectivo,
                    cheque,
                    transferencia,
                    otros,
                    credito,
                    total,
                    iva,
                    cargando: false,
                  });
                });
            }

            if (tipoListado === "a") {
              db.collection("ventasCabezera")
                .where("fecha", ">=", desde)
                .where("fecha", "<=", hasta)
                .where("ventaCobro", "==", "venta")
                .where("aOb", "==", true)
                .where("idVendedor", "==", idVendedor)
                .orderBy("fecha")
                .orderBy("nDocumento")
                .onSnapshot((snapShot) => {
                  const documentos = [];
                  let total = 0;
                  let iva = 0;
                  let efectivo = 0;
                  let cheque = 0;
                  let transferencia = 0;
                  let otros = 0;
                  let credito = 0;
                  snapShot.forEach((doc) => {
                    documentos[doc.id] = doc.data();
                    total = total + documentos[doc.id].total;
                    if (documentos[doc.id].iva) {
                      iva = iva + documentos[doc.id].iva;
                    }
                    if (!documentos[doc.id].estadoCuenta) {
                      if (documentos[doc.id].formaPago === "Efectivo") {
                        efectivo = efectivo + documentos[doc.id].total;
                      }
                      if (documentos[doc.id].formaPago === "Cheque") {
                        cheque = cheque + documentos[doc.id].total;
                      }
                      if (documentos[doc.id].formaPago === "Transferencia") {
                        transferencia =
                          transferencia + documentos[doc.id].total;
                      }
                      if (documentos[doc.id].formaPago === "otros") {
                        otros = otros + documentos[doc.id].total;
                      }
                    } else {
                      credito = credito + documentos[doc.id].total;
                    }
                  });
                  this.setState({
                    documentos,
                    efectivo,
                    cheque,
                    transferencia,
                    otros,
                    credito,
                    total,
                    iva,
                    cargando: false,
                  });
                });
            }

            if (tipoListado === "b") {
              db.collection("ventasCabezera")
                .where("fecha", ">=", desde)
                .where("fecha", "<=", hasta)
                .where("ventaCobro", "==", "venta")
                .where("aOb", "==", false)
                .where("idVendedor", "==", idVendedor)
                .orderBy("fecha")
                .orderBy("nDocumento")
                .onSnapshot((snapShot) => {
                  const documentos = [];
                  let total = 0;
                  let iva = 0;
                  let efectivo = 0;
                  let cheque = 0;
                  let transferencia = 0;
                  let otros = 0;
                  let credito = 0;
                  snapShot.forEach((doc) => {
                    documentos[doc.id] = doc.data();
                    total = total + documentos[doc.id].total;
                    if (documentos[doc.id].iva) {
                      iva = iva + documentos[doc.id].iva;
                    }
                    if (!documentos[doc.id].estadoCuenta) {
                      if (documentos[doc.id].formaPago === "Efectivo") {
                        efectivo = efectivo + documentos[doc.id].total;
                      }
                      if (documentos[doc.id].formaPago === "Cheque") {
                        cheque = cheque + documentos[doc.id].total;
                      }
                      if (documentos[doc.id].formaPago === "Transferencia") {
                        transferencia =
                          transferencia + documentos[doc.id].total;
                      }
                      if (documentos[doc.id].formaPago === "otros") {
                        otros = otros + documentos[doc.id].total;
                      }
                    } else {
                      credito = credito + documentos[doc.id].total;
                    }
                  });
                  this.setState({
                    documentos,
                    efectivo,
                    cheque,
                    transferencia,
                    otros,
                    credito,
                    total,
                    iva,
                    cargando: false,
                  });
                });
            }
          });
        }
      } else {
        if (reparto) {
          this.setState({ cargando: true }, () => {
            if (tipoListado === "todos") {
              db.collection("ventasCabezera")
                .where("fecha", ">=", desde)
                .where("fecha", "<=", hasta)
                .where("ventaCobro", "==", "venta")
                .where("zona", "==", reparto)
                .orderBy("fecha")
                .orderBy("nDocumento")
                .onSnapshot((snapShot) => {
                  const documentos = [];
                  let total = 0;
                  let iva = 0;
                  let efectivo = 0;
                  let cheque = 0;
                  let transferencia = 0;
                  let otros = 0;
                  let credito = 0;
                  snapShot.forEach((doc) => {
                    documentos[doc.id] = doc.data();
                    total = total + documentos[doc.id].total;
                    if (documentos[doc.id].iva) {
                      iva = iva + documentos[doc.id].iva;
                    }
                    if (!documentos[doc.id].estadoCuenta) {
                      if (documentos[doc.id].formaPago === "Efectivo") {
                        efectivo = efectivo + documentos[doc.id].total;
                      }
                      if (documentos[doc.id].formaPago === "Cheque") {
                        cheque = cheque + documentos[doc.id].total;
                      }
                      if (documentos[doc.id].formaPago === "Transferencia") {
                        transferencia =
                          transferencia + documentos[doc.id].total;
                      }
                      if (documentos[doc.id].formaPago === "otros") {
                        otros = otros + documentos[doc.id].total;
                      }
                    } else {
                      credito = credito + documentos[doc.id].total;
                    }
                  });
                  this.setState({
                    documentos,
                    efectivo,
                    cheque,
                    transferencia,
                    otros,
                    credito,
                    total,
                    iva,
                    cargando: false,
                  });
                });
            }

            if (tipoListado === "a") {
              db.collection("ventasCabezera")
                .where("fecha", ">=", desde)
                .where("fecha", "<=", hasta)
                .where("ventaCobro", "==", "venta")
                .where("zona", "==", reparto)
                .where("aOb", "==", true)
                .orderBy("fecha")
                .orderBy("nDocumento")
                .onSnapshot((snapShot) => {
                  const documentos = [];
                  let total = 0;
                  let iva = 0;
                  let efectivo = 0;
                  let cheque = 0;
                  let transferencia = 0;
                  let otros = 0;
                  let credito = 0;
                  snapShot.forEach((doc) => {
                    documentos[doc.id] = doc.data();
                    total = total + documentos[doc.id].total;
                    if (documentos[doc.id].iva) {
                      iva = iva + documentos[doc.id].iva;
                    }
                    if (!documentos[doc.id].estadoCuenta) {
                      if (documentos[doc.id].formaPago === "Efectivo") {
                        efectivo = efectivo + documentos[doc.id].total;
                      }
                      if (documentos[doc.id].formaPago === "Cheque") {
                        cheque = cheque + documentos[doc.id].total;
                      }
                      if (documentos[doc.id].formaPago === "Transferencia") {
                        transferencia =
                          transferencia + documentos[doc.id].total;
                      }
                      if (documentos[doc.id].formaPago === "otros") {
                        otros = otros + documentos[doc.id].total;
                      }
                    } else {
                      credito = credito + documentos[doc.id].total;
                    }
                  });
                  this.setState({
                    documentos,
                    efectivo,
                    cheque,
                    transferencia,
                    otros,
                    credito,
                    total,
                    iva,
                    cargando: false,
                  });
                });
            }

            if (tipoListado === "b") {
              db.collection("ventasCabezera")
                .where("fecha", ">=", desde)
                .where("fecha", "<=", hasta)
                .where("ventaCobro", "==", "venta")
                .where("zona", "==", reparto)
                .where("aOb", "==", false)
                .orderBy("fecha")
                .orderBy("nDocumento")
                .onSnapshot((snapShot) => {
                  const documentos = [];
                  let total = 0;
                  let iva = 0;
                  let efectivo = 0;
                  let cheque = 0;
                  let transferencia = 0;
                  let otros = 0;
                  let credito = 0;
                  snapShot.forEach((doc) => {
                    documentos[doc.id] = doc.data();
                    total = total + documentos[doc.id].total;
                    if (documentos[doc.id].iva) {
                      iva = iva + documentos[doc.id].iva;
                    }
                    if (!documentos[doc.id].estadoCuenta) {
                      if (documentos[doc.id].formaPago === "Efectivo") {
                        efectivo = efectivo + documentos[doc.id].total;
                      }
                      if (documentos[doc.id].formaPago === "Cheque") {
                        cheque = cheque + documentos[doc.id].total;
                      }
                      if (documentos[doc.id].formaPago === "Transferencia") {
                        transferencia =
                          transferencia + documentos[doc.id].total;
                      }
                      if (documentos[doc.id].formaPago === "otros") {
                        otros = otros + documentos[doc.id].total;
                      }
                    } else {
                      credito = credito + documentos[doc.id].total;
                    }
                  });
                  this.setState({
                    documentos,
                    efectivo,
                    cheque,
                    transferencia,
                    otros,
                    credito,
                    total,
                    iva,
                    cargando: false,
                  });
                });
            }
          });
        } else {
          this.setState({ cargando: true }, () => {
            if (tipoListado === "todos") {
              db.collection("ventasCabezera")
                .where("fecha", ">=", desde)
                .where("fecha", "<=", hasta)
                .where("ventaCobro", "==", "venta")
                .orderBy("fecha")
                .orderBy("nDocumento")
                .onSnapshot((snapShot) => {
                  const documentos = [];
                  let total = 0;
                  let iva = 0;
                  let efectivo = 0;
                  let cheque = 0;
                  let transferencia = 0;
                  let otros = 0;
                  let credito = 0;
                  snapShot.forEach((doc) => {
                    documentos[doc.id] = doc.data();
                    total = total + documentos[doc.id].total;
                    if (documentos[doc.id].iva) {
                      iva = iva + documentos[doc.id].iva;
                    }
                    if (!documentos[doc.id].estadoCuenta) {
                      if (documentos[doc.id].formaPago === "Efectivo") {
                        efectivo = efectivo + documentos[doc.id].total;
                      }
                      if (documentos[doc.id].formaPago === "Cheque") {
                        cheque = cheque + documentos[doc.id].total;
                      }
                      if (documentos[doc.id].formaPago === "Transferencia") {
                        transferencia =
                          transferencia + documentos[doc.id].total;
                      }
                      if (documentos[doc.id].formaPago === "otros") {
                        otros = otros + documentos[doc.id].total;
                      }
                    } else {
                      credito = credito + documentos[doc.id].total;
                    }
                  });
                  this.setState({
                    documentos,
                    efectivo,
                    cheque,
                    transferencia,
                    otros,
                    credito,
                    total,
                    iva,
                    cargando: false,
                  });
                });
            }

            if (tipoListado === "a") {
              db.collection("ventasCabezera")
                .where("fecha", ">=", desde)
                .where("fecha", "<=", hasta)
                .where("ventaCobro", "==", "venta")
                .where("aOb", "==", true)
                .orderBy("fecha")
                .orderBy("nDocumento")
                .onSnapshot((snapShot) => {
                  const documentos = [];
                  let total = 0;
                  let iva = 0;
                  let efectivo = 0;
                  let cheque = 0;
                  let transferencia = 0;
                  let otros = 0;
                  let credito = 0;
                  snapShot.forEach((doc) => {
                    documentos[doc.id] = doc.data();
                    total = total + documentos[doc.id].total;
                    if (documentos[doc.id].iva) {
                      iva = iva + documentos[doc.id].iva;
                    }
                    if (!documentos[doc.id].estadoCuenta) {
                      if (documentos[doc.id].formaPago === "Efectivo") {
                        efectivo = efectivo + documentos[doc.id].total;
                      }
                      if (documentos[doc.id].formaPago === "Cheque") {
                        cheque = cheque + documentos[doc.id].total;
                      }
                      if (documentos[doc.id].formaPago === "Transferencia") {
                        transferencia =
                          transferencia + documentos[doc.id].total;
                      }
                      if (documentos[doc.id].formaPago === "otros") {
                        otros = otros + documentos[doc.id].total;
                      }
                    } else {
                      credito = credito + documentos[doc.id].total;
                    }
                  });
                  this.setState({
                    documentos,
                    efectivo,
                    cheque,
                    transferencia,
                    otros,
                    credito,
                    total,
                    iva,
                    cargando: false,
                  });
                });
            }

            if (tipoListado === "b") {
              db.collection("ventasCabezera")
                .where("fecha", ">=", desde)
                .where("fecha", "<=", hasta)
                .where("ventaCobro", "==", "venta")
                .where("aOb", "==", false)
                .orderBy("fecha")
                .orderBy("nDocumento")
                .onSnapshot((snapShot) => {
                  const documentos = [];
                  let total = 0;
                  let iva = 0;
                  let efectivo = 0;
                  let cheque = 0;
                  let transferencia = 0;
                  let otros = 0;
                  let credito = 0;
                  snapShot.forEach((doc) => {
                    documentos[doc.id] = doc.data();
                    total = total + documentos[doc.id].total;
                    if (documentos[doc.id].iva) {
                      iva = iva + documentos[doc.id].iva;
                    }
                    if (!documentos[doc.id].estadoCuenta) {
                      if (documentos[doc.id].formaPago === "Efectivo") {
                        efectivo = efectivo + documentos[doc.id].total;
                      }
                      if (documentos[doc.id].formaPago === "Cheque") {
                        cheque = cheque + documentos[doc.id].total;
                      }
                      if (documentos[doc.id].formaPago === "Transferencia") {
                        transferencia =
                          transferencia + documentos[doc.id].total;
                      }
                      if (documentos[doc.id].formaPago === "otros") {
                        otros = otros + documentos[doc.id].total;
                      }
                    } else {
                      credito = credito + documentos[doc.id].total;
                    }
                  });
                  this.setState({
                    documentos,
                    efectivo,
                    cheque,
                    transferencia,
                    otros,
                    credito,
                    total,
                    iva,
                    cargando: false,
                  });
                });
            }
          });
        }
      }
    }
  };

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleClose = () => {
    this.setState({ show: false });
  };

  handleShow = async (id, clienteDoc, estadoCuenta, totalDoc) => {
    if (estadoCuenta) {
      let snapCliente = await db.collection("clientes").doc(clienteDoc).get();
      let totalAdeudado = snapCliente.data().totalAdeudado;
      if (totalAdeudado) {
        totalAdeudado = parseFloat(totalAdeudado);
      } else {
        totalAdeudado = 0;
      }
      this.setState({
        show: true,
        id: id,
        clienteDoc,
        totalDoc,
        estadoCuenta,
        totalAdeudado,
      });
    } else {
      this.setState({ show: true, id: id, clienteDoc, totalDoc, estadoCuenta });
    }
  };

  borrar = () => {
    this.handleClose();
  };

  detalle = (id, documento) => {
    let claseDoc = "";
    if (id === this.state.idFactura) {
      this.setState({ idFactura: "", claseDoc });
    } else {
      if (documento === "Factura Contado") {
        claseDoc = "table-danger";
      }
      if (documento === "Boleta Contado") {
        claseDoc = "table-warning";
      }
      if (documento === "Factura Crédito") {
        claseDoc = "table-active";
      }
      if (documento === "Boleta Crédito") {
        claseDoc = "table-info";
      }
      this.setState({ idFactura: id, claseDoc });
    }
  };

  handleTipoListado = (val) => {
    this.setState({ tipoListado: val }, () => {
      this.traerDocumentos();
    });
  };

  imprimir = () => {
    const { desde, hasta, documentos, total, iva } = this.state;

    if (Object.keys(documentos).length > 0) {
      let doc = new jsPDF();

      doc.setFontSize(20);
      doc.text(70, 15, "Ventas LAGO MAYORAL SA");

      doc.setFontSize(12);
      doc.text(10, 25, `Ventas desde ${desde} hasta ${hasta}`);
      doc.text(5, 35, `Fecha`);
      doc.text(35, 35, `Cliente`);
      doc.text(102, 35, `Documento`);
      doc.text(136, 35, `Nº`);
      doc.text(157, 35, `IVA`);
      doc.text(180, 35, `Importe`);

      doc.line(1, 37, 205, 37);
      let co = 38;
      doc.setFontSize(10);
      Object.keys(documentos).forEach((id) => {
        co = co + 5;
        if (co >= 263) {
          doc.addPage();
          doc.setFontSize(20);
          doc.text(70, 15, "Ventas LAGO MAYORAL SA");

          doc.setFontSize(12);
          doc.text(10, 25, `Ventas desde ${desde} hasta ${hasta}`);
          doc.text(5, 35, `Fecha`);
          doc.text(35, 35, `Cliente`);
          doc.text(102, 35, `Documento`);
          doc.text(136, 35, `Nº`);
          doc.text(157, 35, `IVA`);
          doc.text(180, 35, `Importe`);

          doc.line(1, 37, 205, 37);
          co = 43;
          doc.setFontSize(10);
          doc.text(3, co, documentos[id].fecha);
          doc.text(23, co, documentos[id].clienteNombre);
          doc.text(100, co, documentos[id].documento);
          doc.text(133, co, `${documentos[id].nDocumento}`);
          doc.text(155, co, documentos[id].iva.toLocaleString("de-DE"));
          doc.text(183, co, documentos[id].total.toLocaleString("de-DE"));
        } else {
          doc.text(3, co, documentos[id].fecha);
          doc.text(23, co, documentos[id].clienteNombre);
          doc.text(100, co, documentos[id].documento);
          doc.text(133, co, `${documentos[id].nDocumento}`);
          doc.text(155, co, documentos[id].iva.toLocaleString("de-DE"));
          doc.text(183, co, documentos[id].total.toLocaleString("de-DE"));
        }
      });

      doc.setFontSize(12);

      co = co + 15;
      doc.text(130, co, `Total IVA: `);
      doc.text(180, co, iva.toLocaleString("de-DE"));

      co = co + 8;
      doc.text(130, co, `Total Facturado: `);
      doc.text(180, co, total.toLocaleString("de-DE"));

      doc.save(`Ventas LAGO MAYORAL SA desde: ${desde} hasta: ${hasta}.pdf`);
    }
  };
  
  separarContado = () => {
    let { documentos } = this.state;
    let docContado = [];
    let totalContado = 0;
    let ivaContado = 0;
    let subTotalContado = 0;

    Object.keys(documentos).forEach(id => {
      if(documentos[id].documento === "Factura Contado" || documentos[id].documento === "Boleta Contado") {
        totalContado = totalContado + parseFloat(documentos[id].total);
        ivaContado = ivaContado + parseFloat(documentos[id].iva);
        subTotalContado = subTotalContado + parseFloat(documentos[id].subtotal);
        docContado.push({...documentos[id]});
      }
    })

    this.setState({ docContado, totalContado, ivaContado, subTotalContado });
  }
  
  separarCredito = () => {
    let { documentos } = this.state;
    let docCredito = [];
    let totalCredito = 0;
    let ivaCredito = 0;
    let subTotalCredito = 0;

    Object.keys(documentos).forEach(id => {
      if(documentos[id].documento === "Factura Crédito" || documentos[id].documento === "Boleta Crédito") {
        totalCredito = totalCredito + parseFloat(documentos[id].total);
        ivaCredito = ivaCredito + parseFloat(documentos[id].iva);
        subTotalCredito = subTotalCredito + parseFloat(documentos[id].subtotal);
        docCredito.push({...documentos[id]});
      }
    })

    this.setState({ docCredito, totalCredito, ivaCredito, subTotalCredito });
  }
  
  imprimirContador = async () => {
    let separarContado = await this.separarContado();
    let separarCredito = await this.separarCredito();

    const { 
       desde,
       hasta,
       docContado,
       docCredito,
       totalContado,
       ivaContado,
       subTotalContado,
       totalCredito,
       ivaCredito,
       subTotalCredito } = this.state;

    if (Object.keys(docContado).length > 0 || Object.keys(docCredito).length > 0) {
      let doc = new jsPDF();

      doc.setFontSize(20);
      doc.text(65, 15, "Ventas LAGO MAYORAL SA");

      doc.setFontSize(12);
      doc.text(10, 25, `Ventas desde ${desde} hasta ${hasta}`);
      let co = 35;
      if(Object.keys(docContado).length > 0) {
        doc.text(5, co, `Fecha`);
        doc.text(35, co, `Cliente`);
        doc.text(102, co, `Documento`);
        doc.text(136, co, `Nº`);
        doc.text(157, co, `IVA`);
        doc.text(180, co, `Importe`);
      co = co + 2
      doc.line(1, co, 205, co);
      co = co + 1;
      doc.setFontSize(10);
      Object.keys(docContado).forEach((id) => {
        co = co + 5;
        if (co >= 263) {
          doc.addPage();
          doc.setFontSize(20);
          doc.text(65, 15, "Ventas LAGO MAYORAL SA");

          doc.setFontSize(12);
          doc.text(10, 25, `Ventas desde ${desde} hasta ${hasta}`);
          co = 35;
          doc.text(5, co, `Fecha`);
          doc.text(35, co, `Cliente`);
          doc.text(102, co, `Documento`);
          doc.text(136, co, `Nº`);
          doc.text(157, co, `IVA`);
          doc.text(180, co, `Importe`);
          co = co + 2;
          doc.line(1, co, 205, co);
          co = co + 8;
          doc.setFontSize(10);
          doc.text(3, co, docContado[id].fecha);
          doc.text(23, co, docContado[id].clienteNombre);
          doc.text(100, co, docContado[id].documento);
          doc.text(133, co, `${docContado[id].nDocumento}`);
          doc.text(155, co, docContado[id].iva.toLocaleString("de-DE"));
          doc.text(183, co, docContado[id].total.toLocaleString("de-DE"));
        } else {
          doc.text(3, co, docContado[id].fecha);
          doc.text(23, co, docContado[id].clienteNombre);
          doc.text(100, co, docContado[id].documento);
          doc.text(133, co, `${docContado[id].nDocumento}`);
          doc.text(155, co, docContado[id].iva.toLocaleString("de-DE"));
          doc.text(183, co, docContado[id].total.toLocaleString("de-DE"));
        }
      });

        
      doc.setFontSize(12);
      
      co = co + 15;
      doc.text(130, co, `Subtotal: `);
      doc.text(180, co, subTotalContado.toLocaleString("de-DE"));

      co = co + 8;
      doc.text(130, co, `Total IVA: `);
      doc.text(180, co, ivaContado.toLocaleString("de-DE"));

      co = co + 8;
      doc.text(130, co, `Total Facturado: `);
      doc.text(180, co, totalContado.toLocaleString("de-DE"));
      co = co + 10;
      }


      if(Object.keys(docCredito).length > 0) {

      doc.setFontSize(12);
      doc.text(5, co, `Fecha`);
      doc.text(35, co, `Cliente`);
      doc.text(102, co, `Documento`);
      doc.text(136, co, `Nº`);
      doc.text(157, co, `IVA`);
      doc.text(180, co, `Importe`);
      co = co + 2;
      doc.line(1, co, 205, co);
      co = co + 1;
      doc.setFontSize(10);
      Object.keys(docCredito).forEach((id) => {
        co = co + 5;
        if (co >= 263) {
          doc.addPage();
          doc.setFontSize(20);
          doc.text(65, 15, "Ventas LAGO MAYORAL SA");

          doc.setFontSize(12);
          doc.text(10, 25, `Ventas desde ${desde} hasta ${hasta}`);
          co = 35;
          doc.text(5, co, `Fecha`);
          doc.text(35, co, `Cliente`);
          doc.text(102, co, `Documento`);
          doc.text(136, co, `Nº`);
          doc.text(157, co, `IVA`);
          doc.text(180, co, `Importe`);
          co = co + 2
          doc.line(1, co, 205, co);
          co = co + 8;
          doc.setFontSize(10);
          doc.text(3, co, docCredito[id].fecha);
          doc.text(23, co, docCredito[id].clienteNombre);
          doc.text(100, co, docCredito[id].documento);
          doc.text(133, co, `${docCredito[id].nDocumento}`);
          doc.text(155, co, docCredito[id].iva.toLocaleString("de-DE"));
          doc.text(183, co, docCredito[id].total.toLocaleString("de-DE"));
        } else {
          doc.text(3, co, docCredito[id].fecha);
          doc.text(23, co, docCredito[id].clienteNombre);
          doc.text(100, co, docCredito[id].documento);
          doc.text(133, co, `${docCredito[id].nDocumento}`);
          doc.text(155, co, docCredito[id].iva.toLocaleString("de-DE"));
          doc.text(183, co, docCredito[id].total.toLocaleString("de-DE"));
        }
      });

        doc.setFontSize(12);
        
        co = co + 15;
        doc.text(130, co, `Subtotal: `);
        doc.text(180, co, subTotalCredito.toLocaleString("de-DE"));
  
        co = co + 8;
        doc.text(130, co, `Total IVA: `);
        doc.text(180, co, ivaCredito.toLocaleString("de-DE"));
  
        co = co + 8;
        doc.text(130, co, `Total Facturado: `);
        doc.text(180, co, totalCredito.toLocaleString("de-DE"));
      }

      doc.save(`Ventas LAGO MAYORAL SA desde: ${desde} hasta: ${hasta} Contador.pdf`);
    }
  };

  render() {
    const {
      documentos,
      vendedores,
      total,
      iva,
      efectivo,
      cheque,
      transferencia,
      otros,
      credito,
    } = this.state;
    return (
      <div>
        <Container>
          <Row>
          <Col>
              {Object.keys(documentos).length > 0 ? (
                <Button
                  style={{ margin: "28px" }}
                  variant="dark"
                  onClick={() => {
                    this.imprimir();
                  }}
                >
                  <FaPrint size="1.5em"/>
                </Button>
              ) : null}
            </Col>
            <Col>
              <h1 style={{ padding: "15px" }}>Listado Ventas</h1>
            </Col>
            <Col>
              {Object.keys(documentos).length > 0 ? (
                <Button
                  style={{ margin: "28px" }}
                  variant="outline-dark"
                  onClick={() => {
                    this.imprimirContador();
                  }}
                >
                <FaPrint size="1.5em"/> Contador
                </Button>
              ) : null}
            </Col>
          </Row>
          <Row>
          <Col style={{ textAlign: "right" }}>
              <ToggleButtonGroup
                style={{
                  height: "42px",
                  marginBottom: "25px",
                  marginTop: "25px",
                }}
                type="radio"
                name="tipoListado"
                value={this.state.tipoListado}
                onChange={this.handleTipoListado}
              >
                <ToggleButton
                  variant="outline-primary"
                  name="radio"
                  value="todos"
                >
                  Todos
                </ToggleButton>
                <ToggleButton variant="outline-success" name="radio" value="a">
                  A
                </ToggleButton>
                <ToggleButton variant="outline-danger" name="radio" value="b">
                  B
                </ToggleButton>
              </ToggleButtonGroup>
            </Col>
          </Row>
          <Row>
            <Col>
              <Form.Group>
                <Form.Label>Desde</Form.Label>
                <input
                  name="desde"
                  className="form-control text-center"
                  type="date"
                  onChange={(e) => this.handleChange(e)}
                  value={this.state.desde}
                />
              </Form.Group>
            </Col>
            <Col>
              <Form.Group>
                <Form.Label>Hasta</Form.Label>
                <input
                  name="hasta"
                  className="form-control text-center"
                  type="date"
                  onChange={(e) => this.handleChange(e)}
                  value={this.state.hasta}
                />
              </Form.Group>
            </Col>
          </Row>
          <Row>
            <Col>
              <Form.Group>
                <Form.Label>Vendedor</Form.Label>
                <select
                  className="form-control"
                  onChange={(e) => this.handleChange(e)}
                  name="idVendedor"
                  value={this.state.idVendedor}
                  aria-label="Small"
                >
                  <option value="">Todos...</option>
                  {Object.keys(vendedores).map((id) => {
                    return (
                      <option value={id}>{vendedores[id].vendedor}</option>
                    );
                  })}
                </select>
              </Form.Group>
            </Col>
          </Row>
          <Row>
            <Col>
              <Form.Group>
                <Form.Label>Reparto</Form.Label>
                <select
                  className="form-control"
                  onChange={(e) => this.handleChange(e)}
                  name="reparto"
                  value={this.state.reparto}
                  aria-label="Small"
                >
                  <option value="">Todos...</option>
                  <option value="Montevideo">Montevideo</option>
                  <option value="La Costa">La Costa</option>
                </select>
              </Form.Group>
            </Col>
          </Row>
          <Row>
            <Col>
              <Button
                variant="success"
                onClick={() => {
                  this.traerDocumentos();
                }}
              >
                Buscar
              </Button>
            </Col>
          </Row>
        </Container>
        {this.state.cargando ? (
          <div
            style={{
              width: "100%",
              height: "200px",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Spinner animation="grow" variant="primary" />
          </div>
        ) : (
          <div style={{ width: "100%", overflow: "scroll", marginTop: "20px" }}>
            <Table striped bordered condensed hover responsive>
              <thead>
                <tr>
                  <th>Fecha</th>
                  <th>Cliente</th>
                  <th>Documento</th>
                  <th>Nº</th>
                  <th>IVA</th>
                  <th>Importe</th>
                  <th />
                </tr>
              </thead>
              <tbody>
                {Object.keys(documentos).length > 0 ? (
                  Object.keys(documentos).map((id) => {
                    return (
                      <Fragment>
                        <tr
                          key={id}
                          className={
                            this.state.idFactura === id
                              ? this.state.claseDoc
                              : ""
                          }
                        >
                          <td
                            onClick={() =>
                              this.detalle(id, documentos[id].documento)
                            }
                          >
                            {documentos[id].fecha}
                          </td>
                          <td
                            onClick={() =>
                              this.detalle(id, documentos[id].documento)
                            }
                          >
                            {documentos[id].clienteNombre}
                          </td>
                          <td
                            onClick={() =>
                              this.detalle(id, documentos[id].documento)
                            }
                          >
                            {documentos[id].documento}
                          </td>
                          <td
                            onClick={() =>
                              this.detalle(id, documentos[id].documento)
                            }
                          >
                            {documentos[id].nDocumento}
                          </td>
                          <td
                            onClick={() =>
                              this.detalle(id, documentos[id].documento)
                            }
                          >
                            {documentos[id].iva.toLocaleString("de-DE")}
                          </td>
                          {this.state.idFactura === id ? (
                            <td
                              onClick={() =>
                                this.detalle(id, documentos[id].documento)
                              }
                            >
                              <strong>
                                {documentos[id].total.toLocaleString("de-DE")}
                              </strong>
                            </td>
                          ) : (
                            <td
                              onClick={() =>
                                this.detalle(id, documentos[id].documento)
                              }
                            >
                              {documentos[id].total.toLocaleString("de-DE")}
                            </td>
                          )}
                          <td
                            onClick={() =>
                              this.detalle(id, documentos[id].documento)
                            }
                          >
                            {documentos[id].formaPago}
                          </td>
                          <td>
                            <Button
                              variant="danger"
                              onClick={() => {
                                this.handleShow(
                                  id,
                                  documentos[id].cliente,
                                  documentos[id].estadoCuenta,
                                  documentos[id].total
                                );
                              }}
                            >
                              Eliminar
                            </Button>
                          </td>
                        </tr>
                        {this.state.idFactura === id ? (
                          <Detalle
                            idFactura={id}
                            claseDoc={this.state.claseDoc}
                            documentos={documentos[id]}
                            empresa={this.props.empresa}
                          />
                        ) : null}
                      </Fragment>
                    );
                  })
                ) : (
                  <tr>
                    <td colSpan="8">
                      <p>NO se encontraron resultados</p>
                    </td>
                  </tr>
                )}
              </tbody>
            </Table>
          </div>
        )}
        {Object.keys(documentos).length > 0 ? (
          <div>
            <Table>
              <tbody>
                <tr>
                  <td />
                  <td />
                  <td />
                  <td />
                  <td colSpan="2">
                    <strong>Total Efectivo</strong>
                  </td>
                  <td>
                    <strong>{efectivo.toLocaleString("de-DE")}</strong>
                  </td>
                  <td />
                </tr>
                <tr>
                  <td />
                  <td />
                  <td />
                  <td />
                  <td colSpan="2">
                    <strong>Total Cheque</strong>
                  </td>
                  <td>
                    <strong>{cheque.toLocaleString("de-DE")}</strong>
                  </td>
                  <td />
                </tr>
                <tr>
                  <td />
                  <td />
                  <td />
                  <td />
                  <td colSpan="2">
                    <strong>Total Transferencia</strong>
                  </td>
                  <td>
                    <strong>{transferencia.toLocaleString("de-DE")}</strong>
                  </td>
                  <td />
                </tr>
                <tr>
                  <td />
                  <td />
                  <td />
                  <td />
                  <td colSpan="2">
                    <strong>Total Otros</strong>
                  </td>
                  <td>
                    <strong>{otros.toLocaleString("de-DE")}</strong>
                  </td>
                  <td />
                </tr>
                <tr>
                  <td />
                  <td />
                  <td />
                  <td />
                  <td colSpan="2">
                    <strong>Total Crédito</strong>
                  </td>
                  <td>
                    <strong>{credito.toLocaleString("de-DE")}</strong>
                  </td>
                  <td />
                </tr>
                <tr>
                  <td />
                  <td />
                  <td />
                  <td />
                  <td colSpan="2">
                    <strong>Total IVA</strong>
                  </td>
                  <td>
                    <strong>{iva.toLocaleString("de-DE")}</strong>
                  </td>
                  <td />
                </tr>
                <tr>
                  <td />
                  <td />
                  <td />
                  <td />
                  <td colSpan="2">
                    <strong>Total Facturado</strong>
                  </td>
                  <td>
                    <strong>{total.toLocaleString("de-DE")}</strong>
                  </td>
                  <td />
                </tr>
              </tbody>
            </Table>
          </div>
        ) : null}
        <Modal show={this.state.show} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>ATENCIÓN!!!</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            Estás seguro que quieres ELIMINAR este documento?!
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleClose}>
              Cancelar
            </Button>
            <Button
              variant="danger"
              onClick={() => {
                this.borrar();
              }}
            >
              ELIMINAR
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

export default ListadoVentas;
