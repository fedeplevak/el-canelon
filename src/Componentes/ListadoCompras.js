import React, { Component, Fragment } from "react";
import {
  Table,
  Button,
  Form,
  Container,
  Row,
  Col,
  Modal,
  ToggleButton,
  ToggleButtonGroup,
  Spinner,
} from "react-bootstrap";
import { db } from "../firebaseConfig";
import jsPDF from "jspdf";
import DetalleCompras from "./DetalleCompras";
import { FaPrint } from 'react-icons/fa';

class ListadoCompras extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cargando: false,
      documentos: [],
      desde: "",
      hasta: "",
      show: false,
      id: "",
      total: 0,
      iva: 0,
      efectivo: 0,
      cheque: 0,
      transferencia: 0,
      otros: 0,
      credito: 0,
      idFactura: "",
      claseDoc: "",
      clienteDoc: "",
      totalDoc: "",
      lugar: "",
      compraPago: "",
      estadoCuenta: "",
      totalAdeudado: "",
      tipoListado: "todos",
      docContado: [],
      totalContado: 0,
      ivaContado: 0,
      subTotalContado: 0,
      percepcionContado: 0,
      docCredito: [],
      totalCredito: 0,
      ivaCredito: 0,
      subTotalCredito: 0,
      percepcionCredito: 0,
    };
  }

  traerDocumentos = () => {
    const { desde, hasta, tipoListado } = this.state;
    if (desde && hasta) {
      this.setState({ cargando: true }, () => {
        if (tipoListado === "todos") {
          db.collection("comprasCabezera")
            .where("fecha", ">=", desde)
            .where("fecha", "<=", hasta)
            .where("compraPago", "==", "compra")
            .orderBy("fecha")
            .orderBy("nDocumento")
            .onSnapshot((snapShot) => {
              const documentos = [];
              let total = 0;
              let percepcion = 0;
              let iva = 0;
              let efectivo = 0;
              let cheque = 0;
              let transferencia = 0;
              let otros = 0;
              let credito = 0;
              snapShot.forEach((doc) => {
                documentos[doc.id] = doc.data();
                total = total + documentos[doc.id].total;
                if (documentos[doc.id].percepcion) {
                  documentos[doc.id].percepcion = parseFloat(
                    documentos[doc.id].percepcion
                  );
                } else {
                  documentos[doc.id].percepcion = 0;
                }
                percepcion = percepcion + documentos[doc.id].percepcion;
                if (documentos[doc.id].iva) {
                  iva = iva + documentos[doc.id].iva;
                }
                if (!documentos[doc.id].estadoCuenta) {
                  if (documentos[doc.id].formaPago === "Efectivo") {
                    efectivo = efectivo + documentos[doc.id].total;
                  }
                  if (documentos[doc.id].formaPago === "Cheque") {
                    cheque = cheque + documentos[doc.id].total;
                  }
                  if (documentos[doc.id].formaPago === "Transferencia") {
                    transferencia = transferencia + documentos[doc.id].total;
                  }
                  if (documentos[doc.id].formaPago === "otros") {
                    otros = otros + documentos[doc.id].total;
                  }
                } else {
                  credito = credito + documentos[doc.id].total;
                }
              });
              this.setState({
                documentos,
                efectivo,
                cheque,
                transferencia,
                otros,
                credito,
                total,
                percepcion,
                iva,
                cargando: false,
              });
            });
        }

        if (tipoListado === "a") {
          db.collection("comprasCabezera")
            .where("fecha", ">=", desde)
            .where("fecha", "<=", hasta)
            .where("compraPago", "==", "compra")
            .where("aOb", "==", true)
            .orderBy("fecha")
            .orderBy("nDocumento")
            .onSnapshot((snapShot) => {
              const documentos = [];
              let total = 0;
              let percepcion = 0;
              let iva = 0;
              let efectivo = 0;
              let cheque = 0;
              let transferencia = 0;
              let otros = 0;
              let credito = 0;
              snapShot.forEach((doc) => {
                documentos[doc.id] = doc.data();
                total = total + documentos[doc.id].total;
                if (documentos[doc.id].percepcion) {
                  documentos[doc.id].percepcion = parseFloat(
                    documentos[doc.id].percepcion
                  );
                } else {
                  documentos[doc.id].percepcion = 0;
                }
                percepcion = percepcion + documentos[doc.id].percepcion;
                if (documentos[doc.id].iva) {
                  iva = iva + documentos[doc.id].iva;
                }
                if (!documentos[doc.id].estadoCuenta) {
                  if (documentos[doc.id].formaPago === "Efectivo") {
                    efectivo = efectivo + documentos[doc.id].total;
                  }
                  if (documentos[doc.id].formaPago === "Cheque") {
                    cheque = cheque + documentos[doc.id].total;
                  }
                  if (documentos[doc.id].formaPago === "Transferencia") {
                    transferencia = transferencia + documentos[doc.id].total;
                  }
                  if (documentos[doc.id].formaPago === "otros") {
                    otros = otros + documentos[doc.id].total;
                  }
                } else {
                  credito = credito + documentos[doc.id].total;
                }
              });
              this.setState({
                documentos,
                efectivo,
                cheque,
                transferencia,
                otros,
                credito,
                total,
                percepcion,
                iva,
                cargando: false,
              });
            });
        }

        if (tipoListado === "b") {
          db.collection("comprasCabezera")
            .where("fecha", ">=", desde)
            .where("fecha", "<=", hasta)
            .where("compraPago", "==", "compra")
            .where("aOb", "==", false)
            .orderBy("fecha")
            .orderBy("nDocumento")
            .onSnapshot((snapShot) => {
              const documentos = [];
              let total = 0;
              let percepcion = 0;
              let iva = 0;
              let efectivo = 0;
              let cheque = 0;
              let transferencia = 0;
              let otros = 0;
              let credito = 0;
              snapShot.forEach((doc) => {
                documentos[doc.id] = doc.data();
                total = total + documentos[doc.id].total;
                if (documentos[doc.id].percepcion) {
                  documentos[doc.id].percepcion = parseFloat(
                    documentos[doc.id].percepcion
                  );
                } else {
                  documentos[doc.id].percepcion = 0;
                }
                percepcion = percepcion + documentos[doc.id].percepcion;
                if (documentos[doc.id].iva) {
                  iva = iva + documentos[doc.id].iva;
                }
                if (!documentos[doc.id].estadoCuenta) {
                  if (documentos[doc.id].formaPago === "Efectivo") {
                    efectivo = efectivo + documentos[doc.id].total;
                  }
                  if (documentos[doc.id].formaPago === "Cheque") {
                    cheque = cheque + documentos[doc.id].total;
                  }
                  if (documentos[doc.id].formaPago === "Transferencia") {
                    transferencia = transferencia + documentos[doc.id].total;
                  }
                  if (documentos[doc.id].formaPago === "otros") {
                    otros = otros + documentos[doc.id].total;
                  }
                } else {
                  credito = credito + documentos[doc.id].total;
                }
              });
              this.setState({
                documentos,
                efectivo,
                cheque,
                transferencia,
                otros,
                credito,
                total,
                percepcion,
                iva,
                cargando: false,
              });
            });
        }
      });
    }
  };

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleClose = () => {
    this.setState({ show: false });
  };

  handleShow = async (id, proveedorDoc, estadoCuenta, totalDoc, lugar, compraPago) => {
    if (estadoCuenta) {
      let snapProveedor = await db
        .collection("proveedores")
        .doc(proveedorDoc)
        .get();
      let totalAdeudado = snapProveedor.data().totalAdeudado;
      if (totalAdeudado) {
        totalAdeudado = parseFloat(totalAdeudado);
      } else {
        totalAdeudado = 0;
      }
      this.setState({
        show: true,
        id: id,
        proveedorDoc,
        totalDoc,
        estadoCuenta,
        totalAdeudado,
        lugar,
        compraPago
      });
    } else {
      this.setState({
        show: true,
        id: id,
        proveedorDoc,
        totalDoc,
        estadoCuenta,
        lugar,
        compraPago
      });
    }
  };

  borrar = async () => {
    let { id, proveedorDoc, totalDoc, estadoCuenta, deudaActual, lugar, compraPago } = this.state;
    
    if(compraPago === "compra") {
      let snapStock = await db.collection("lugares").doc(lugar).collection("productos").get();
      let stock = [];
      snapStock.forEach(doc => {
        stock[doc.id] = doc.data();
      })
  
      let snapProductosABorrar = await db
        .collection("comprasProductos")
        .where("idCabezera", "==", id)
        .get();
      let productosABorrar = [];
      snapProductosABorrar.forEach((doc) => {
        productosABorrar[doc.id] = doc.data();
        productosABorrar[doc.id].stock = productosABorrar[doc.id].cantidad * (-1);
      });
      
      Object.keys(productosABorrar).forEach(id => {
        Object.keys(stock).forEach(key => {
          if(productosABorrar[id].idProducto === key) {
            productosABorrar[id].stock = parseFloat(stock[key].stock) - productosABorrar[id].cantidad;
          }
        })
      })
  
      Object.keys(productosABorrar).forEach(id => {
        let idProducto = productosABorrar[id].idProducto;
        db.collection("lugares").doc(lugar).collection("productos").doc(idProducto)
        .set({
          ...productosABorrar[id]
        })
        .then(()=> console.log("stock producto actualizado"))
        .catch(error => console.log(error));
      })
    }

    db.collection("comprasProductos")
      .where("idCabezera", "==", id)
      .get()
      .then((query) => {
        query.forEach((doc) => {
          db.collection("comprasProductos")
            .doc(doc.id)
            .delete()
            .then(() => console.log("linea borrada"))
            .catch((error) => console.log(error));
        });
        db.collection("comprasCabezera")
          .doc(id)
          .delete()
          .then(() => {
            this.setState({ show: false }, console.log("cabezera borrada"));
          })
          .catch((error) => console.log(error));
      })
      .then(() => {
        if (estadoCuenta) {
          let nuevaDeudaActual = deudaActual - totalDoc;
          db.collection("proveedores")
            .doc(proveedorDoc)
            .update({ deudaActual: nuevaDeudaActual })
            .then(() => console.log("Total Adeudado CORREGIDO"))
            .catch((error) => console.log(error));
        }
      });
  };

  detalle = (id, documento) => {
    let claseDoc = "";
    if (id === this.state.idFactura) {
      this.setState({ idFactura: "", claseDoc });
    } else {
      if (documento === "Factura Contado") {
        claseDoc = "table-danger";
      }
      if (documento === "Boleta Contado") {
        claseDoc = "table-warning";
      }
      if (documento === "Factura Crédito") {
        claseDoc = "table-active";
      }
      if (documento === "Boleta Crédito") {
        claseDoc = "table-info";
      }
      this.setState({ idFactura: id, claseDoc });
    }
  };

  handleTipoListado = (val) => {
    this.setState({ tipoListado: val }, () => {
      this.traerDocumentos();
    });
  };

  imprimir = () => {
    const { desde, hasta, documentos, total, percepcion, iva } = this.state;

    if (Object.keys(documentos).length > 0) {
      let doc = new jsPDF();

      doc.setFontSize(20);
      doc.text(65, 15, "Compras LAGO MAYORAL SA");

      doc.setFontSize(12);
      doc.text(10, 25, `Compras desde ${desde} hasta ${hasta}`);
      doc.text(5, 35, `Fecha`);
      doc.text(35, 35, `Proveedor`);
      doc.text(86, 35, `Documento`);
      doc.text(120, 35, `Nº`);
      doc.text(134, 35, `IVA`);
      doc.text(154, 35, `Percepción`);
      doc.text(183, 35, `Importe`);

      doc.line(1, 37, 205, 37);
      let co = 38;
      doc.setFontSize(10);
      Object.keys(documentos).forEach((id) => {
        co = co + 5;
        if (co >= 263) {
          doc.addPage();
          doc.setFontSize(20);
          doc.text(65, 15, "Compras LAGO MAYORAL SA");

          doc.setFontSize(12);
          doc.text(10, 25, `Compras desde ${desde} hasta ${hasta}`);
          doc.text(5, 35, `Fecha`);
          doc.text(35, 35, `Proveedor`);
          doc.text(86, 35, `Documento`);
          doc.text(120, 35, `Nº`);
          doc.text(134, 35, `IVA`);
          doc.text(154, 35, `Percepción`);
          doc.text(183, 35, `Importe`);

          doc.line(1, 37, 205, 37);
          co = 43;
          doc.setFontSize(10);
          doc.text(3, co, documentos[id].fecha);
          doc.text(23, co, documentos[id].proveedorNombre);
          doc.text(84, co, documentos[id].documento);
          doc.text(117, co, `${documentos[id].nDocumento}`);
          doc.text(132, co, documentos[id].iva.toLocaleString("de-DE"));
          doc.text(157, co, documentos[id].percepcion.toLocaleString("de-DE"));
          doc.text(183, co, documentos[id].total.toLocaleString("de-DE"));
        } else {
          doc.text(3, co, documentos[id].fecha);
          doc.text(23, co, documentos[id].proveedorNombre);
          doc.text(84, co, documentos[id].documento);
          doc.text(117, co, `${documentos[id].nDocumento}`);
          doc.text(132, co, documentos[id].iva.toLocaleString("de-DE"));
          doc.text(157, co, documentos[id].percepcion.toLocaleString("de-DE"));
          doc.text(183, co, documentos[id].total.toLocaleString("de-DE"));
        }
      });

      doc.setFontSize(12);

      co = co + 15;
      doc.text(130, co, `Total IVA: `);
      doc.text(180, co, iva.toLocaleString("de-DE"));

      co = co + 8;
      doc.text(130, co, `Total Percepción: `);
      doc.text(180, co, percepcion.toLocaleString("de-DE"));

      co = co + 8;
      doc.text(130, co, `Total Facturado: `);
      doc.text(180, co, total.toLocaleString("de-DE"));

      doc.save(`Compras LAGO MAYORAL SA desde: ${desde} hasta: ${hasta}.pdf`);
    }
  };

  separarContado = () => {
    let { documentos } = this.state;
    let docContado = [];
    let totalContado = 0;
    let ivaContado = 0;
    let subTotalContado = 0;
    let percepcionContado = 0;

    Object.keys(documentos).forEach(id => {
      if(documentos[id].documento === "Factura Contado" || documentos[id].documento === "Boleta Contado") {
        totalContado = totalContado + parseFloat(documentos[id].total);
        ivaContado = ivaContado + parseFloat(documentos[id].iva);
        subTotalContado = subTotalContado + parseFloat(documentos[id].subtotal);
        percepcionContado = percepcionContado + parseFloat(documentos[id].percepcion);
        docContado.push({...documentos[id]});
      }
    })

    this.setState({ docContado, totalContado, ivaContado, subTotalContado, percepcionContado });
  }
  
  separarCredito = () => {
    let { documentos } = this.state;
    let docCredito = [];
    let totalCredito = 0;
    let ivaCredito = 0;
    let subTotalCredito = 0;
    let percepcionCredito = 0;

    Object.keys(documentos).forEach(id => {
      if(documentos[id].documento === "Factura Crédito" || documentos[id].documento === "Boleta Crédito") {
        totalCredito = totalCredito + parseFloat(documentos[id].total);
        ivaCredito = ivaCredito + parseFloat(documentos[id].iva);
        subTotalCredito = subTotalCredito + parseFloat(documentos[id].subtotal);
        percepcionCredito = percepcionCredito + parseFloat(documentos[id].percepcion);
        docCredito.push({...documentos[id]});
      }
    })

    this.setState({ docCredito, totalCredito, ivaCredito, subTotalCredito, percepcionCredito });
  }
  
  imprimirContador = async () => {
    let separarContado = await this.separarContado();
    let separarCredito = await this.separarCredito();

    const { 
       desde,
       hasta,
       docContado,
       docCredito,
       totalContado,
       percepcionContado,
       ivaContado,
       subTotalContado,
       totalCredito,
       percepcionCredito,
       ivaCredito,
       subTotalCredito } = this.state;

    if (Object.keys(docContado).length > 0 || Object.keys(docCredito).length > 0) {
      let doc = new jsPDF();

      doc.setFontSize(20);
      doc.text(65, 15, "Compras LAGO MAYORAL SA");

      doc.setFontSize(12);
      doc.text(10, 25, `Compras desde ${desde} hasta ${hasta}`);
      let co = 35;
      if(Object.keys(docContado).length > 0) {
      doc.text(5, co, `Fecha`);
      doc.text(35, co, `Proveedor`);
      doc.text(86, co, `Documento`);
      doc.text(120, co, `Nº`);
      doc.text(134, co, `IVA`);
      doc.text(154, co, `Percepción`);
      doc.text(183, co, `Importe`);
      co = co + 2
      doc.line(1, co, 205, co);
      co = co + 1;
      doc.setFontSize(10);
      Object.keys(docContado).forEach((id) => {
        co = co + 5;
        if (co >= 263) {
          doc.addPage();
          doc.setFontSize(20);
          doc.text(65, 15, "Compras LAGO MAYORAL SA");

          doc.setFontSize(12);
          doc.text(10, 25, `Compras desde ${desde} hasta ${hasta}`);
          co = 35;
          doc.text(5, co, `Fecha`);
          doc.text(35, co, `Proveedor`);
          doc.text(86, co, `Documento`);
          doc.text(120, co, `Nº`);
          doc.text(134, co, `IVA`);
          doc.text(154, co, `Percepción`);
          doc.text(183, co, `Importe`);
          co = co + 2;
          doc.line(1, co, 205, co);
          co = co + 8;
          doc.setFontSize(10);
          doc.text(3, co, docContado[id].fecha);
          doc.text(23, co, docContado[id].proveedorNombre);
          doc.text(84, co, docContado[id].documento);
          doc.text(117, co, `${docContado[id].nDocumento}`);
          doc.text(132, co, docContado[id].iva.toLocaleString("de-DE"));
          doc.text(157, co, docContado[id].percepcion.toLocaleString("de-DE"));
          doc.text(183, co, docContado[id].total.toLocaleString("de-DE"));
        } else {
          doc.text(3, co, docContado[id].fecha);
          doc.text(23, co, docContado[id].proveedorNombre);
          doc.text(84, co, docContado[id].documento);
          doc.text(117, co, `${docContado[id].nDocumento}`);
          doc.text(132, co, docContado[id].iva.toLocaleString("de-DE"));
          doc.text(157, co, docContado[id].percepcion.toLocaleString("de-DE"));
          doc.text(183, co, docContado[id].total.toLocaleString("de-DE"));
        }
      });

        
      doc.setFontSize(12);
      
      co = co + 15;
      doc.text(130, co, `Subtotal: `);
      doc.text(180, co, subTotalContado.toLocaleString("de-DE"));

      co = co + 8;
      doc.text(130, co, `Total IVA: `);
      doc.text(180, co, ivaContado.toLocaleString("de-DE"));

      co = co + 8;
      doc.text(130, co, `Total Percepción: `);
      doc.text(180, co, percepcionContado.toLocaleString("de-DE"));

      co = co + 8;
      doc.text(130, co, `Total Facturado: `);
      doc.text(180, co, totalContado.toLocaleString("de-DE"));
      co = co + 10;
      }


      if(Object.keys(docCredito).length > 0) {

      doc.setFontSize(12);
      doc.text(5, co, `Fecha`);
      doc.text(35, co, `Proveedor`);
      doc.text(86, co, `Documento`);
      doc.text(120, co, `Nº`);
      doc.text(134, co, `IVA`);
      doc.text(154, co, `Percepción`);
      doc.text(183, co, `Importe`);
      co = co + 2;
      doc.line(1, co, 205, co);
      co = co + 1;
      doc.setFontSize(10);
      Object.keys(docCredito).forEach((id) => {
        co = co + 5;
        if (co >= 263) {
          doc.addPage();
          doc.setFontSize(20);
          doc.text(65, 15, "Compras LAGO MAYORAL SA");

          doc.setFontSize(12);
          doc.text(10, 25, `Compras desde ${desde} hasta ${hasta}`);
          co = 35;
          doc.text(5, co, `Fecha`);
          doc.text(35, co, `Proveedor`);
          doc.text(86, co, `Documento`);
          doc.text(120, co, `Nº`);
          doc.text(134, co, `IVA`);
          doc.text(154, co, `Percepción`);
          doc.text(183, co, `Importe`);
          co = co + 2
          doc.line(1, co, 205, co);
          co = co + 8;
          doc.setFontSize(10);
          doc.text(3, co, docCredito[id].fecha);
          doc.text(23, co, docCredito[id].proveedorNombre);
          doc.text(84, co, docCredito[id].documento);
          doc.text(117, co, `${docCredito[id].nDocumento}`);
          doc.text(132, co, docCredito[id].iva.toLocaleString("de-DE"));
          doc.text(157, co, docCredito[id].percepcion.toLocaleString("de-DE"));
          doc.text(183, co, docCredito[id].total.toLocaleString("de-DE"));
        } else {
          doc.text(3, co, docCredito[id].fecha);
          doc.text(23, co, docCredito[id].proveedorNombre);
          doc.text(84, co, docCredito[id].documento);
          doc.text(117, co, `${docCredito[id].nDocumento}`);
          doc.text(132, co, docCredito[id].iva.toLocaleString("de-DE"));
          doc.text(157, co, docCredito[id].percepcion.toLocaleString("de-DE"));
          doc.text(183, co, docCredito[id].total.toLocaleString("de-DE"));
        }
      });

        doc.setFontSize(12);
        
        co = co + 15;
        doc.text(130, co, `Subtotal: `);
        doc.text(180, co, subTotalCredito.toLocaleString("de-DE"));
  
        co = co + 8;
        doc.text(130, co, `Total IVA: `);
        doc.text(180, co, ivaCredito.toLocaleString("de-DE"));
  
        co = co + 8;
        doc.text(130, co, `Total Percepción: `);
        doc.text(180, co, percepcionCredito.toLocaleString("de-DE"));
  
        co = co + 8;
        doc.text(130, co, `Total Facturado: `);
        doc.text(180, co, totalCredito.toLocaleString("de-DE"));
      }

      doc.save(`Compras LAGO MAYORAL SA desde: ${desde} hasta: ${hasta} Contador.pdf`);
    }
  };

  render() {
    const {
      documentos,
      total,
      percepcion,
      iva,
      efectivo,
      cheque,
      transferencia,
      otros,
      credito,
    } = this.state;
    return (
      <div>
        <Container>
          <Row>
            <Col>
              {Object.keys(documentos).length > 0 ? (
                <Button
                  style={{ margin: "28px" }}
                  variant="dark"
                  onClick={() => {
                    this.imprimir();
                  }}
                >
                  <FaPrint size="1.5em"/>
                </Button>
              ) : null}
            </Col>
            <Col>
              <h1 style={{ padding: "15px" }}>Listado Compras</h1>
            </Col>
            <Col>
              {Object.keys(documentos).length > 0 ? (
                <Button
                  style={{ margin: "28px" }}
                  variant="outline-dark"
                  onClick={() => {
                    this.imprimirContador();
                  }}
                >
                <FaPrint size="1.5em"/> Contador
                </Button>
              ) : null}
            </Col>
          </Row>
          <Row>
          <Col style={{ textAlign: "right" }}>
              <ToggleButtonGroup
                style={{
                  height: "42px",
                  marginBottom: "25px",
                  marginTop: "25px",
                }}
                type="radio"
                name="tipoListado"
                value={this.state.tipoListado}
                onChange={this.handleTipoListado}
              >
                <ToggleButton
                  variant="outline-primary"
                  name="radio"
                  value="todos"
                >
                  Todos
                </ToggleButton>
                <ToggleButton variant="outline-success" name="radio" value="a">
                  A
                </ToggleButton>
                <ToggleButton variant="outline-danger" name="radio" value="b">
                  B
                </ToggleButton>
              </ToggleButtonGroup>
            </Col>
          </Row>
          <Row>
            <Col>
              <Form.Group>
                <Form.Label>Desde</Form.Label>
                <input
                  name="desde"
                  className="form-control text-center"
                  type="date"
                  onChange={(e) => this.handleChange(e)}
                  value={this.state.desde}
                />
              </Form.Group>
            </Col>
            <Col>
              <Form.Group>
                <Form.Label>Hasta</Form.Label>
                <input
                  name="hasta"
                  className="form-control text-center"
                  type="date"
                  onChange={(e) => this.handleChange(e)}
                  value={this.state.hasta}
                />
              </Form.Group>
            </Col>
          </Row>
          <Row>
            <Col>
              <Button
                variant="success"
                onClick={() => {
                  this.traerDocumentos();
                }}
              >
                Buscar
              </Button>
            </Col>
          </Row>
        </Container>
        {this.state.cargando ? (
          <div
            style={{
              width: "100%",
              height: "200px",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Spinner animation="grow" variant="primary" />
          </div>
        ) : (
          <div style={{ width: "100%", overflow: "scroll", marginTop: "20px" }}>
            <Table striped bordered condensed hover responsive>
              <thead>
                <tr>
                  <th>Fecha</th>
                  <th>Proveedor</th>
                  <th>Documento</th>
                  <th>Nº</th>
                  <th>IVA</th>
                  <th>Percepción</th>
                  <th>Importe</th>
                  <th />
                </tr>
              </thead>
              <tbody>
                {Object.keys(documentos).length > 0 ? (
                  Object.keys(documentos).map((id) => {
                    return (
                      <Fragment>
                        <tr
                          key={id}
                          className={
                            this.state.idFactura === id
                              ? this.state.claseDoc
                              : ""
                          }
                        >
                          <td
                            onClick={() =>
                              this.detalle(id, documentos[id].documento)
                            }
                          >
                            {documentos[id].fecha}
                          </td>
                          <td
                            onClick={() =>
                              this.detalle(id, documentos[id].documento)
                            }
                          >
                            {documentos[id].proveedorNombre}
                          </td>
                          <td
                            onClick={() =>
                              this.detalle(id, documentos[id].documento)
                            }
                          >
                            {documentos[id].documento}
                          </td>
                          <td
                            onClick={() =>
                              this.detalle(id, documentos[id].documento)
                            }
                          >
                            {documentos[id].nDocumento}
                          </td>
                          <td
                            onClick={() =>
                              this.detalle(id, documentos[id].documento)
                            }
                          >
                            {documentos[id].iva.toLocaleString("de-DE")}
                          </td>
                          <td
                            onClick={() =>
                              this.detalle(id, documentos[id].documento)
                            }
                          >
                            {documentos[id].percepcion.toLocaleString("de-DE")}
                          </td>
                          {this.state.idFactura === id ? (
                            <td
                              onClick={() =>
                                this.detalle(id, documentos[id].documento)
                              }
                            >
                              <strong>
                                {documentos[id].total.toLocaleString("de-DE")}
                              </strong>
                            </td>
                          ) : (
                            <td
                              onClick={() =>
                                this.detalle(id, documentos[id].documento)
                              }
                            >
                              {documentos[id].total.toLocaleString("de-DE")}
                            </td>
                          )}
                          <td>
                            <Button
                              variant="danger"
                              onClick={() => {
                                this.handleShow(
                                  id,
                                  documentos[id].proveedor,
                                  documentos[id].estadoCuenta,
                                  documentos[id].total,
                                  documentos[id].lugar,
                                  documentos[id].compraPago
                                );
                              }}
                            >
                              Eliminar
                            </Button>
                          </td>
                        </tr>
                        {this.state.idFactura === id ? (
                          <DetalleCompras
                            idFactura={id}
                            claseDoc={this.state.claseDoc}
                            documentos={documentos[id]}
                          />
                        ) : null}
                      </Fragment>
                    );
                  })
                ) : (
                  <tr>
                    <td colSpan="8">
                      <p>NO se encontraron resultados</p>
                    </td>
                  </tr>
                )}
              </tbody>
            </Table>
          </div>
        )}
        {Object.keys(documentos).length > 0 ? (
          <div>
            <Table>
              <tbody>
                <tr>
                  <td />
                  <td />
                  <td />
                  <td />
                  <td colSpan="2">
                    <strong>Total Efectivo</strong>
                  </td>
                  <td>
                    <strong>{efectivo.toLocaleString("de-DE")}</strong>
                  </td>
                  <td />
                </tr>
                <tr>
                  <td />
                  <td />
                  <td />
                  <td />
                  <td colSpan="2">
                    <strong>Total Cheque</strong>
                  </td>
                  <td>
                    <strong>{cheque.toLocaleString("de-DE")}</strong>
                  </td>
                  <td />
                </tr>
                <tr>
                  <td />
                  <td />
                  <td />
                  <td />
                  <td colSpan="2">
                    <strong>Total Transferencia</strong>
                  </td>
                  <td>
                    <strong>{transferencia.toLocaleString("de-DE")}</strong>
                  </td>
                  <td />
                </tr>
                <tr>
                  <td />
                  <td />
                  <td />
                  <td />
                  <td colSpan="2">
                    <strong>Total Otros</strong>
                  </td>
                  <td>
                    <strong>{otros.toLocaleString("de-DE")}</strong>
                  </td>
                  <td />
                </tr>
                <tr>
                  <td />
                  <td />
                  <td />
                  <td />
                  <td colSpan="2">
                    <strong>Total Crédito</strong>
                  </td>
                  <td>
                    <strong>{credito.toLocaleString("de-DE")}</strong>
                  </td>
                  <td />
                </tr>
                <tr>
                  <td />
                  <td />
                  <td />
                  <td />
                  <td colSpan="2">
                    <strong>Total IVA</strong>
                  </td>
                  <td>
                    <strong>{iva.toLocaleString("de-DE")}</strong>
                  </td>
                  <td />
                </tr>
                <tr>
                  <td />
                  <td />
                  <td />
                  <td />
                  <td colSpan="2">
                    <strong>Total Percepción</strong>
                  </td>
                  <td>
                    <strong>{percepcion.toLocaleString("de-DE")}</strong>
                  </td>
                  <td />
                </tr>
                <tr>
                  <td />
                  <td />
                  <td />
                  <td />
                  <td colSpan="2">
                    <strong>Total Facturado</strong>
                  </td>
                  <td>
                    <strong>{total.toLocaleString("de-DE")}</strong>
                  </td>
                  <td />
                </tr>
              </tbody>
            </Table>
          </div>
        ) : null}
        <Modal show={this.state.show} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>ATENCIÓN!!!</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            Estás seguro que quieres ELIMINAR este documento?!
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleClose}>
              Cancelar
            </Button>
            <Button
              variant="danger"
              onClick={() => {
                this.borrar();
              }}
            >
              ELIMINAR
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

export default ListadoCompras;
