import React, { Component } from "react";
import { db } from "../firebaseConfig";
import { Button, Form, Row, Col, Spinner } from "react-bootstrap";

class RecibosCompra extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fecha: "",
      documento: "Recibo",
      ventaCobro: "pago",
      nDocumento: "",
      rut: "",
      totalAdeudado: "",
      formaPago: "",
      total: "",
      estadoCuenta: true,
      vendedores: [],
      recibo: "",
      tipoDocumento: "recibo",
      direccion: "recibos-compra",
      cargandoConfirmar: false,
    };
  }

  componentDidMount() {
    this.hoy();
  }

  hoy = () => {
    let hoy = new Date();

    let año = hoy.getFullYear();
    let mes =
      hoy.getMonth() + 1 > 9 ? hoy.getMonth() + 1 : `0${hoy.getMonth() + 1}`;
    let dia = hoy.getDate() > 9 ? hoy.getDate() : `0${hoy.getDate()}`;

    const fecha = `${año}-${mes}-${dia}`;
    this.setState({ fecha });
  };

  handleChange = (e) => {
    if (e.target.name === "nDocumento") {
      let num = parseFloat(e.target.value);
      this.setState({ [e.target.name]: num }, () => this.comprobarCompleto());
    }
    if (e.target.name === "total") {
      let total = parseFloat(e.target.value);
      this.setState({ total }, () => this.comprobarCompleto());
    } else {
      this.setState(
        {
          [e.target.name]: e.target.value,
        },
        () => this.comprobarCompleto()
      );
    }
  };

  comprobarCompleto = () => {
    let { fecha, documento, nDocumento, formaPago, total } = this.state;
    let { proveedor } = this.props.estado;
    console.log(
      `fecha: ${fecha.length} documento: ${documento.length} nDocumento ${nDocumento} proveedor: ${proveedor.length}, formaPago: ${formaPago.length}`
    );
    if (
      fecha.length > 0 &&
      nDocumento > 0 &&
      proveedor.length > 0 &&
      formaPago.length > 0 &&
      total !== 0
    ) {
      this.setState({ direccion: "home" });
    } else {
      this.setState({ direccion: "recibos-compra" });
    }
  };

  confirmar = () => {
    let {
      fecha,
      documento,
      ventaCobro,
      nDocumento,
      formaPago,
      total,
      estadoCuenta,
    } = this.state;

    let totalRecibo = total * -1;

    let { proveedor, proveedorNombre, proveedorDatos } = this.props.estado;

    let cabezera = {
      ...proveedorDatos,
      proveedor: proveedor,
      proveedorNombre: proveedorNombre,
      fecha: fecha,
      documento: documento,
      ventaCobro: ventaCobro,
      nDocumento: nDocumento,
      formaPago: formaPago,
      total: totalRecibo,
      anulado: false,
      estadoCuenta: estadoCuenta,
    };

    this.setState({ cargandoConfirmar: true }, () => {
      let deudaActual = 0;
      db.collection("proveedores")
        .doc(proveedor)
        .get()
        .then((doc) => {
          if (doc.data().deudaActual) {
            deudaActual = parseFloat(doc.data().deudaActual);
          }
        })
        .then(() => {
          deudaActual = deudaActual + totalRecibo;
          db.collection("proveedores")
            .doc(proveedor)
            .update({
              deudaActual: deudaActual,
            })
            .then(() => {
              console.log("deuda corregida");
            })
            .catch((error) => console.log(error));
        })
        .catch((error) => console.log(error));

      db.collection("comprasCabezera")
        .add(cabezera)
        .then(() => {
          console.log("recibo subido");
          this.setState({
            documento: "Recibo",
            nDocumento: "",
            totalAdeudado: "",
            formaPago: "",
            total: 0,
            estadoCuenta: true,
            recibo: "",
            tipoDocumento: "recibo",
            direccion: "recibos-compra",
            cargandoConfirmar: false,
          });
        })
        .then(() => this.props.estadoCuenta())
        .catch((error) => console.log(error));
    });
  };

  render() {
    const { fecha, documento, nDocumento, formaPago, total } = this.state;

    const { proveedor, proveedorNombre, rut } = this.props.estado;

    return this.state.cargando ? (
      <div
        style={{
          width: "100%",
          height: "200px",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Spinner animation="grow" variant="primary" />
      </div>
    ) : (
      <div
        className={this.state.direccion === "home" ? "table-success" : null}
        style={{ padding: "15px" }}
      >
        <h1 style={{ padding: "25px" }}>Recibos</h1>
        <div className="container" style={{ marginTop: "15px" }}>
          <div>
            <Form>
              <Form.Group>
                <Form.Label
                  style={!fecha ? { color: "red", fontWeight: "bold" } : null}
                >
                  Fecha
                </Form.Label>
                <input
                  name="fecha"
                  className="form-control text-center"
                  type="date"
                  placeholder="Fecha"
                  onChange={(e) => this.handleChange(e)}
                  value={this.state.fecha}
                  disabled={!this.props.vendedorNombre.permisosVendedor}
                />
              </Form.Group>
              <Form.Group>
                <Form.Label
                  style={
                    !nDocumento ? { color: "red", fontWeight: "bold" } : null
                  }
                >
                  Nº
                </Form.Label>
                <input
                  name="nDocumento"
                  className="form-control"
                  type="number"
                  placeholder="Nº Documento"
                  onChange={(e) => this.handleChange(e)}
                  onBlur={this.comprobarCompleto}
                  value={this.state.nDocumento}
                />
              </Form.Group>
              <Form.Group>
                <Form.Label
                  style={
                    !formaPago ? { color: "red", fontWeight: "bold" } : null
                  }
                >
                  Forma de pago
                </Form.Label>
                <select
                  className="form-control"
                  onChange={(e) => this.handleChange(e)}
                  name="formaPago"
                  value={this.state.formaPago}
                  aria-label="Small"
                >
                  <option value="">Seleccione forma de Pago...</option>
                  <option value="Efectivo">Efectivo</option>
                  <option value="Cheque">Cheque</option>
                  <option value="Transferencia">Transferencia</option>
                  <option value="otros">otros...</option>
                </select>
              </Form.Group>
              <Form.Group>
                <Form.Label
                  style={
                    total === 0 ? { color: "red", fontWeight: "bold" } : null
                  }
                >
                  Importe
                </Form.Label>
                <input
                  name="total"
                  className="form-control"
                  type="number"
                  placeholder="Importe"
                  onChange={(e) => this.handleChange(e)}
                  value={this.state.total}
                />
              </Form.Group>
            </Form>
          </div>
          <Row style={{ textAlign: "left" }}>
            <Col>
              <p style={!fecha ? { color: "red", fontWeight: "bold" } : null}>
                Fecha: <strong>{fecha}</strong>
              </p>
            </Col>
          </Row>
          <Row style={{ textAlign: "left" }}>
            <Col>
              <p
                style={!proveedor ? { color: "red", fontWeight: "bold" } : null}
              >
                Proveedor: <strong>{proveedorNombre}</strong>
              </p>
            </Col>
          </Row>
          <Row style={{ textAlign: "left" }}>
            <Col>
              <p>
                Rut: <strong>{rut}</strong>
              </p>
            </Col>
          </Row>
          <Row style={{ textAlign: "left" }}>
            <Col>
              <p
                style={!documento ? { color: "red", fontWeight: "bold" } : null}
              >
                Documento: <strong>{documento}</strong>
              </p>
            </Col>
            <Col style={{ textAlign: "right" }}>
              <p
                style={
                  !nDocumento ? { color: "red", fontWeight: "bold" } : null
                }
              >
                Nº: <strong>{nDocumento}</strong>
              </p>
            </Col>
          </Row>
          <Row style={{ textAlign: "left" }}>
            <Col>
              <p
                style={!formaPago ? { color: "red", fontWeight: "bold" } : null}
              >
                Forma de Pago: <strong>{formaPago}</strong>
              </p>
            </Col>
          </Row>
          <Row style={{ textAlign: "left" }}>
            <Col>
              <p
                style={
                  total === 0 ? { color: "red", fontWeight: "bold" } : null
                }
              >
                TOTAL: <strong>{total.toLocaleString("de-DE")}</strong>
              </p>
            </Col>
            <Col style={{ textAlign: "right" }}>
              {this.state.direccion === "home" ? (
                this.state.cargandoConfirmar ? (
                  <Button
                    variant="outline-success"
                    disabled
                    style={{ height: "42px", marginTop: "29px" }}
                  >
                    <Spinner
                      as="span"
                      animation="grow"
                      size="sm"
                      role="status"
                      aria-hidden="true"
                    />
                    Cargando...
                  </Button>
                ) : (
                  <Button variant="outline-success" onClick={this.confirmar}>
                    Confirmar
                  </Button>
                )
              ) : null}
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

export default RecibosCompra;
