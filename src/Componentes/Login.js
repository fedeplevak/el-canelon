import React, { Component } from "react";
import { fire } from "../firebaseConfig";
import { Button, Spinner } from "react-bootstrap";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      contraseña: "",
      cargando: false
    };
  }

  login = e => {
    e.preventDefault();
    this.setState({ cargando: true }, () => {
      fire
        .auth()
        .signInWithEmailAndPassword(this.state.email, this.state.contraseña)
        .then(u => {
          console.log(u.user);
        })
        .catch(error => console.log(error));
    });
  };

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  render() {
    return (
      <div className="App">
        <h1>Login</h1>
        <form className="form-horizonatl">
          <div className="form-group">
            <label for="email" className="control-label">
              Email
            </label>
            <input
              className="form-control"
              value={this.state.email}
              onChange={this.handleChange}
              type="email"
              name="email"
              placeholder="Email"
            />
          </div>
          <div className="form-group">
            <label for="contraseña" className="control-label">
              Contraseña
            </label>
            <input
              className="form-control"
              value={this.state.contraseña}
              onChange={this.handleChange}
              type="password"
              name="contraseña"
              placeholder="Contraseña"
            />
          </div>
          {!this.state.cargando ? (
            <Button variant="light" type="submit" onClick={this.login}>
              Entrar
            </Button>
          ) : (
            <Button variant="light" disabled>
              <Spinner
                as="span"
                animation="grow"
                size="sm"
                role="status"
                aria-hidden="true"
              />
               Cargando...
            </Button>
          )}
        </form>
      </div>
    );
  }
}

export default Login;
