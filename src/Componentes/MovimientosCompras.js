import React, { Component, Fragment } from "react";
import {
  Table,
  Button,
  Spinner,
  Container,
  Row,
  Col,
  Tab,
  Nav,
  Form,
  ToggleButton,
  ToggleButtonGroup,
  Modal,
} from "react-bootstrap";
import { db } from "../firebaseConfig";
import DetalleCompras from "./DetalleCompras";
import MovimientosProductosCompras from "./MovimientosProductosCompras";

class MovimientosCompras extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: "",
      proveedorDoc: "",
      totalDoc: "",
      lugar: "",
      compraPago: "",
      estadoCuenta: "",
      deudaActual: "",
      desde: "",
      hasta: "",
      tipoListado: "todos",
      cargandoMovimientosFecha: false,
      movimientosFecha: [],
      total: 0,
      iva: 0,
      efectivo: 0,
      cheque: 0,
      transferencia: 0,
      otros: 0,
      credito: 0,
      recibos: 0,
    };
  }

  borrar = async () => {
    let {
      id,
      proveedorDoc,
      totalDoc,
      estadoCuenta,
      deudaActual,
      lugar,
      compraPago,
    } = this.state;

    if (compraPago === "compra") {
      let snapStock = await db
        .collection("lugares")
        .doc(lugar)
        .collection("productos")
        .get();
      let stock = [];
      snapStock.forEach((doc) => {
        stock[doc.id] = doc.data();
      });

      let snapProductosABorrar = await db
        .collection("comprasProductos")
        .where("idCabezera", "==", id)
        .get();
      let productosABorrar = [];
      snapProductosABorrar.forEach((doc) => {
        productosABorrar[doc.id] = doc.data();
        productosABorrar[doc.id].stock = productosABorrar[doc.id].cantidad * -1;
      });

      Object.keys(productosABorrar).forEach((id) => {
        Object.keys(stock).forEach((key) => {
          if (productosABorrar[id].idProducto === key) {
            productosABorrar[id].stock =
              parseFloat(stock[key].stock) - productosABorrar[id].cantidad;
          }
        });
      });

      Object.keys(productosABorrar).forEach((id) => {
        let idProducto = productosABorrar[id].idProducto;
        db.collection("lugares")
          .doc(lugar)
          .collection("productos")
          .doc(idProducto)
          .set({
            ...productosABorrar[id],
          })
          .then(() => console.log("stock producto actualizado"))
          .catch((error) => console.log(error));
      });
    }

    db.collection("comprasProductos")
      .where("idCabezera", "==", id)
      .get()
      .then((query) => {
        query.forEach((doc) => {
          db.collection("comprasProductos")
            .doc(doc.id)
            .delete()
            .then(() => console.log("linea borrada"))
            .catch((error) => console.log(error));
        });
        db.collection("comprasCabezera")
          .doc(id)
          .delete()
          .then(() => {
            this.setState({ show: false }, console.log("cabezera borrada"));
          })
          .catch((error) => console.log(error));
      })
      .then(() => {
        if (estadoCuenta) {
          let nuevaDeudaActual = deudaActual - totalDoc;
          db.collection("proveedores")
            .doc(proveedorDoc)
            .update({ deudaActual: nuevaDeudaActual })
            .then(() => console.log("Total Adeudado CORREGIDO"))
            .catch((error) => console.log(error));
        }
      })
      .then(() => this.props.estadoCuenta())
      .catch((error) => console.log(error));
  };

  handleClose = () => {
    this.setState({ show: false });
  };

  handleShow = async (
    id,
    proveedorDoc,
    estadoCuenta,
    totalDoc,
    lugar,
    compraPago
  ) => {
    if (estadoCuenta) {
      let snapProveedor = await db
        .collection("proveedores")
        .doc(proveedorDoc)
        .get();
      let deudaActual = snapProveedor.data().deudaActual;
      if (deudaActual) {
        deudaActual = parseFloat(deudaActual);
      } else {
        deudaActual = 0;
      }
      this.setState({
        show: true,
        id: id,
        proveedorDoc,
        totalDoc,
        estadoCuenta,
        deudaActual,
        lugar,
        compraPago,
      });
    } else {
      this.setState({
        show: true,
        id: id,
        proveedorDoc,
        totalDoc,
        estadoCuenta,
        lugar,
        compraPago,
      });
    }
  };

  detalle = (id, documento) => {
    let claseDoc = "";
    if (id === this.state.idFactura) {
      this.setState({ idFactura: "", claseDoc });
    } else {
      if (documento === "Recibo") {
        claseDoc = "table-success";
        this.setState({ idFactura: id, claseDoc });
      } else {
        if (documento === "Factura Contado") {
          claseDoc = "table-danger";
        }
        if (documento === "Boleta Contado") {
          claseDoc = "table-warning";
        }
        if (documento === "Factura Crédito") {
          claseDoc = "table-active";
        }
        if (documento === "Boleta Crédito") {
          claseDoc = "table-info";
        }
        this.setState({ idFactura: id, claseDoc });
      }
    }
  };

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleTipoListado = (val) => {
    this.setState({ tipoListado: val }, () => {
      this.traerMovimientos();
    });
  };

  traerMovimientos = () => {
    let { proveedor } = this.props.estado;
    let { desde, hasta, tipoListado } = this.state;
    this.setState({ cargandoMovimientosFecha: true }, async () => {
      if (tipoListado === "todos") {
        var snapMovimientosFecha = await db
          .collection("comprasCabezera")
          .where("proveedor", "==", proveedor)
          .where("fecha", ">=", desde)
          .where("fecha", "<=", hasta)
          .orderBy("fecha", "desc")
          .get();
      }
      if (tipoListado === "a") {
        var snapMovimientosFecha = await db
          .collection("comprasCabezera")
          .where("proveedor", "==", proveedor)
          .where("aOb", "==", true)
          .where("fecha", ">=", desde)
          .where("fecha", "<=", hasta)
          .orderBy("fecha", "desc")
          .get();
      }
      if (tipoListado === "b") {
        var snapMovimientosFecha = await db
          .collection("comprasCabezera")
          .where("proveedor", "==", proveedor)
          .where("aOb", "==", false)
          .where("fecha", ">=", desde)
          .where("fecha", "<=", hasta)
          .orderBy("fecha", "desc")
          .get();
      }

      let total = 0;
      let iva = 0;
      let efectivo = 0;
      let cheque = 0;
      let transferencia = 0;
      let otros = 0;
      let credito = 0;
      let recibos = 0;

      let movimientosFecha = [];
      snapMovimientosFecha.forEach((doc) => {
        movimientosFecha[doc.id] = doc.data();

        if (movimientosFecha[doc.id].documento === "Recibo") {
          recibos = recibos + movimientosFecha[doc.id].total * -1;
          if (movimientosFecha[doc.id].formaPago === "Efectivo") {
            efectivo = efectivo + movimientosFecha[doc.id].total * -1;
          }
          if (movimientosFecha[doc.id].formaPago === "Cheque") {
            cheque = cheque + movimientosFecha[doc.id].total * -1;
          }
          if (movimientosFecha[doc.id].formaPago === "Transferencia") {
            transferencia = transferencia + movimientosFecha[doc.id].total * -1;
          }
          if (movimientosFecha[doc.id].formaPago === "otros") {
            otros = otros + movimientosFecha[doc.id].total * -1;
          }
        } else {
          total = total + movimientosFecha[doc.id].total;
        }

        if (movimientosFecha[doc.id].iva) {
          iva = iva + movimientosFecha[doc.id].iva;
        }
        if (!movimientosFecha[doc.id].estadoCuenta) {
          if (movimientosFecha[doc.id].formaPago === "Efectivo") {
            efectivo = efectivo + movimientosFecha[doc.id].total;
          }
          if (movimientosFecha[doc.id].formaPago === "Cheque") {
            cheque = cheque + movimientosFecha[doc.id].total;
          }
          if (movimientosFecha[doc.id].formaPago === "Transferencia") {
            transferencia = transferencia + movimientosFecha[doc.id].total;
          }
          if (movimientosFecha[doc.id].formaPago === "otros") {
            otros = otros + movimientosFecha[doc.id].total;
          }
        } else {
          if (
            movimientosFecha[doc.id].documento === "Factura Credito" ||
            movimientosFecha[doc.id].documento === "Boleto Credito"
          ) {
            credito = credito + movimientosFecha[doc.id].total;
          }
        }
      });

      this.setState({
        movimientosFecha,
        cargandoMovimientosFecha: false,
        total,
        iva,
        efectivo,
        cheque,
        transferencia,
        otros,
        credito,
        recibos,
      });
    });
  };

  render() {
    const { documentos, cargando } = this.props;
    const { permisosVendedor } = this.props.vendedor;
    const {
      cargandoMovimientosFecha,
      movimientosFecha,
      total,
      iva,
      efectivo,
      cheque,
      transferencia,
      otros,
      credito,
      recibos,
    } = this.state;
    return (
      <div>
        <div className="container" style={{ marginTop: "15px" }}>
          <Tab.Container id="movimientos" defaultActiveKey="ultimosMovimientos">
            <Nav fill variant="tabs">
              <Nav.Item>
                <Nav.Link eventKey="ultimosMovimientos">
                  Últimos Movimientos
                </Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link eventKey="movimientosXFecha">
                  Movimientos x Fecha
                </Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link eventKey="movimientosProductos">
                  Movimientos Productos
                </Nav.Link>
              </Nav.Item>
            </Nav>
            <Tab.Content>
              <Tab.Pane eventKey="ultimosMovimientos">
                <Container>
                  <Row>
                    <Col>
                      <h1 style={{ padding: "15px" }}>Últimos Movimientos</h1>
                    </Col>
                  </Row>
                </Container>
                {cargando ? (
                  <div
                    style={{
                      width: "100%",
                      height: "200px",
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <Spinner animation="grow" variant="primary" />
                  </div>
                ) : (
                  <div
                    style={{
                      width: "100%",
                      overflow: "scroll",
                      marginTop: "20px",
                    }}
                  >
                    <Table striped bordered condensed hover responsive>
                      <thead>
                        <tr>
                          <th>Fecha</th>
                          <th>Proveedor</th>
                          <th>Documento</th>
                          <th>Nº</th>
                          <th>Percepción</th>
                          <th>Importe</th>
                          <th>Lugar</th>
                          {permisosVendedor ? <th /> : null}
                        </tr>
                      </thead>
                      <tbody>
                        {Object.keys(documentos).length > 0 ? (
                          Object.keys(documentos).map((id) => {
                            return (
                              <Fragment>
                                <tr
                                  key={id}
                                  className={
                                    this.state.idFactura === id
                                      ? this.state.claseDoc
                                      : ""
                                  }
                                >
                                  <td
                                    onClick={() =>
                                      this.detalle(id, documentos[id].documento)
                                    }
                                  >
                                    {documentos[id].fecha}
                                  </td>
                                  <td
                                    onClick={() =>
                                      this.detalle(id, documentos[id].documento)
                                    }
                                  >
                                    {documentos[id].proveedorNombre}
                                  </td>
                                  <td
                                    onClick={() =>
                                      this.detalle(id, documentos[id].documento)
                                    }
                                  >
                                    {documentos[id].documento}
                                  </td>
                                  <td
                                    onClick={() =>
                                      this.detalle(id, documentos[id].documento)
                                    }
                                  >
                                    {documentos[id].nDocumento}
                                  </td>
                                  <td
                                    onClick={() =>
                                      this.detalle(id, documentos[id].documento)
                                    }
                                  >
                                    {documentos[id].compraPago === "compra"
                                      ? documentos[
                                          id
                                        ].percepcion.toLocaleString("de-DE")
                                      : ""}
                                  </td>
                                  {this.state.idFactura === id ? (
                                    <td
                                      onClick={() =>
                                        this.detalle(
                                          id,
                                          documentos[id].documento
                                        )
                                      }
                                    >
                                      <strong>
                                        {documentos[id].total.toLocaleString(
                                          "de-DE"
                                        )}
                                      </strong>
                                    </td>
                                  ) : (
                                    <td
                                      onClick={() =>
                                        this.detalle(
                                          id,
                                          documentos[id].documento
                                        )
                                      }
                                    >
                                      {documentos[id].total.toLocaleString(
                                        "de-DE"
                                      )}
                                    </td>
                                  )}
                                  <td
                                    onClick={() =>
                                      this.detalle(id, documentos[id].documento)
                                    }
                                  >
                                    {documentos[id].lugarNombre}
                                  </td>
                                  {permisosVendedor ? (
                                    <td>
                                      <Button
                                        variant="danger"
                                        onClick={() => {
                                          this.handleShow(
                                            id,
                                            documentos[id].proveedor,
                                            documentos[id].estadoCuenta,
                                            documentos[id].total,
                                            documentos[id].lugar,
                                            documentos[id].compraPago
                                          );
                                        }}
                                      >
                                        Eliminar
                                      </Button>
                                    </td>
                                  ) : null}
                                </tr>
                                {this.state.idFactura === id ? (
                                  <DetalleCompras
                                    idFactura={id}
                                    claseDoc={this.state.claseDoc}
                                    documentos={documentos[id]}
                                    empresa={this.props.empresa}
                                  />
                                ) : null}
                              </Fragment>
                            );
                          })
                        ) : (
                          <tr>
                            <td colSpan="8">
                              <p>NO se encontraron resultados</p>
                            </td>
                          </tr>
                        )}
                      </tbody>
                    </Table>
                  </div>
                )}
              </Tab.Pane>
              <Tab.Pane eventKey="movimientosXFecha">
                <Container>
                  <Row
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      marginTop: "20px",
                    }}
                  >
                    {Object.keys(movimientosFecha).length > 0 ? (
                      <Button
                        variant="outline-primary"
                        onClick={() => {
                          this.imprimir();
                        }}
                      >
                        Imprimir
                      </Button>
                    ) : null}
                  </Row>
                  <Row>
                    <Col>
                      <h1 style={{ padding: "15px" }}>Movimientos x Fecha</h1>
                    </Col>
                    <Col>
                      <ToggleButtonGroup
                        style={{
                          height: "42px",
                          marginBottom: "25px",
                          marginTop: "25px",
                        }}
                        type="radio"
                        name="tipoListado"
                        value={this.state.tipoListado}
                        onChange={this.handleTipoListado}
                      >
                        <ToggleButton
                          variant="outline-primary"
                          name="radio"
                          value="todos"
                        >
                          Todos
                        </ToggleButton>
                        <ToggleButton
                          variant="outline-success"
                          name="radio"
                          value="a"
                        >
                          A
                        </ToggleButton>
                        <ToggleButton
                          variant="outline-danger"
                          name="radio"
                          value="b"
                        >
                          B
                        </ToggleButton>
                      </ToggleButtonGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col>
                      <Form.Group>
                        <Form.Label>Desde</Form.Label>
                        <input
                          name="desde"
                          className="form-control text-center"
                          type="date"
                          onChange={(e) => this.handleChange(e)}
                          value={this.state.desde}
                        />
                      </Form.Group>
                    </Col>
                    <Col>
                      <Form.Group>
                        <Form.Label>Hasta</Form.Label>
                        <input
                          name="hasta"
                          className="form-control text-center"
                          type="date"
                          onChange={(e) => this.handleChange(e)}
                          value={this.state.hasta}
                        />
                      </Form.Group>
                    </Col>
                  </Row>
                  <Row>
                    <Col>
                      <Button
                        variant="success"
                        onClick={() => {
                          this.traerMovimientos();
                        }}
                      >
                        Buscar
                      </Button>
                    </Col>
                  </Row>
                  <Row>
                    {cargandoMovimientosFecha ? (
                      <div
                        style={{
                          width: "100%",
                          height: "200px",
                          display: "flex",
                          justifyContent: "center",
                          alignItems: "center",
                        }}
                      >
                        <Spinner animation="grow" variant="primary" />
                      </div>
                    ) : (
                      <div
                        style={{
                          width: "100%",
                          overflow: "scroll",
                          marginTop: "20px",
                        }}
                      >
                        <Table striped bordered condensed hover responsive>
                          <thead>
                            <tr>
                              <th>Fecha</th>
                              <th>Proveedor</th>
                              <th>Documento</th>
                              <th>Nº</th>
                              <th>Importe</th>
                              <th>Lugar</th>
                              <th>Vendedor</th>
                              {permisosVendedor ? <th /> : null}
                            </tr>
                          </thead>
                          <tbody>
                            {Object.keys(movimientosFecha).length > 0 ? (
                              Object.keys(movimientosFecha).map((id) => {
                                return (
                                  <Fragment>
                                    <tr
                                      key={id}
                                      className={
                                        this.state.idFactura === id
                                          ? this.state.claseDoc
                                          : ""
                                      }
                                    >
                                      <td
                                        onClick={() =>
                                          this.detalle(
                                            id,
                                            movimientosFecha[id].documento
                                          )
                                        }
                                      >
                                        {movimientosFecha[id].fecha}
                                      </td>
                                      <td
                                        onClick={() =>
                                          this.detalle(
                                            id,
                                            movimientosFecha[id].documento
                                          )
                                        }
                                      >
                                        {movimientosFecha[id].proveedorNombre}
                                      </td>
                                      <td
                                        onClick={() =>
                                          this.detalle(
                                            id,
                                            movimientosFecha[id].documento
                                          )
                                        }
                                      >
                                        {movimientosFecha[id].documento}
                                      </td>
                                      <td
                                        onClick={() =>
                                          this.detalle(
                                            id,
                                            movimientosFecha[id].documento
                                          )
                                        }
                                      >
                                        {movimientosFecha[id].nDocumento}
                                      </td>
                                      {this.state.idFactura === id ? (
                                        <td
                                          onClick={() =>
                                            this.detalle(
                                              id,
                                              movimientosFecha[id].documento
                                            )
                                          }
                                        >
                                          <strong>
                                            {movimientosFecha[
                                              id
                                            ].total.toLocaleString("de-DE")}
                                          </strong>
                                        </td>
                                      ) : (
                                        <td
                                          onClick={() =>
                                            this.detalle(
                                              id,
                                              movimientosFecha[id].documento
                                            )
                                          }
                                        >
                                          {movimientosFecha[
                                            id
                                          ].total.toLocaleString("de-DE")}
                                        </td>
                                      )}
                                      <td
                                        onClick={() =>
                                          this.detalle(
                                            id,
                                            movimientosFecha[id].documento
                                          )
                                        }
                                      >
                                        {movimientosFecha[id].lugarNombre}
                                      </td>
                                      <td
                                        onClick={() =>
                                          this.detalle(
                                            id,
                                            movimientosFecha[id].documento
                                          )
                                        }
                                      >
                                        {movimientosFecha[id].vendedor}
                                      </td>
                                      {permisosVendedor ? (
                                        <td>
                                          <Button
                                            variant="danger"
                                            onClick={() => {
                                              this.handleShow(
                                                id,
                                                movimientosFecha[id].cliente,
                                                movimientosFecha[id]
                                                  .estadoCuenta,
                                                movimientosFecha[id].total,
                                                movimientosFecha[id].lugar
                                              );
                                            }}
                                          >
                                            Eliminar
                                          </Button>
                                        </td>
                                      ) : null}
                                    </tr>
                                    {this.state.idFactura === id ? (
                                      <DetalleCompras
                                        idFactura={id}
                                        claseDoc={this.state.claseDoc}
                                        documentos={movimientosFecha[id]}
                                        empresa={this.props.empresa}
                                      />
                                    ) : null}
                                  </Fragment>
                                );
                              })
                            ) : (
                              <tr>
                                <td colSpan="8">
                                  <p>NO se encontraron resultados</p>
                                </td>
                              </tr>
                            )}
                          </tbody>
                        </Table>
                      </div>
                    )}
                  </Row>
                  <Row>
                    {Object.keys(movimientosFecha).length > 0 ? (
                      <div>
                        <Table>
                          <tbody>
                            <tr>
                              <td />
                              <td />
                              <td />
                              <td />
                              <td colSpan="2">
                                <strong>Total Efectivo</strong>
                              </td>
                              <td>
                                <strong>
                                  {efectivo.toLocaleString("de-DE")}
                                </strong>
                              </td>
                              <td />
                            </tr>
                            <tr>
                              <td />
                              <td />
                              <td />
                              <td />
                              <td colSpan="2">
                                <strong>Total Cheque</strong>
                              </td>
                              <td>
                                <strong>
                                  {cheque.toLocaleString("de-DE")}
                                </strong>
                              </td>
                              <td />
                            </tr>
                            <tr>
                              <td />
                              <td />
                              <td />
                              <td />
                              <td colSpan="2">
                                <strong>Total Transferencia</strong>
                              </td>
                              <td>
                                <strong>
                                  {transferencia.toLocaleString("de-DE")}
                                </strong>
                              </td>
                              <td />
                            </tr>
                            <tr>
                              <td />
                              <td />
                              <td />
                              <td />
                              <td colSpan="2">
                                <strong>Total Otros</strong>
                              </td>
                              <td>
                                <strong>{otros.toLocaleString("de-DE")}</strong>
                              </td>
                              <td />
                            </tr>
                            <tr>
                              <td />
                              <td />
                              <td />
                              <td />
                              <td colSpan="2">
                                <strong>Total Crédito</strong>
                              </td>
                              <td>
                                <strong>
                                  {credito.toLocaleString("de-DE")}
                                </strong>
                              </td>
                              <td />
                            </tr>
                            <tr>
                              <td />
                              <td />
                              <td />
                              <td />
                              <td colSpan="2">
                                <strong>Total IVA</strong>
                              </td>
                              <td>
                                <strong>{iva.toLocaleString("de-DE")}</strong>
                              </td>
                              <td />
                            </tr>
                            <tr>
                              <td />
                              <td />
                              <td />
                              <td />
                              <td colSpan="2">
                                <strong>Total Facturado</strong>
                              </td>
                              <td>
                                <strong>{total.toLocaleString("de-DE")}</strong>
                              </td>
                              <td />
                            </tr>
                            <tr>
                              <td />
                              <td />
                              <td />
                              <td />
                              <td colSpan="2">
                                <strong>Total Cobrado</strong>
                              </td>
                              <td>
                                <strong>
                                  {recibos.toLocaleString("de-DE")}
                                </strong>
                              </td>
                              <td />
                            </tr>
                          </tbody>
                        </Table>
                      </div>
                    ) : null}
                  </Row>
                </Container>
              </Tab.Pane>
              <Tab.Pane eventKey="movimientosProductos">
                <MovimientosProductosCompras
                  proveedor={this.props.estado.proveedor}
                />
              </Tab.Pane>
            </Tab.Content>
          </Tab.Container>
        </div>

        <Modal show={this.state.show} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>ATENCIÓN!!!</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            Estás seguro que quieres ELIMINAR este documento?!
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleClose}>
              Cancelar
            </Button>
            <Button
              variant="danger"
              onClick={() => {
                this.borrar();
              }}
            >
              ELIMINAR
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

export default MovimientosCompras;
