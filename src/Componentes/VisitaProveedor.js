import React, { Component } from "react";
import { db } from "../firebaseConfig";
import {
  Form,
  Col,
  Button,
  Tab,
  Nav,
  ToggleButton,
  ToggleButtonGroup,
  Spinner,
} from "react-bootstrap";
import Compras from "./Compras";
import RecibosCompras from "./RecibosCompras";
import MovimientosCompras from "./MovimientosCompras";
import EstadoCuentaCompras from "./EstadoCuentaCompras";

class VisitaProveedor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cargandoProveedores: true,
      cargandoMovimientos: false,
      cargandoEstadoCuenta: false,
      documentos: [],
      deudaInicial: 0,
      deudaActual: "",
      proveedoresVer: "activos",
      proveedores: [],
      proveedor: "",
      proveedorNombre: "",
      rut: "",
      totalAdeudado: "",
      proveedorDatos: {},
      movimientos: [],
      cargandoProductos: true,
      productosVer: "activos",
      productos: [],
      lugares: []
    };
  }

  componentDidMount() {
    this.traerProveedores();
    this.traerLugares();
  }

  handleActivoProveedores = (val) => {
    this.setState({ proveedoresVer: val }, () => {
      this.traerProveedores();
    });
  };
  
  traerLugares = () => {
    db.collection("lugares").onSnapshot((snap) => {
      let lugares = [];
      snap.forEach((doc) => {
        lugares[doc.id] = doc.data();
      });
      this.setState({ lugares });
    });
  };

  traerProveedores = () => {
    if (this.state.proveedoresVer === "activos") {
      this.setState({ cargandoProveedores: true }, () => {
        db.collection("proveedores")
          .where("activo", "==", true)
          .orderBy("nombre")
          .onSnapshot((snapShot) => {
            const proveedores = [];
            snapShot.forEach((doc) => {
              proveedores[doc.id] = doc.data();
            });
            this.setState({ proveedores, cargandoProveedores: false });
          });
      });
    }
    if (this.state.proveedoresVer === "todos") {
      this.setState({ cargandoProveedores: true }, () => {
        db.collection("proveedores")
          .orderBy("nombre")
          .onSnapshot((snapShot) => {
            const proveedores = [];
            snapShot.forEach((doc) => {
              proveedores[doc.id] = doc.data();
            });
            this.setState({ proveedores, cargandoProveedores: false });
          });
      });
    }
  };

  handleChange = (e) => {
    if (e.target.name === "proveedor") {
      if (e.target.value) {
        this.movimientos(e.target.value);
        this.estadoCuenta(e.target.value);
        let listaProveedor = this.state.proveedores;
        let proveedorSelecionado = e.target.value;
        let proveedorNombre = e.target.selectedOptions[0].textContent;
        let totalAdeudado = "";
        let rut = "";
        let proveedorDatos = {};

        Object.keys(listaProveedor).forEach((id) => {
          if (id === proveedorSelecionado) {
            proveedorDatos = listaProveedor[id];
            rut = listaProveedor[id].rut;
            totalAdeudado = listaProveedor[id].totalAdeudado;
            if (totalAdeudado) {
              totalAdeudado = parseFloat(totalAdeudado);
            } else {
              totalAdeudado = 0;
            }
            this.setState(
              {
                proveedor: proveedorSelecionado,
                proveedorNombre,
                rut,
                totalAdeudado,
                proveedorDatos,
              },
              () => {
                this.traerProductos();
              }
            );
          }
        });
      }
    } else {
      this.setState(
        {
          [e.target.name]: e.target.value,
        },
        () => this.comprobarCompleto()
      );
    }
  };

  movimientos = (proveedor) => {
    this.setState({ cargandoMovimientos: true }, () => {
      db.collection("comprasCabezera")
        .where("proveedor", "==", proveedor)
        .orderBy("fecha", "desc")
        .limit(10)
        .onSnapshot((snap) => {
          let movimientos = [];
          snap.forEach((doc) => {
            movimientos[doc.id] = doc.data();
          });
          this.setState({ movimientos, cargandoMovimientos: false });
        });
    });
  };

  estadoCuenta = (proveedor) => {
    this.setState({ cargandoEstadoCuenta: true }, async () => {
      let snapProveedor = await db
        .collection("proveedores")
        .doc(proveedor)
        .get();
      let deudaActual = snapProveedor.data().deudaActual;
      if (!deudaActual) {
        deudaActual = 0;
      }
      let snap = await db
        .collection("comprasCabezera")
        .where("proveedor", "==", proveedor)
        .where("estadoCuenta", "==", true)
        .orderBy("fecha", "desc")
        .orderBy("documento", "desc")
        .orderBy("nDocumento", "desc")
        .limit(10)
        .get();
      let documentos = [];
      snap.forEach((doc) => {
        documentos.splice(0, 0, doc.data());
      });

      let deudaInicial = await this.deudaInicial(documentos, deudaActual);

      let docs = await this.saldo(documentos, deudaInicial);

      this.setState({
        deudaInicial,
        deudaActual,
        documentos: docs,
        cargandoEstadoCuenta: false,
      });
    });
  };

  saldo = (documentos, deudaInicial) => {
    let saldo = deudaInicial;
    Object.keys(documentos).forEach((id) => {
      let total = parseFloat(documentos[id].total);
      saldo = saldo + total;
      documentos[id].saldo = saldo;
    });
    return documentos;
  };

  deudaInicial = (documentos, deudaActual) => {
    let deudaInicial = deudaActual;
    Object.keys(documentos).forEach((id) => {
      let total = parseFloat(documentos[id].total);
      deudaInicial = deudaInicial - total;
    });
    return deudaInicial;
  };

  handleActivoProductos = (val) => {
    console.log(val);
    this.setState({ productosVer: val }, () => {
      this.traerProductos();
    });
  };

  traerProductosProveedor = async () => {
    let { proveedor } = this.state;
    let snapProductos = await db
      .collection("proveedores")
      .doc(proveedor)
      .collection("listaPrecios")
      .get();
    let productos = [];
    snapProductos.forEach((doc) => {
      productos[doc.id] = doc.data();
    });
    return productos;
  };

  traerProductos = () => {
    let { proveedor } = this.state;
    if (this.state.productosVer === "activos") {
      this.setState({ cargandoProductos: true }, async () => {
        let snapProductosActivos = await db
          .collection("productos")
          .where("activo", "==", true)
          .orderBy("orden")
          .orderBy("producto")
          .get();

        let pro = [];
        snapProductosActivos.forEach((doc) => {
          pro[doc.id] = doc.data();
          pro[doc.id].precio = 0;
        });
        if (proveedor) {
          let products = await this.traerProductosProveedor();
          Object.keys(pro).forEach((id) => {
            Object.keys(products).forEach((key) => {
              if (id === key) {
                pro[id].precio = parseFloat(products[key].precio);
              }
            });
          });
        }

        this.setState({ productos: pro, cargandoProductos: false });
      });
    }

    if (this.state.productosVer === "todos") {
      this.setState({ cargandoProductos: true }, async () => {
        let snapProductosTodos = await db
          .collection("productos")
          .orderBy("orden")
          .orderBy("producto")
          .get();
        let pro = [];
        snapProductosTodos.forEach((doc) => {
          pro[doc.id] = doc.data();
          pro[doc.id].precio = 0;
        });

        if (proveedor) {
          let products = await this.traerProductosProveedor();
          Object.keys(pro).forEach((id) => {
            Object.keys(products).forEach((key) => {
              if (id === key) {
                pro[id].precio = parseFloat(products[key].precio);
              }
            });
          });
        }

        this.setState({ productos: pro, cargandoProductos: false });
      });
    }
  };

  movimientos = (proveedor) => {
    this.setState({ cargandoMovimientos: true }, () => {
      db.collection("comprasCabezera")
        .where("proveedor", "==", proveedor)
        .orderBy("fecha", "desc")
        .orderBy("documento")
        .orderBy("nDocumento")
        .limit(10)
        .onSnapshot((snap) => {
          let movimientos = [];
          snap.forEach((doc) => {
            movimientos[doc.id] = doc.data();
          });
          this.setState({ movimientos, cargandoMovimientos: false });
        });
    });
  };

  render() {
    const { cargandoProveedores, proveedores, proveedor } = this.state;

    return this.state.cargando ? (
      <div
        style={{
          width: "100%",
          height: "200px",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Spinner animation="grow" variant="primary" />
      </div>
    ) : (
      <div style={{ maxWidth: "100vw" }}>
        <h1 style={{ padding: "25px" }}>Visita Proveedor</h1>
        <div>
          <Form style={{ display: "flex", justifyContent: "space-around" }}>
            <Form.Group as={Col} controlId="proveedor">
              <Form.Label
                style={!proveedor ? { color: "red", fontWeight: "bold" } : null}
              >
                Proveedor
              </Form.Label>
              <select
                className="form-control"
                onChange={(e) => this.handleChange(e)}
                name="proveedor"
                value={proveedor}
                disable={cargandoProveedores}
              >
                <option value="">Seleccione Proveedor...</option>
                {Object.keys(proveedores).map((id) => {
                  return (
                    <option value={id}>
                      {proveedores[id].nombre} ({proveedores[id].razonSocial})
                    </option>
                  );
                })}
              </select>
            </Form.Group>
            {this.state.cargandoProveedores ? (
              <Button
                variant="outline-primary"
                disabled
                style={{
                  height: "42px",
                  marginTop: "29px",
                  paddingRight: "15px",
                }}
              >
                <Spinner
                  as="span"
                  animation="grow"
                  size="sm"
                  role="status"
                  aria-hidden="true"
                />
                Cargando...
              </Button>
            ) : (
              <ToggleButtonGroup
                style={{
                  height: "42px",
                  marginTop: "29px",
                  paddingRight: "15px",
                }}
                type="radio"
                name="proveedoresVer"
                value={this.state.proveedoresVer}
                onChange={this.handleActivoProveedores}
              >
                <ToggleButton
                  variant="outline-success"
                  name="radio"
                  value="activos"
                >
                  Activos
                </ToggleButton>
                <ToggleButton
                  variant="outline-danger"
                  name="radio"
                  value="todos"
                >
                  Todos
                </ToggleButton>
              </ToggleButtonGroup>
            )}
          </Form>
        </div>
        <div className="container" style={{ marginTop: "15px" }}>
          <Tab.Container id="listado" defaultActiveKey="compras">
            <Nav fill variant="tabs">
              <Nav.Item>
                <Nav.Link eventKey="compras">Compras</Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link eventKey="estado-de-cuenta">
                  Estado de Cuenta
                </Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link eventKey="movimientos">Movimientos</Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link eventKey="recibo">Recibo</Nav.Link>
              </Nav.Item>
            </Nav>
            <Tab.Content>
              <Tab.Pane eventKey="compras">
                <Compras
                  estado={this.state}
                  handleActivoProductos={this.handleActivoProductos}
                  vendedor={this.props.idUsuario}
                  vendedorNombre={this.props.usuario}
                  estadoCuenta={() => this.estadoCuenta(proveedor)}
                  empresa={this.props.empresa}
                />
              </Tab.Pane>
              <Tab.Pane eventKey="estado-de-cuenta">
                <EstadoCuentaCompras
                  estado={this.state}
                  vendedorNombre={this.props.usuario}
                  empresa={this.props.empresa}
                />
              </Tab.Pane>
              <Tab.Pane eventKey="movimientos">
                <MovimientosCompras
                  estado={this.state}
                  documentos={this.state.movimientos}
                  cargando={this.state.cargandoMovimientos}
                  estadoCuenta={() => this.estadoCuenta(proveedor)}
                  vendedor={this.props.usuario}
                  empresa={this.props.empresa}
                />
              </Tab.Pane>
              <Tab.Pane eventKey="recibo">
                <RecibosCompras
                  estado={this.state}
                  handleActivoProductos={this.handleActivoProductos}
                  vendedor={this.props.idUsuario}
                  vendedorNombre={this.props.usuario}
                  estadoCuenta={() => this.estadoCuenta(proveedor)}
                  empresa={this.props.empresa}
                />
              </Tab.Pane>
            </Tab.Content>
          </Tab.Container>
        </div>
      </div>
    );
  }
}

export default VisitaProveedor;
