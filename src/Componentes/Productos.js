import React, { Component } from "react";
import { db } from "../firebaseConfig";
import {
  Table,
  Button,
  ToggleButton,
  ToggleButtonGroup,
  Form,
  Container,
  Row,
  Col,
  Modal,
  Spinner
} from "react-bootstrap";

class Productos extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cargandoProductos: true,
      productos: [],
      show: false,
      editar: "",
      orden: "",
      codigo: "",
      producto: "",
      iva: ""
    };
  }

  componentDidMount() {
    this.traerProductos();
  }

  /*  subirProductos = () => {
    let productos = [];
    
    Object.keys(productos).map(id => {
        db.collection("productos").add(productos[id]).then(()=> {console.log(`Producto ${id} Subido con Exito`)}).catch(error => {console.log(error)});
    });
  } */

  traerProductos = () => {
    db.collection("productos")
      .orderBy("orden")
      .orderBy("activo", "desc")
      .orderBy("producto")
      .onSnapshot(snapShot => {
        const productos = [];
        snapShot.forEach(doc => {
          productos[doc.id] = doc.data();
        });
        this.setState({ productos, cargandoProductos: false });
      });
  };

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleActivo = (val, id) => {
    db.collection("productos")
      .doc(id)
      .update({ activo: val })
      .then(() => console.log("pronto Activo/Inactivo"))
      .catch(error => console.log(error));
  };

  editar = (editar, orden, codigo, producto, iva) => {
    this.setState({
      editar,
      orden,
      codigo,
      producto,
      iva
    });
  };

  borrar = id => {
    db.collection("productos")
      .doc(id)
      .delete()
      .then(() => {
        this.handleClose();
      })
      .catch(error => console.log(error));
  };

  nuevo = () => {
    console.log("NUEVO");
    this.setState(
      {
        activo: true,
        orden: "",
        codigo: "",
        descripcion: "",
        iva: ""
      },
      () => {
        db.collection("productos")
          .add({
            activo: true,
            orden: 0,
            codigo: "",
            producto: "",
            iva: ""
          })
          .then(docRef => {
            this.setState({ editar: docRef.id });
          })
          .catch(error => console.log(error));
      }
    );
  };

  guardar = id => {
    let { orden, codigo, producto, iva } = this.state;
    if (!iva) {
      iva = 0;
    }
    if (!orden) {
      orden = 20;
    }
    orden = parseFloat(orden);
    db.collection("productos")
      .doc(id)
      .update({
        orden: orden,
        codigo: codigo,
        producto: producto,
        iva: iva
      })
      .then(() => {
        console.log("subido con éxito");
        this.setState({ editar: "" });
      })
      .catch(error => console.log(error));
  };

  handleClose = () => {
    this.setState({ show: false });
  };

  handleShow = id => {
    this.setState({ show: true, id: id });
  };

  verId = id => {
    console.log(id);
  };

  render() {
    const { productos, cargandoProductos } = this.state;
    const { permisosVendedor } = this.props.usuario;
    return (
      <div className="container">
        <Container>
          <div style={{ paddingTop: "15px", paddingBottom: "15px" }}>
            <Row>
              {permisosVendedor ? (
                <Col>
                  <div style={{ marginTop: "9px" }}>
                    <Button variant="outline-info" onClick={() => this.nuevo()}>
                      Nuevo
                    </Button>
                  </div>
                </Col>
              ) : null}
              <Col>
                <h1>Productos</h1>
              </Col>
            </Row>
          </div>
        </Container>
        <div style={{ width: "100%", overflow: "scroll" }}>
          {cargandoProductos ? (
            <div
              style={{
                width: "100%",
                height: "200px",
                display: "flex",
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <Spinner animation="grow" variant="primary" />
            </div>
          ) : (
            <Table striped bordered condensed hover responsive>
              <thead>
                <tr>
                  <th>Orden</th>
                  <th>Activo/Inactivo</th>
                  <th>Código</th>
                  <th>Producto</th>
                  <th>IVA</th>
                </tr>
              </thead>
              <tbody>
                {Object.keys(productos).map(id => {
                  return this.state.editar !== id ? (
                    <tr key={id}>
                      <td>
                        <strong>{productos[id].orden}</strong>
                      </td>
                      <td>
                        <ToggleButtonGroup
                          type="radio"
                          name={id}
                          value={productos[id].activo}
                          onChange={val => this.handleActivo(val, id)}
                        >
                          <ToggleButton
                            variant="outline-success"
                            name="radio"
                            value={true}
                          >
                            Activo
                          </ToggleButton>
                          <ToggleButton
                            variant="outline-danger"
                            name="radio"
                            value={false}
                          >
                            Inactivo
                          </ToggleButton>
                        </ToggleButtonGroup>
                      </td>
                      <td onClick={() => this.verId(id)}>
                        {productos[id].codigo}
                      </td>
                      <td onClick={() => this.verId(id)}>
                        {productos[id].producto}
                      </td>
                      <td onClick={() => this.verId(id)}>
                        {productos[id].iva}
                      </td>
                      {permisosVendedor ? (
                        <td>
                          <Button
                            variant="outline-warning"
                            onClick={() =>
                              this.editar(
                                id,
                                productos[id].orden,
                                productos[id].codigo,
                                productos[id].producto,
                                productos[id].iva
                              )
                            }
                          >
                            Editar
                          </Button>
                        </td>
                      ) : null}
                      {permisosVendedor ? (
                        <td>
                          <Button
                            variant="outline-danger"
                            onClick={() => this.handleShow(id)}
                          >
                            Borrar
                          </Button>
                        </td>
                      ) : null}
                    </tr>
                  ) : (
                    <tr key={id}>
                      <td>
                        <Form.Control
                          value={this.state.orden}
                          name="orden"
                          type="number"
                          placeholder="Orden"
                          onChange={e => this.handleChange(e)}
                        />
                      </td>
                      <td>
                        <ToggleButtonGroup
                          type="radio"
                          name={id}
                          value={productos[id].activo}
                          onChange={val => this.handleActivo(val, id)}
                        >
                          <ToggleButton
                            variant="outline-success"
                            name="radio"
                            value={true}
                          >
                            Activo
                          </ToggleButton>
                          <ToggleButton
                            variant="outline-danger"
                            name="radio"
                            value={false}
                          >
                            Inactivo
                          </ToggleButton>
                        </ToggleButtonGroup>
                      </td>
                      <td>
                        <Form.Control
                          value={this.state.codigo}
                          name="codigo"
                          type="text"
                          placeholder="Codigo"
                          onChange={e => this.handleChange(e)}
                        />
                      </td>
                      <td>
                        <Form.Control
                          value={this.state.producto}
                          name="producto"
                          onChange={e => this.handleChange(e)}
                          type="text"
                          placeholder="Producto"
                        />
                      </td>
                      <td>
                        <Form.Control
                          value={this.state.iva}
                          name="iva"
                          onChange={e => this.handleChange(e)}
                          type="number"
                          placeholder="IVA"
                        />
                      </td>
                      <td>
                        <Button
                          variant="outline-success"
                          onClick={() => this.guardar(id)}
                        >
                          Guardar
                        </Button>
                      </td>
                      <td>
                        <Button
                          variant="outline-danger"
                          onClick={() => this.handleShow(id)}
                        >
                          Borrar
                        </Button>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </Table>
          )}
        </div>
        <Modal show={this.state.show} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>ATENCIÓN!!!</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            Estás seguro que quieres ELIMINAR este producto?!
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleClose}>
              Cancelar
            </Button>
            <Button
              variant="danger"
              onClick={() => {
                this.borrar(this.state.id);
              }}
            >
              ELIMINAR
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

export default Productos;
