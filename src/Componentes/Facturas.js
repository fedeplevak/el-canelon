import React, { Component } from "react";
import { db } from "../firebaseConfig";
import {
  Table,
  Button,
  Form,
  Row,
  Col,
  Tab,
  Nav,
  ToggleButton,
  ToggleButtonGroup,
  Spinner,
  Modal
} from "react-bootstrap";
import jsPDF from "jspdf";
import { FaPrint } from "react-icons/fa";

class Facturas extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fecha: "",
      documento: "",
      ventaCobro: "venta",
      documentoClase: "",
      nDocumento: "",
      rut: "",
      totalAdeudado: "",
      formaPago: "",
      cantidadTotal: 0,
      subtotal: 0,
      iva: 0,
      total: 0,
      estadoCuenta: "",
      vendedores: [],
      productosFinal: [],
      boleta: "",
      facturaContado: "",
      facturaCredito: "",
      tipoDocumento: "",
      idVendedor: "",
      vendedor: "",
      cantidad: "",
      producto: "",
      precio: "",
      direccion: "facturas",
      cargandoConfirmar: false,
      banderaVendedor: false,
      show: false
    };
  }

  componentDidMount() {
    this.traerNumDoc();
    this.traerVendedor();
    this.traerVendedores();
    this.hoy();
  }

  componentWillReceiveProps(nextProps) {
    let documento = "";
    let formaPago = "";
    if (nextProps.estado.clienteDatos.documento) {
      documento = nextProps.estado.clienteDatos.documento;
    }
    if (nextProps.estado.clienteDatos.formaPago) {
      formaPago = nextProps.estado.clienteDatos.formaPago;
    }
    let documentoClase = "";
    let boleta = this.state.boleta;
    let facturaContado = this.state.facturaContado;
    let facturaCredito = this.state.facturaCredito;
    let nDocumento = "";
    let aOb = "";
    let estadoCuenta = "";
    let tipoDocumento = "";
    if (documento === "Factura Contado") {
      documentoClase = "table-danger";
      nDocumento = facturaContado;
      aOb = true;
      estadoCuenta = false;
      tipoDocumento = "remito";
    }
    if (documento === "Boleta Contado") {
      documentoClase = "table-warning";
      nDocumento = boleta;
      aOb = false;
      estadoCuenta = false;
      tipoDocumento = "boleta";
    }
    if (documento === "Factura Crédito") {
      documentoClase = "table-active";
      nDocumento = facturaCredito;
      aOb = true;
      estadoCuenta = true;
      tipoDocumento = "remito";
    }
    if (documento === "Boleta Crédito") {
      documentoClase = "table-info";
      nDocumento = boleta;
      aOb = false;
      estadoCuenta = true;
      tipoDocumento = "boleta";
    }
    this.setState({
      documento,
      formaPago,
      nDocumento,
      aOb,
      estadoCuenta,
      tipoDocumento,
      documentoClase,
    });
  }

  traerVendedores = async () => {
    let snapVendedores = await db.collection("vendedores").get();
    let vendedores = [];
    snapVendedores.forEach((doc) => {
      vendedores[doc.id] = doc.data();
    });
    this.setState({ vendedores });
  };

  traerVendedor = () => {
    let idVendedor = this.props.vendedor;
    let vendedor = this.props.vendedorNombre.vendedor;
    this.setState({ idVendedor, vendedor });
  };

  traerNumDoc = async () => {
    let { vendedor } = this.props;
    let boleta = "";
    let remito = "";
    try {
      let snapVendedor = await db.collection("vendedores").doc(vendedor).get();
      let datosVendedor = snapVendedor.data();
      boleta = snapVendedor.data().boleta;
      remito = snapVendedor.data().remito;
      this.setState({ boleta, remito, datosVendedor }, () => {
        console.log(
          `boleta: ${this.state.boleta}, facturaContado: ${this.state.remito}`
        );
      });
    } catch (error) {
      console.log(error);
    }
  };

  hoy = () => {
    let hoy = new Date();

    let año = hoy.getFullYear();
    let mes =
      hoy.getMonth() + 1 > 9 ? hoy.getMonth() + 1 : `0${hoy.getMonth() + 1}`;
    let dia = hoy.getDate() > 9 ? hoy.getDate() : `0${hoy.getDate()}`;

    const fecha = `${año}-${mes}-${dia}`;
    this.setState({ fecha });
  };

  handleChange = (e) => {
    if (e.target.name === "documento") {
      let documentoClase = "";
      let boleta = this.state.boleta;
      let remito = this.state.remito;
      let nDocumento = "";
      let aOb = "";
      let estadoCuenta = "";
      let tipoDocumento = "";
      if (e.target.value === "Factura Contado") {
        documentoClase = "table-danger";
        nDocumento = remito;
        aOb = true;
        estadoCuenta = false;
        tipoDocumento = "remito";
      }
      if (e.target.value === "Boleta Contado") {
        documentoClase = "table-warning";
        nDocumento = boleta;
        aOb = false;
        estadoCuenta = false;
        tipoDocumento = "boleta";
      }
      if (e.target.value === "Factura Crédito") {
        documentoClase = "table-active";
        nDocumento = remito;
        aOb = true;
        estadoCuenta = true;
        tipoDocumento = "remito";
      }
      if (e.target.value === "Boleta Crédito") {
        documentoClase = "table-info";
        nDocumento = boleta;
        aOb = false;
        estadoCuenta = true;
        tipoDocumento = "boleta";
      }
      this.setState(
        {
          [e.target.name]: e.target.value,
          documentoClase,
          nDocumento,
          aOb,
          estadoCuenta,
          tipoDocumento,
        },
        () => this.comprobarCompleto()
      );
    }
    if (e.target.name === "nDocumento") {
      let num = parseFloat(e.target.value);
      this.setState({ [e.target.name]: num }, () => this.comprobarCompleto());
    }
    if (e.target.name === "idVendedor") {
      let idVendedor = e.target.value;
      let vendedor = e.target.selectedOptions[0].textContent;
      this.setState({ idVendedor, vendedor, banderaVendedor: false });
    }
    if (e.target.name === "producto") {
      let pro = this.props.estado.productos;
      let precio = "";
      let id = e.target.value;
      if (pro[id].precio) {
        precio = parseFloat(pro[id].precio);
      }
      this.setState({ producto: id, precio }, () => this.comprobarCompleto());
    } else {
      this.setState(
        {
          [e.target.name]: e.target.value,
        },
        () => this.comprobarCompleto()
      );
    }
  };

  comprobarCompleto = () => {
    let {
      fecha,
      documento,
      nDocumento,
      formaPago,
      idVendedor,
      productosFinal,
    } = this.state;
    let { cliente } = this.props.estado;
    if (
      fecha.length > 0 &&
      documento.length > 0 &&
      nDocumento > 0 &&
      cliente.length > 0 &&
      idVendedor.length > 0 &&
      Object.keys(productosFinal).length > 0
    ) {
      if (documento === "Factura Contado" || documento === "Boleta Contado") {
        if (formaPago.length > 0) {
          this.setState({ direccion: "home" });
        } else {
          this.setState({ direccion: "facturas" });
        }
      } else {
        this.setState({ direccion: "home" });
      }
    } else {
      this.setState({ direccion: "facturas" });
    }
  };

  agregarProducto = () => {
    let proFinal = this.state.productosFinal;
    let pro = this.props.estado.productos;
    let { cantidad, producto, precio } = this.state;
    pro[producto].cantidad = parseFloat(cantidad);
    pro[producto].precio = parseFloat(precio);
    pro[producto].importe = cantidad * precio;
    pro[producto].idProducto = producto;
    let product = { ...pro[producto] };
    let total = 0;
    let iva = 0;
    let subtotal = 0;
    let cantidadTotal = 0;
    proFinal.push(product);
    Object.keys(proFinal).forEach((id) => {
      let imp = parseFloat("1." + proFinal[id].iva);
      total = total + proFinal[id].importe;
      iva = iva + (proFinal[id].importe - proFinal[id].importe / imp);
      subtotal = subtotal + proFinal[id].importe / imp;
      cantidadTotal = cantidadTotal + parseFloat(proFinal[id].cantidad);
    });
    this.setState(
      {
        productosFinal: proFinal,
        total,
        iva,
        subtotal,
        cantidadTotal,
        cantidad: "",
        producto: "",
        precio: "",
      },
      () => {
        this.comprobarCompleto();
      }
    );
  };

  eliminarLinea = (id) => {
    let proFinal = this.state.productosFinal;
    let total = 0;
    let iva = 0;
    let subtotal = 0;
    let cantidadTotal = 0;
    delete proFinal[id];
    Object.keys(proFinal).forEach((id) => {
      let imp = parseFloat("1." + proFinal[id].iva);
      total = total + proFinal[id].importe;
      iva = iva + (proFinal[id].importe - proFinal[id].importe / imp);
      subtotal = subtotal + proFinal[id].importe / imp;
      cantidadTotal = cantidadTotal + parseFloat(proFinal[id].cantidad);
    });
    this.setState(
      {
        productosFinal: proFinal,
        total,
        iva,
        subtotal,
        cantidadTotal,
      },
      () => {
        this.comprobarCompleto();
      }
    );
  };

  confirmar = () => {
    let {
      fecha,
      documento,
      ventaCobro,
      nDocumento,
      formaPago,
      idVendedor,
      vendedor,
      total,
      iva,
      subtotal,
      productosFinal,
      aOb,
      estadoCuenta,
    } = this.state;

    let { cliente, clienteNombre, clienteDatos } = this.props.estado;

    if (documento === "Factura Crédito" || documento === "Boleta Crédito") {
      formaPago = "";
    }

    let cabezera = {
      ...clienteDatos,
      cliente: cliente,
      clienteNombre: clienteNombre,
      fecha: fecha,
      documento: documento,
      ventaCobro: ventaCobro,
      nDocumento: nDocumento,
      formaPago: formaPago,
      idVendedor: idVendedor,
      vendedor: vendedor,
      total: total,
      iva: iva,
      subtotal: subtotal,
      anulado: false,
      aOb: aOb,
      estadoCuenta: estadoCuenta,
    };
    this.setState({ cargandoConfirmar: true }, () => {
      if (documento === "Factura Contado" || documento === "Factura Crédito") {
        db.collection("vendedores")
          .doc(idVendedor)
          .update({ remito: parseFloat(nDocumento) + 1 });
      } else {
        db.collection("vendedores")
          .doc(idVendedor)
          .update({ boleta: parseFloat(nDocumento) + 1 });
      }

      if (estadoCuenta) {
        let deudaActual = 0;
        db.collection("clientes")
          .doc(cliente)
          .get()
          .then((doc) => {
            if (doc.data().deudaActual) {
              deudaActual = parseFloat(doc.data().deudaActual);
            }
          })
          .then(() => {
            deudaActual = deudaActual + total;
            db.collection("clientes")
              .doc(cliente)
              .update({
                deudaActual: deudaActual,
              })
              .then(() => console.log("deuda corregida"))
              .catch((error) => console.log(error));
          })
          .catch((error) => console.log(error));
      }

      Object.keys(productosFinal).forEach((id) => {
        let pro = { ...productosFinal[id] };
        let idProducto = productosFinal[id].idProducto;
        db.collection("clientes")
          .doc(cliente)
          .collection("listaPrecios")
          .doc(idProducto)
          .set(pro)
          .then(() => console.log("precio actualizado"))
          .catch((error) => console.log(error));
      });

      db.collection("ventasCabezera")
        .add(cabezera)
        .then((ref) => {
          let idCabezera = ref.id;
          Object.keys(productosFinal).forEach((id) => {
            let producto = productosFinal[id];
            db.collection("ventasProductos")
              .add({
                idCabezera: idCabezera,
                ...cabezera,
                ...producto,
              })
              .then(() => {
                this.props.estadoCuenta();
                console.log("linea subida con éxito");
              })
              .catch((error) => console.log(error));
          });
        })
        .then(() => {
          db.collection("clientes")
            .doc(cliente)
            .update({
              documento: documento,
              formaPago: formaPago,
            })
            .then(() => console.log("documento y formaPago actualizado"))
            .catch((error) => console.log(error));
        })
        .then(() => {
          this.traerNumDoc();
          this.setState({
            documento,
            documentoClase: "",
            nDocumento: "",
            formaPago,
            cantidadTotal: 0,
            subtotal: 0,
            iva: 0,
            total: 0,
            estadoCuenta: "",
            productosFinal: [],
            boleta: "",
            facturaContado: "",
            facturaCredito: "",
            tipoDocumento: "",
            cantidad: "",
            producto: "",
            precio: "",
            direccion: "facturas",
            cargandoConfirmar: false,
            show: false
          });
        })
        .then(() => {
          this.props.actualizarDatosCliente()
        })
        .catch((error) => console.log(error));
    });
  };

  handleMiniPrinter = () => {
    this.setState({ show: true });
  }

  confirmarFactura = async () => {
    let imprimir = await this.miniPrinter();
    let confirmar = await this.confirmar();
  }
  
  handleClose = () => {
    this.setState({ show: false }, ()=> {
      this.confirmar();
    });
  };

  miniPrinter = () => {
    let {
      fecha,
      documento,
      nDocumento,
      vendedor,
      total,
      iva,
      subtotal,
      productosFinal,
    } = this.state;
    let { clienteNombre, rut } = this.props.estado;

    let { nCarpeta } = this.props.estado.clienteDatos;

    const { empresa, telefono } = this.props.empresa;

    let salto = ` <BR>`;

    let cabezera = "";

    documento === "Boleta Contado" || documento === "Boleta Crédito"
      ? (cabezera = `<BIG><CENTER>Distri ${empresa}<BR>
    <RIGHT><SMALL>Tel: ${telefono}<BR>
    ${salto}
    <LEFT>Fecha: ${fecha}<BR>
    <LEFT>Documento: ${documento}<BR>
    <RIGHT>Nro: ${nDocumento}<BR>
    ${salto}
    <LEFT>Cliente: <BOLD>${clienteNombre}<BR>
    ${salto}
    ${salto}
    <LEFT>Vendedor: ${vendedor}
    ${salto}
    ${salto}
    <RIGHT>Ticket SIN VALOR OFICIAL<BR>
    ${salto + salto}`)
      : (cabezera = `<BIG><CENTER>Distri ${empresa}<BR>
    <RIGHT><SMALL>Tel: ${telefono}<BR>
    ${salto}
    <LEFT>Fecha: ${fecha}<BR>
    <LEFT>Documento: ${documento}<BR>
    <RIGHT>Nro: ${nDocumento}<BR>
    ${salto}
    <LEFT>Cliente: <BOLD>${clienteNombre}<BR>
    <LEFT>Rut: <BOLD>${rut}<BR>
    <LEFT>Nro. Carperta: <BOLD>${nCarpeta}<BR>
    ${salto}
    ${salto}
    <LEFT>Vendedor: ${vendedor}
    ${salto}
    ${salto}
    <RIGHT>Ticket SIN VALOR OFICIAL<BR>
    ${salto + salto}`);

    let lineas =
      "<UNDERLINE><BOLD>CANT;;PRODUCTO;;PRECIO;;IMPORTE<BR>" +
      Object.keys(productosFinal).map((id) => {
        return `${productosFinal[id].cantidad};;${
          productosFinal[id].producto
        };;${productosFinal[id].precio.toLocaleString(
          "de-DE"
        )};;${productosFinal[id].importe.toLocaleString("de-DE")}<BR>`;
      }) +
      salto;

    let pie = "";

    documento === "Boleta Contado" || documento === "Boleta Crédito"
      ? (pie = `${salto}
    <RIGHT><BIG>TOTAL: $${total.toLocaleString("de-DE")}`)
      : (pie = `${salto}
    <RIGHT>Subtotal: $${subtotal.toLocaleString("de-DE")}<BR>
    ${salto}
    <RIGHT>IVA: $${iva.toLocaleString("de-DE")}<BR>
    ${salto}
    <RIGHT><BIG>TOTAL: $${total.toLocaleString("de-DE")}`);

    let viaEmpresa = `
      ${salto}
      <RIGHT>Via Empresa<BR>
      ${salto}
      <DLINE>
      ${salto}
    `;

    let viaCliente = `
      ${salto}
      <RIGHT>Via Cliente<BR>
      ${salto}<CUT>
    `;

    var text = cabezera + lineas + pie + viaEmpresa + cabezera + lineas + pie + viaCliente;

    var textEncoded = encodeURI(text);
    window.location.href = "quickprinter://" + textEncoded;
  };

  imprimir = () => {
    let doc = new jsPDF("p", "mm", [58, 180]);
    let {
      fecha,
      documento,
      nDocumento,
      vendedor,
      total,
      iva,
      subtotal,
      productosFinal,
    } = this.state;
    let { clienteNombre, rut } = this.props.estado;

    let { nCarpeta } = this.props.estado.clienteDatos;

    const { empresa, telefono } = this.props.empresa;

    doc.setFontSize(5);
    doc.text(6, 2, `Distri ${empresa}`);
    doc.setFontSize(2);
    doc.text(10, 3.5, `Tel: ${telefono}`);

    doc.setFontSize(2.5);
    doc.text(0.3, 5, `Fecha: ${fecha}`);
    doc.text(0.3, 6, `Documento: ${documento}`);
    doc.text(15, 6, `Nº: ${nDocumento}`);
    doc.text(0.3, 8, `Cliente: ${clienteNombre}`);
    if (documento === "Factura Contado" || documento === "Factura Crédito") {
      doc.text(0.3, 9, `Rut: ${rut}`);
      doc.text(12.5, 9, `Nº Carpeta: ${nCarpeta}`);
    }
    doc.text(0.3, 10, `Vendedor: ${vendedor}`);

    doc.text(8, 13, `Ticket SIN VALOR OFICIAL`);

    let co = 16;

    doc.setFontSize(2.3);
    doc.text(0.1, co, `Cant`);
    doc.text(6, co, `Producto`);
    doc.text(14.5, co, `Precio`);
    doc.text(17.4, co, `Importe`);
    co = co + 0.2;
    doc.setLineWidth(0.08);
    doc.line(0.3, co, 20, co);
    doc.setFontSize(2.3);
    Object.keys(productosFinal).forEach((id) => {
      co = co + 1.3;
      doc.text(0.25, co, `${productosFinal[id].cantidad}`);
      doc.text(2.9, co, `${productosFinal[id].producto}`);
      doc.text(
        14.9,
        co,
        `${productosFinal[id].precio.toLocaleString("de-DE")}`
      );
      doc.text(
        17.8,
        co,
        `${productosFinal[id].importe.toLocaleString("de-DE")}`
      );
    });

    doc.setFontSize(2.5);
    co = co + 3;
    if (documento === "Factura Contado" || documento === "Factura Crédito") {
      doc.text(11.5, co, `Subtotal: `);
      doc.text(15.5, co, subtotal.toLocaleString("de-DE"));
      co = co + 1.5;
      doc.text(13, co, `IVA: `);
      doc.text(15.5, co, iva.toLocaleString("de-DE"));
    }

    doc.setFontSize(3);
    co = co + 1.5;
    doc.text(11.5, co, `TOTAL: `);
    doc.text(15.5, co, total.toLocaleString("de-DE"));

    doc.save(`${clienteNombre}-${nDocumento}.pdf`);
  };

  banderaVendedor = () => {
    let { permisosVendedor } = this.props.vendedorNombre;
    if (permisosVendedor) {
      let { banderaVendedor } = this.state;
      banderaVendedor = !banderaVendedor;
      this.setState({ banderaVendedor, idVendedor: "", vendedor: "" });
    }
  };

  render() {
    const {
      documentoClase,
      fecha,
      documento,
      nDocumento,
      formaPago,
      cantidad,
      producto,
      precio,
      cantidadTotal,
      subtotal,
      iva,
      total,
      productosFinal,
      vendedores,
      idVendedor,
      vendedor,
    } = this.state;

    const {
      productosMostrar,
      cargandoProductos,
      clienteNombre,
      cliente,
      rut,
    } = this.props.estado;

    const { nCarpeta } = this.props.estado.clienteDatos;

    return this.state.cargando ? (
      <div
        style={{
          width: "100%",
          height: "200px",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Spinner animation="grow" variant="primary" />
      </div>
    ) : (
      <div className={`container ${documentoClase}`}>
        <h1 style={{ padding: "25px" }}>Ventas</h1>
        <div className="container" style={{ marginTop: "15px" }}>
          <Tab.Container id="listado">
            <Nav variant="tabs" defaultActiveKey="cabezera">
              <Nav.Item>
                <Nav.Link eventKey="cabezera">Cabezera</Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link eventKey="productos">Productos</Nav.Link>
              </Nav.Item>
            </Nav>
            <Tab.Content>
              <Tab.Pane eventKey="cabezera">
                <div>
                  <Form>
                    <Form.Group>
                      <Form.Label
                        style={
                          !fecha ? { color: "red", fontWeight: "bold" } : null
                        }
                      >
                        Fecha
                      </Form.Label>
                      <input
                        name="fecha"
                        className="form-control text-center"
                        type="date"
                        placeholder="Fecha"
                        onChange={(e) => this.handleChange(e)}
                        value={this.state.fecha}
                        disabled={!this.props.vendedorNombre.permisosVendedor}
                      />
                    </Form.Group>
                    <Form.Group>
                      <Form.Label
                        style={
                          !documento
                            ? { color: "red", fontWeight: "bold" }
                            : null
                        }
                      >
                        Documento
                      </Form.Label>
                      <select
                        className="form-control"
                        onChange={(e) => this.handleChange(e)}
                        name="documento"
                        value={this.state.documento}
                        aria-label="Small"
                      >
                        <option value="">Seleccione documento...</option>
                        <option value="Factura Contado">Factura Contado</option>
                        <option value="Boleta Contado">Boleta Contado</option>
                        <option value="Factura Crédito">Factura Crédito</option>
                        <option value="Boleta Crédito">Boleta Crédito</option>
                      </select>
                    </Form.Group>
                    <Form.Group>
                      <Form.Label
                        style={
                          !nDocumento
                            ? { color: "red", fontWeight: "bold" }
                            : null
                        }
                      >
                        Nº
                      </Form.Label>
                      <input
                        name="nDocumento"
                        className="form-control"
                        type="number"
                        placeholder="Nº Documento"
                        onChange={(e) => this.handleChange(e)}
                        onBlur={this.comprobarCompleto}
                        value={this.state.nDocumento}
                      />
                    </Form.Group>
                    {this.state.documento === "Factura Contado" ||
                    this.state.documento === "Boleta Contado" ? (
                      <Form.Group>
                        <Form.Label
                          style={
                            !formaPago
                              ? { color: "red", fontWeight: "bold" }
                              : null
                          }
                        >
                          Forma de pago
                        </Form.Label>
                        {this.props.vendedorNombre.permisosVendedor ? (
                          <select
                            className="form-control"
                            onChange={(e) => this.handleChange(e)}
                            name="formaPago"
                            value={this.state.formaPago}
                            aria-label="Small"
                          >
                            <option value="">
                              Seleccione forma de Pago...
                            </option>
                            <option value="Efectivo">Efectivo</option>
                            <option value="Cheque">Cheque</option>
                            <option value="Transferencia">Transferencia</option>
                            <option value="otros">otros...</option>
                          </select>
                        ) : (
                          <select
                            className="form-control"
                            onChange={(e) => this.handleChange(e)}
                            name="formaPago"
                            value={this.state.formaPago}
                            aria-label="Small"
                          >
                            <option value="">
                              Seleccione forma de Pago...
                            </option>
                            <option value="Efectivo">Efectivo</option>
                            <option value="Cheque">Cheque</option>
                          </select>
                        )}
                      </Form.Group>
                    ) : null}
                    <Form.Group>
                      <Form.Label
                        style={
                          !idVendedor
                            ? { color: "red", fontWeight: "bold" }
                            : null
                        }
                      >
                        Vendedor
                      </Form.Label>
                      {!this.state.banderaVendedor ? (
                        <p onClick={() => this.banderaVendedor()}>
                          <strong>{vendedor}</strong>
                        </p>
                      ) : (
                        <select
                          className="form-control"
                          onChange={(e) => this.handleChange(e)}
                          name="idVendedor"
                          value={this.state.idVendedor}
                          aria-label="Small"
                        >
                          <option value="">Seleccione Vendedor...</option>
                          {Object.keys(vendedores).map((id) => {
                            return (
                              <option value={id}>
                                {vendedores[id].vendedor}
                              </option>
                            );
                          })}
                        </select>
                      )}
                    </Form.Group>
                  </Form>
                </div>
              </Tab.Pane>
              <Tab.Pane eventKey="productos">
                {cargandoProductos ? (
                  <div
                    style={{
                      width: "100%",
                      height: "200px",
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <Spinner animation="grow" variant="primary" />
                  </div>
                ) : (
                  <div>
                    <Row style={{ marginBottom: "15px" }}>
                      <Col>
                        <h3 style={{ marginTop: "28px" }}>Productos</h3>
                      </Col>
                      <Col>
                        {this.state.cargandoProductos ? (
                          <Button
                            variant="outline-primary"
                            disabled
                            style={{ height: "42px", marginTop: "29px" }}
                          >
                            <Spinner
                              as="span"
                              animation="grow"
                              size="sm"
                              role="status"
                              aria-hidden="true"
                            />
                            Cargando...
                          </Button>
                        ) : (
                          <ToggleButtonGroup
                            style={{ height: "42px", marginTop: "29px" }}
                            type="radio"
                            name="productosVer"
                            value={this.props.estado.productosVer}
                            onChange={(val) =>
                              this.props.handleActivoProductos(val)
                            }
                          >
                            <ToggleButton
                              variant="outline-success"
                              name="radio"
                              value="activos"
                            >
                              Activos
                            </ToggleButton>
                            <ToggleButton
                              variant="outline-danger"
                              name="radio"
                              value="todos"
                            >
                              Todos
                            </ToggleButton>
                          </ToggleButtonGroup>
                        )}
                      </Col>
                    </Row>
                    <Row>
                      <Form style={{ width: "100%" }}>
                        <Form.Group>
                          <Form.Label>Cantidad</Form.Label>
                          <input
                            name="cantidad"
                            className="form-control text-center"
                            type="number"
                            placeholder="Cantidad"
                            onChange={(e) => this.handleChange(e)}
                            value={this.state.cantidad}
                          />
                        </Form.Group>
                        <Form.Group>
                          <Form.Label>Producto</Form.Label>
                          <input
                            className="form-control"
                            value={this.props.estado.buscador}
                            onChange={this.props.handleChangeBuscador}
                            type="text"
                            name="buscador"
                            placeholder="Buscador..."
                          />
                          <select
                            style={{ marginTop: "20px" }}
                            className="form-control"
                            onChange={(e) => this.handleChange(e)}
                            name="producto"
                            value={this.state.producto}
                            aria-label="Small"
                          >
                            <option value="">Seleccione producto...</option>
                            {Object.keys(productosMostrar).map((id) => {
                              return (
                                <option value={id}>
                                  {productosMostrar[id].producto}
                                </option>
                              );
                            })}
                          </select>
                        </Form.Group>
                        <Form.Group>
                          <Form.Label>Precio</Form.Label>
                          <input
                            name="precio"
                            className="form-control"
                            type="number"
                            placeholder="Precio"
                            onChange={(e) => this.handleChange(e)}
                            value={this.state.precio}
                          />
                        </Form.Group>
                      </Form>
                    </Row>
                    <Row>
                      {cantidad !== 0 && producto.length > 0 && precio !== 0 ? (
                        <Button
                          variant="outline-success"
                          style={{ marginBottom: "15px" }}
                          onClick={this.agregarProducto}
                        >
                          Agregar
                        </Button>
                      ) : null}
                    </Row>
                  </div>
                )}
              </Tab.Pane>
            </Tab.Content>
          </Tab.Container>
          <Row style={{ textAlign: "left" }}>
            <Col>
              <p style={!fecha ? { color: "red", fontWeight: "bold" } : null}>
                Fecha: <strong>{fecha}</strong>
              </p>
            </Col>
          </Row>
          <Row style={{ textAlign: "left" }}>
            <Col>
              <p style={!cliente ? { color: "red", fontWeight: "bold" } : null}>
                Cliente: <strong>{clienteNombre}</strong>
              </p>
            </Col>
          </Row>
          <Row style={{ textAlign: "left" }}>
            <Col>
              <p>
                Rut: <strong>{rut}</strong>
              </p>
            </Col>
            <Col style={{ textAlign: "right" }}>
              <p>
                Nº Carpeta: <strong>{nCarpeta}</strong>
              </p>
            </Col>
          </Row>
          <Row style={{ textAlign: "left" }}>
            <Col>
              <p
                style={!documento ? { color: "red", fontWeight: "bold" } : null}
              >
                Documento: <strong>{documento}</strong>
              </p>
            </Col>
            <Col style={{ textAlign: "right" }}>
              <p
                style={
                  !nDocumento ? { color: "red", fontWeight: "bold" } : null
                }
              >
                Nº: <strong>{nDocumento}</strong>
              </p>
            </Col>
          </Row>
          <Row style={{ textAlign: "left" }}>
            <Col>
              <p
                style={
                  !idVendedor ? { color: "red", fontWeight: "bold" } : null
                }
              >
                Vendedor: <strong>{vendedor}</strong>
              </p>
            </Col>
          </Row>
          {this.state.documento === "Factura Contado" ||
          this.state.documento === "Boleta Contado" ? (
            <Row style={{ textAlign: "left" }}>
              <Col>
                <p
                  style={
                    !formaPago ? { color: "red", fontWeight: "bold" } : null
                  }
                >
                  Forma de Pago: <strong>{formaPago}</strong>
                </p>
              </Col>
            </Row>
          ) : null}
        </div>
        <div style={{ width: "100%", overflow: "scroll" }}>
          <Table
            striped
            bordered
            condensed
            hover
            responsive
            style={
              cantidadTotal === 0 ? { color: "red", fontWeight: "bold" } : null
            }
          >
            <thead>
              <tr>
                <th>Cantidad</th>
                <th>Producto</th>
                <th>Precio</th>
                <th>Importe</th>
              </tr>
            </thead>
            <tbody>
              {Object.keys(productosFinal).map((id) => {
                return (
                  <tr>
                    <td>
                      {productosFinal[id].cantidad.toLocaleString("de-DE")}
                    </td>
                    <td>{productosFinal[id].producto}</td>
                    <td>{productosFinal[id].precio.toLocaleString("de-DE")}</td>
                    <td>
                      {productosFinal[id].importe.toLocaleString("de-DE")}
                    </td>
                    <td>
                      <Button
                        variant="outline-danger"
                        onClick={() => this.eliminarLinea(id)}
                      >
                        X
                      </Button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
            <tbody>
              {this.state.documento === "Factura Contado" ||
              this.state.documento === "Factura Crédito" ? (
                <tr>
                  <td />
                  <td />
                  <td>Subtotal</td>
                  <td>{subtotal.toLocaleString("de-DE")}</td>
                </tr>
              ) : null}
            </tbody>
            <tbody>
              {this.state.documento === "Factura Contado" ||
              this.state.documento === "Factura Crédito" ? (
                <tr>
                  <td />
                  <td />
                  <td>IVA</td>
                  <td>{iva.toLocaleString("de-DE")}</td>
                </tr>
              ) : null}
            </tbody>
            <tbody>
              <tr>
                <td>
                  {this.state.direccion === "home" ? (
                    this.state.cargandoConfirmar ? (
                      <Button
                        variant="outline-success"
                        disabled
                        style={{ height: "42px", marginTop: "29px" }}
                      >
                        <Spinner
                          as="span"
                          animation="grow"
                          size="sm"
                          role="status"
                          aria-hidden="true"
                        />
                        Cargando...
                      </Button>
                    ) : (
                      <Button
                        variant="outline-success"
                        onClick={this.handleMiniPrinter}
                      >
                        Confirmar
                      </Button>
                    )
                  ) : null}
                </td>
                <td>
                </td>
                <td>
                  <strong>Total</strong>
                </td>
                <td>
                  <strong>{total.toLocaleString("de-DE")}</strong>
                </td>
              </tr>
            </tbody>
          </Table>
        </div>
        <Modal show={this.state.show} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>ATENCIÓN!!!</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            Quieres IMPRIMIR este documento?!
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleClose}>
              NO
            </Button>
            <Button
              variant="outline-success"
              onClick={() => {
                this.confirmarFactura();
              }}
            >
              <FaPrint size="1em"/> IMPRIMIR
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

export default Facturas;
