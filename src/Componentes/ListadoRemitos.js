import React, { Component } from "react";
import {
  Button,
  Form,
  Container,
  Row,
  Col,
  ListGroup,
  Spinner,
} from "react-bootstrap";
import { db } from "../firebaseConfig";

class ListadoRemitos extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cargando: false,
      cargandoDetalle: false,
      documentos: [],
      clientes: [],
      remitos: [],
      productos: [],
      productosClientes: [],
      idCliente: "",
      desde: "",
      hasta: "",
      tipoListado: "todos",
      totalFacturado: 0,
    };
  }

  traerDocumentos = () => {
    const { desde, hasta } = this.state;
    let totalFacturado = 0;
    if (desde && hasta) {
      this.setState({ cargando: true }, async () => {
        let snapProductos = await db
          .collection("productos")
          .orderBy("orden")
          .get();
        let productos = [];
        snapProductos.forEach((doc) => {
          productos[doc.id] = doc.data();
          productos[doc.id].cantidad = 0;
          productos[doc.id].precio = 0;
          productos[doc.id].importe = 0;
        });

        let snapClientes = await db
          .collection("clientes")
          .orderBy("zona")
          .orderBy("nombre")
          .get();
        let clientes = [];
        snapClientes.forEach((doc) => {
          clientes[doc.id] = doc.data();
          clientes[doc.id].productos = { ...productos };
          clientes[doc.id].totalFacturado = 0;
        });

        let snapRemitos = await db
          .collection("ventasProductos")
          .where("fecha", ">=", desde)
          .where("fecha", "<=", hasta)
          .where("aOb", "==", true)
          .get();
        let remitos = [];
        snapRemitos.forEach((doc) => {
          remitos[doc.id] = doc.data();
          totalFacturado = totalFacturado + parseFloat(doc.data().importe);
        });

        Object.keys(clientes).forEach((id) => {
          Object.keys(remitos).forEach((key) => {
            if (id === remitos[key].cliente) {
              clientes[id].totalFacturado =
                clientes[id].totalFacturado + parseFloat(remitos[key].importe);
            }
          });
        });

        Object.keys(clientes).forEach((id) => {
          if (clientes[id].totalFacturado === 0) {
            delete clientes[id];
          }
        });

        this.setState({
          clientes,
          remitos,
          totalFacturado,
          productos,
          cargando: false,
        });
      });
    }
  };

  armarFactura = (cliente) => {
    let { remitos, productos, idCliente } = this.state;
    let prod = [];
    Object.keys(productos).forEach((id) => {
      prod[id] = { ...productos[id] };
    });

    if (idCliente === cliente) {
      this.setState({ idCliente: "" });
    } else {
      this.setState({ cargandoDetalle: true, idCliente: cliente }, async () => {
        Object.keys(remitos).forEach((key) => {
          if (remitos[key].cliente === cliente) {
            Object.keys(prod).forEach((id) => {
              if (id === remitos[key].idProducto) {
                prod[id].cantidad =
                  prod[id].cantidad + parseFloat(remitos[key].cantidad);
                prod[id].importe =
                  prod[id].importe + parseFloat(remitos[key].importe);
              }
            });
          }
        });

        Object.keys(prod).forEach((id) => {
          if (prod[id].cantidad === 0) {
            delete prod[id];
          } else {
            prod[id].precio = prod[id].importe / prod[id].cantidad;
          }
        });

        this.setState({
          productosClientes: prod,
          cargandoDetalle: false,
        });
      });
    }
  };

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleTipoListado = (val) => {
    this.setState({ tipoListado: val });
  };

  render() {
    const {
      clientes,
      idCliente,
      productosClientes,
      totalFacturado,
    } = this.state;
    return (
      <div>
        <Container>
          <Row>
            <Col>
              <h1 style={{ padding: "15px" }}>Listado Remitos</h1>
            </Col>
          </Row>
          <Row>
            <Col>
              <Form.Group>
                <Form.Label>Desde</Form.Label>
                <input
                  name="desde"
                  className="form-control text-center"
                  type="date"
                  onChange={(e) => this.handleChange(e)}
                  value={this.state.desde}
                />
              </Form.Group>
            </Col>
            <Col>
              <Form.Group>
                <Form.Label>Hasta</Form.Label>
                <input
                  name="hasta"
                  className="form-control text-center"
                  type="date"
                  onChange={(e) => this.handleChange(e)}
                  value={this.state.hasta}
                />
              </Form.Group>
            </Col>
          </Row>
          <Row>
            <Col>
              <Button
                variant="success"
                onClick={() => {
                  this.traerDocumentos();
                }}
              >
                Buscar
              </Button>
            </Col>
          </Row>
          {this.state.cargando ? (
            <Row>
              <div
                style={{
                  width: "100%",
                  height: "200px",
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <Spinner animation="grow" variant="primary" />
              </div>
            </Row>
          ) : (
            <div>
              <Row style={{ marginTop: "20px", marginBottom: "20px" }}>
                <Col xs={8}>
                  <strong>Clientes</strong>
                </Col>
                <Col xs={4}>
                  <strong>Total Facturado</strong>
                </Col>
              </Row>

              <ListGroup>
                {Object.keys(clientes).map((id) => {
                  return (
                    <ListGroup.Item
                      action
                      variant={id === idCliente ? "success" : "primary"}
                      onClick={() => this.armarFactura(id)}
                    >
                      <Row>
                        <Col
                          xs={7}
                          style={{ textAlign: "left" }}
                        >{`${clientes[id].nombre} (${clientes[id].razonSocial})`}</Col>
                        {id === idCliente ? (
                          <Col xs={5} style={{ textAlign: "right" }}>
                            <strong>
                              {`$${clientes[id].totalFacturado.toLocaleString(
                                "de-DE"
                              )}`}
                            </strong>
                          </Col>
                        ) : (
                          <Col xs={4} style={{ textAlign: "right" }}>
                            {`$${clientes[id].totalFacturado.toLocaleString(
                              "de-DE"
                            )}`}
                          </Col>
                        )}
                      </Row>

                      {this.state.cargandoDetalle ? (
                        <div
                          style={{
                            width: "100%",
                            height: "200px",
                            display: "flex",
                            justifyContent: "center",
                            alignItems: "center",
                            marginTop: "15px",
                          }}
                        >
                          <Spinner animation="grow" variant="primary" />
                        </div>
                      ) : id === idCliente ? (
                        <div>
                          <Row>
                            <Col xs={3}>
                              <strong>Cantidad</strong>
                            </Col>
                            <Col xs={3}>
                              <strong>Producto</strong>
                            </Col>
                            <Col xs={3}>
                              <strong>Precio</strong>
                            </Col>
                            <Col xs={3}>
                              <strong>Importe</strong>
                            </Col>
                          </Row>
                          {Object.keys(productosClientes).map((id) => {
                            return (
                              <Row>
                                <Col xs={3}>
                                  {productosClientes[
                                    id
                                  ].cantidad.toLocaleString("de-DE")}
                                </Col>
                                <Col xs={3}>
                                  {productosClientes[id].producto}
                                </Col>
                                <Col xs={3}>
                                  {`$${productosClientes[
                                    id
                                  ].precio.toLocaleString("de-DE")}`}
                                </Col>
                                <Col xs={3}>
                                  {`$${productosClientes[
                                    id
                                  ].importe.toLocaleString("de-DE")}`}
                                </Col>
                              </Row>
                            );
                          })}
                        </div>
                      ) : null}
                    </ListGroup.Item>
                  );
                })}
                <ListGroup.Item variant="primary">
                  <Row>
                    <Col style={{ textAlign: "right" }}>
                      <strong>
                        Total Facturado: ${" "}
                        {totalFacturado.toLocaleString("de-DE")}
                      </strong>
                    </Col>
                  </Row>
                </ListGroup.Item>
              </ListGroup>
            </div>
          )}
        </Container>
      </div>
    );
  }
}

export default ListadoRemitos;
