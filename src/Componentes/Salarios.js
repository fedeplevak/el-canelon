import React, { Component } from "react";
import { db } from "../firebaseConfig";
import {
  Button,
  Spinner,
  Container,
  Row,
  Col,
  Form,
  ListGroup,
  Modal,
} from "react-bootstrap";
import { MdDelete, MdClose } from "react-icons/md";
import { FaMoneyBillWave } from "react-icons/fa";
import hoy from "../funciones/hoy";

class Salarios extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cargando: true,
      cargandoSalario: false,
      salarios: [],
      vendedores: [],
      nuevo: false,
      fecha: "",
      vendedor: "",
      vendedorNombre: "",
      importe: "",
      show: false,
      id: "",
    };
  }

  componentDidMount() {
    this.traerSalarios();
    this.traerVendedores();
    this.calcularHoy();
  }

  calcularHoy = async () => {
    let fecha = await hoy();
    this.setState({ fecha });
  };

  traerSalarios = () => {
    db.collection("salarios")
      .orderBy("fecha", "desc")
      .onSnapshot((snapShot) => {
        const salarios = [];
        snapShot.forEach((doc) => {
          salarios[doc.id] = doc.data();
        });
        this.setState({ salarios, cargando: false });
      });
  };

  traerVendedores = () => {
    db.collection("vendedores")
      .orderBy("vendedor")
      .onSnapshot((snapShot) => {
        let vendedores = [];
        snapShot.forEach((doc) => {
          vendedores[doc.id] = doc.data();
        });
        this.setState({ vendedores });
      });
  };

  handleChange = (e) => {
    if (e.target.name === "vendedor") {
      this.setState({
        [e.target.name]: e.target.value,
        vendedorNombre: e.target.selectedOptions[0].textContent,
      });
    } else {
      this.setState({ [e.target.name]: e.target.value });
    }
  };

  borrarSalario = () => {
    let { id } = this.state;
    db.collection("salarios")
      .doc(id)
      .delete()
      .then(() => {
        this.handleClose();
      })
      .catch((error) => console.log(error));
  };

  guardarNuevo = () => {
    let { fecha, vendedor, vendedorNombre, importe } = this.state;
    if (importe) {
      importe = parseFloat(importe);
    }
    if (fecha && vendedor && vendedorNombre && importe) {
      this.setState({ cargandoSalario: true }, () => {
        db.collection("salarios")
          .add({
            fecha: fecha,
            vendedor: vendedor,
            vendedorNombre: vendedorNombre,
            importe: importe,
          })
          .then(() => {
            console.log("subido con éxito");
            this.setState({
              nuevo: false,
              vendedor: "",
              vendedorNombre: "",
              importe: "",
              cargandoSalario: false,
            });
          })
          .catch((error) => console.log(error));
      });
    }
  };

  handleClose = () => {
    this.setState({ show: false });
  };

  handleShow = (id) => {
    this.setState({ show: true, id: id });
  };

  banderaSalarioNuevo = () => {
    let { banderaSalarioNuevo } = this.state;
    if (banderaSalarioNuevo) {
      this.setState({ banderaSalarioNuevo: false });
    } else {
      this.setState({ banderaSalarioNuevo: true });
    }
  };

  render() {
    const { salarios, vendedores } = this.state;
    return this.state.cargando ? (
      <div
        style={{
          width: "100%",
          height: "200px",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Spinner animation="grow" variant="primary" />
      </div>
    ) : (
      <Container>
        <h1>Salarios</h1>
        <Row style={{ marginTop: "20px" }}>
          <Col xs={3}>
            {this.state.banderaSalarioNuevo ? (
              <MdClose
                color="red"
                size="1.5em"
                onClick={this.banderaSalarioNuevo}
              />
            ) : (
              <FaMoneyBillWave
                color="green"
                size="1.5em"
                onClick={this.banderaSalarioNuevo}
              />
            )}
          </Col>
        </Row>
        {this.state.banderaSalarioNuevo ? (
          <Form>
            <Form.Group controlId="formFecha">
              <Form.Label>Fecha</Form.Label>
              <input
                className="form-control"
                value={this.state.fecha}
                onChange={this.handleChange}
                type="date"
                name="fecha"
                placeholder="Fecha"
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Vendedor</Form.Label>
              <select
                className="form-control"
                onChange={(e) => this.handleChange(e)}
                value={this.state.vendedor}
                name="vendedor"
                aria-label="Small"
              >
                <option value="">Seleccione Vendedor...</option>
                {Object.keys(vendedores).map((id) => {
                  return <option value={id}>{vendedores[id].vendedor}</option>;
                })}
              </select>
            </Form.Group>
            <Form.Group controlId="formImporte">
              <Form.Label>Importe</Form.Label>
              <input
                className="form-control"
                value={this.state.importe}
                onChange={this.handleChange}
                type="number"
                name="importe"
                placeholder="Importe"
              />
            </Form.Group>

            {this.state.fecha &&
            this.state.vendedor &&
            this.state.vendedorNombre &&
            this.state.importe ? (
              !this.state.cargandoSalario ? (
                true ? (
                  <Button
                    variant="success"
                    type="submit"
                    onClick={this.guardarNuevo}
                  >
                    Guardar
                  </Button>
                ) : null
              ) : (
                <Button variant="light" disabled>
                  <Spinner
                    as="span"
                    animation="grow"
                    size="sm"
                    role="status"
                    aria-hidden="true"
                  />
                  Cargando...
                </Button>
              )
            ) : null}
          </Form>
        ) : null}
        <ListGroup variant="flush" style={{ marginTop: "15px" }}>
          {Object.keys(salarios).map((id) => {
            return (
              <ListGroup.Item key={id}>
                <Row>
                  <Col xs={3}>{salarios[id].fecha}</Col>
                  <Col xs={4}>{salarios[id].vendedorNombre}</Col>
                  <Col xs={3}>
                    {`$${salarios[id].importe.toLocaleString("de-DE")}`}
                  </Col>
                  <Col xs={2}>
                    <MdDelete
                      color="red"
                      size="1.5em"
                      onClick={() => this.handleShow(id)}
                    />
                  </Col>
                </Row>
              </ListGroup.Item>
            );
          })}
        </ListGroup>

        <Modal show={this.state.show} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Atención!!!</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            Está seguro que quiere ELIMINAR este Salario? Esta acción no podrá
            revertirse.
          </Modal.Body>
          <Modal.Footer>
            <Button variant="outline-secondary" onClick={this.handleClose}>
              Cancelar
            </Button>
            <Button variant="outline-danger" onClick={this.borrarSalario}>
              Borrar
            </Button>
          </Modal.Footer>
        </Modal>
      </Container>
    );
  }
}

export default Salarios;
