import React, { Component, Fragment } from "react";
import {
  Table,
  Button,
  Spinner,
  Form,
  Container,
  Row,
  Col,
  Modal,
} from "react-bootstrap";
import { db } from "../firebaseConfig";
import Detalle from "./Detalle";

class Guias extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cargando: true,
      documentos: [],
      productos: [],
      vendedores: [],
      fecha: "",
      vendedor: {},
      idVendedor: "",
      vendedorNombre: "",
      banderaVendedor: false,
      id: "",
      totalKg: 0,
      idFactura: "",
    };
  }

  componentDidMount() {
    this.traerVendedores();
    this.traerVendedor();
    this.hoy();
  }

  hoy = () => {
    let hoy = new Date();

    let año = hoy.getFullYear();
    let mes =
      hoy.getMonth() + 1 > 9 ? hoy.getMonth() + 1 : `0${hoy.getMonth() + 1}`;
    let dia = hoy.getDate() > 9 ? hoy.getDate() : `0${hoy.getDate()}`;

    const fecha = `${año}-${mes}-${dia}`;
    this.setState({ fecha });
  };

  banderaVendedor = () => {
    let { permisosVendedor } = this.props.vendedor;
    if (permisosVendedor) {
      let { banderaVendedor } = this.state;
      banderaVendedor = !banderaVendedor;
      this.setState({ banderaVendedor, idVendedor: "", vendedor: "" });
    }
  };

  traerVendedores = async () => {
    let snapVendedores = await db.collection("vendedores").get();
    let vendedores = [];
    snapVendedores.forEach((doc) => {
      vendedores[doc.id] = doc.data();
    });
    this.setState({ vendedores, cargando: false });
  };

  traerVendedor = () => {
    let idVendedor = this.props.idVendedor;
    let vendedor = this.props.vendedor;
    let vendedorNombre = this.props.vendedor.vendedor;
    this.setState({ idVendedor, vendedor, vendedorNombre });
  };

  traerDocumentos = () => {
    const { fecha, idVendedor } = this.state;
    this.setState({ cargando: true, totalKg: 0 }, async () => {
      let productos = [];
      let documentos = [];

      if (idVendedor) {
        let snapDocumentos = await db
          .collection("ventasCabezera")
          .where("fecha", "==", fecha)
          .where("idVendedor", "==", idVendedor)
          .where("aOb", "==", true)
          .orderBy("nDocumento")
          .get();
        snapDocumentos.forEach((doc) => {
          documentos[doc.id] = doc.data();
          documentos[doc.id].cantidad = 0;
          documentos[doc.id].class = "table-success";
        });

        let snapProductos = await db
          .collection("ventasProductos")
          .where("fecha", "==", fecha)
          .where("idVendedor", "==", idVendedor)
          .where("aOb", "==", true)
          .orderBy("orden")
          .get();
        snapProductos.forEach((doc) => {
          productos[doc.id] = doc.data();
        });
      } else {
        let snapDocumentos = await db
          .collection("ventasCabezera")
          .where("fecha", "==", fecha)
          .where("aOb", "==", true)
          .orderBy("nDocumento")
          .get();
        snapDocumentos.forEach((doc) => {
          documentos[doc.id] = doc.data();
          documentos[doc.id].cantidad = 0;
          documentos[doc.id].class = "table-success";
        });

        let snapProductos = await db
          .collection("ventasProductos")
          .where("fecha", "==", fecha)
          .where("aOb", "==", true)
          .orderBy("orden")
          .get();
        snapProductos.forEach((doc) => {
          productos[doc.id] = doc.data();
        });
      }

      Object.keys(documentos).forEach((id) => {
        Object.keys(productos).forEach((key) => {
          if (id === productos[key].idCabezera) {
            documentos[id].cantidad =
              documentos[id].cantidad + parseFloat(productos[key].cantidad);
            if (productos[key].idProducto !== "HUelMYmTE3yRPDywipkq") {
              documentos[id].class = "table-danger";
            }
          }
        });
      });

      let totalKg = 0;
      Object.keys(productos).forEach((id) => {
        totalKg = totalKg + parseFloat(productos[id].cantidad);
      });

      this.setState({
        documentos,
        totalKg,
        cargando: false,
      });
    });
  };

  handleChange = (e) => {
    if (e.target.name === "idVendedor") {
      let idVendedor = e.target.value;
      let vendedorNombre = e.target.selectedOptions[0].textContent;
      this.setState({ idVendedor, vendedorNombre, banderaVendedor: false });
    } else {
      this.setState({ [e.target.name]: e.target.value });
    }
  };

  detalle = (id, documento) => {
    let claseDoc = "";
    if (id === this.state.idFactura) {
      this.setState({ idFactura: "", claseDoc });
    } else {
      if (documento === "Recibo") {
        claseDoc = "table-success";
        this.setState({ idFactura: id, claseDoc });
      } else {
        if (documento === "Factura Contado") {
          claseDoc = "table-danger";
        }
        if (documento === "Boleta Contado") {
          claseDoc = "table-warning";
        }
        if (documento === "Factura Crédito") {
          claseDoc = "table-active";
        }
        if (documento === "Boleta Crédito") {
          claseDoc = "table-info";
        }
        this.setState({ idFactura: id, claseDoc });
      }
    }
  };

  render() {
    const { documentos, vendedores, vendedorNombre, totalKg } = this.state;
    return (
      <div>
        <Container>
          <Row>
            <Col>
              <h1 style={{ padding: "15px" }}>Guías</h1>
            </Col>
          </Row>
          <Row>
            <Col>
              <Form.Group>
                <Form.Label>Fecha</Form.Label>
                <input
                  name="fecha"
                  className="form-control text-center"
                  type="date"
                  onChange={(e) => this.handleChange(e)}
                  value={this.state.fecha}
                  disabled={!this.props.vendedor.permisosVendedor}
                />
              </Form.Group>
            </Col>
            <Col>
              <Form.Group>
                <Form.Label>Vendedor</Form.Label>
                {!this.state.banderaVendedor ? (
                  <p onClick={() => this.banderaVendedor()}>
                    <strong>{vendedorNombre}</strong>
                  </p>
                ) : (
                  <select
                    className="form-control"
                    onChange={(e) => this.handleChange(e)}
                    name="idVendedor"
                    value={this.state.idVendedor}
                    aria-label="Small"
                  >
                    <option value="">Todos...</option>
                    {Object.keys(vendedores).map((id) => {
                      return (
                        <option value={id}>{vendedores[id].vendedor}</option>
                      );
                    })}
                  </select>
                )}
              </Form.Group>
            </Col>
          </Row>
          <Row>
            <Col>
              <Button
                variant="success"
                onClick={() => {
                  this.traerDocumentos();
                }}
              >
                Buscar
              </Button>
            </Col>
          </Row>
        </Container>
        {this.state.cargando ? (
          <div
            style={{
              width: "100%",
              height: "200px",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Spinner animation="grow" variant="primary" />
          </div>
        ) : (
          <div style={{ marginTop: "20px" }}>
            <div
              style={{ width: "100%", overflow: "scroll", marginTop: "20px" }}
            >
              <Table striped bordered condensed hover responsive>
                <thead>
                  <tr>
                    <th>Fecha</th>
                    <th>Nº Carpeta</th>
                    <th>Cliente</th>
                    <th>Nº Factura</th>
                    <th>Kg</th>
                  </tr>
                </thead>
                <tbody>
                  {Object.keys(documentos).length > 0 ? (
                    Object.keys(documentos).map((id) => {
                      return (
                        <Fragment>
                          <tr key={id} className={documentos[id].class}>
                            <td
                              onClick={() =>
                                this.detalle(id, documentos[id].documento)
                              }
                            >
                              {documentos[id].fecha}
                            </td>
                            <td
                              onClick={() =>
                                this.detalle(id, documentos[id].documento)
                              }
                            >
                              {documentos[id].nCarpeta}
                            </td>
                            <td
                              onClick={() =>
                                this.detalle(id, documentos[id].documento)
                              }
                            >
                              {documentos[id].clienteNombre}
                            </td>

                            <td
                              onClick={() =>
                                this.detalle(id, documentos[id].documento)
                              }
                            >
                              {documentos[id].nDocumento}
                            </td>
                            <td
                              onClick={() =>
                                this.detalle(id, documentos[id].documento)
                              }
                            >
                              {documentos[id].cantidad.toLocaleString("de-DE")}
                            </td>
                          </tr>
                          {this.state.idFactura === id ? (
                            <Detalle
                              idFactura={id}
                              claseDoc={this.state.claseDoc}
                              documentos={documentos[id]}
                            />
                          ) : null}
                        </Fragment>
                      );
                    })
                  ) : (
                    <tr>
                      <td colSpan="8">
                        <p>NO se encontraron resultados</p>
                      </td>
                    </tr>
                  )}
                </tbody>
              </Table>
            </div>
          </div>
        )}
        {Object.keys(documentos).length > 0 ? (
          <div>
            <Table>
              <tbody>
                <tr>
                  <td />
                  <td />
                  <td colSpan="2">
                    <strong>Total Kg</strong>
                  </td>
                  <td>
                    <strong>{totalKg.toLocaleString("de-DE")}</strong>
                  </td>
                  <td />
                </tr>
              </tbody>
            </Table>
          </div>
        ) : null}
        <Modal show={this.state.show} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>ATENCIÓN!!!</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            Estás seguro que quieres ELIMINAR este documento?!
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleClose}>
              Cancelar
            </Button>
            <Button
              variant="danger"
              onClick={() => {
                this.borrar();
              }}
            >
              ELIMINAR
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

export default Guias;
