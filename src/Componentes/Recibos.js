import React, { Component } from "react";
import { db } from "../firebaseConfig";
import { Button, Form, Row, Col, Spinner, Modal } from "react-bootstrap";
import jsPDF from "jspdf";
import { FaPrint } from "react-icons/fa";

class Recibos extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fecha: "",
      documento: "Recibo",
      ventaCobro: "cobro",
      nDocumento: "",
      rut: "",
      totalAdeudado: "",
      formaPago: "",
      total: "",
      estadoCuenta: true,
      vendedores: [],
      recibo: "",
      tipoDocumento: "recibo",
      idVendedor: "",
      vendedor: "",
      direccion: "recibos",
      cargandoConfirmar: false,
      show: false
    };
  }

  componentDidMount() {
    this.traerNumDoc();
    this.traerVendedor();
    this.traerVendedores();
    this.hoy();
  }

  traerVendedores = async () => {
    let snapVendedores = await db.collection("vendedores").get();
    let vendedores = [];
    snapVendedores.forEach((doc) => {
      vendedores[doc.id] = doc.data();
    });
    this.setState({ vendedores });
  };

  traerVendedor = () => {
    let idVendedor = this.props.vendedor;
    let vendedor = this.props.vendedorNombre.vendedor;
    this.setState({ idVendedor, vendedor });
  };

  traerNumDoc = async () => {
    let { vendedor } = this.props;
    let recibo = "";
    try {
      let snapVendedor = await db.collection("vendedores").doc(vendedor).get();
      let datosVendedor = snapVendedor.data();
      recibo = snapVendedor.data().recibo;
      this.setState({ recibo, datosVendedor }, () => {
        console.log(`boleta: ${this.state.recibo}`);
      });
    } catch (error) {
      console.log(error);
    }
  };

  hoy = () => {
    let hoy = new Date();

    let año = hoy.getFullYear();
    let mes =
      hoy.getMonth() + 1 > 9 ? hoy.getMonth() + 1 : `0${hoy.getMonth() + 1}`;
    let dia = hoy.getDate() > 9 ? hoy.getDate() : `0${hoy.getDate()}`;

    const fecha = `${año}-${mes}-${dia}`;
    this.setState({ fecha });
  };

  handleChange = (e) => {
    if (e.target.name === "nDocumento") {
      let num = parseFloat(e.target.value);
      this.setState({ [e.target.name]: num }, () => this.comprobarCompleto());
    }
    if (e.target.name === "idVendedor") {
      let idVendedor = e.target.value;
      let vendedor = e.target.selectedOptions[0].textContent;
      this.setState({ idVendedor, vendedor }, () => this.comprobarCompleto());
    }
    if (e.target.name === "total") {
      let total = parseFloat(e.target.value);
      this.setState({ total }, () => this.comprobarCompleto());
    } else {
      this.setState(
        {
          [e.target.name]: e.target.value,
        },
        () => this.comprobarCompleto()
      );
    }
  };

  comprobarCompleto = () => {
    let {
      fecha,
      documento,
      nDocumento,
      formaPago,
      idVendedor,
      total,
    } = this.state;
    let { cliente } = this.props.estado;
    console.log(
      `fecha: ${fecha.length} documento: ${documento.length} nDocumento ${nDocumento} cliente: ${cliente.length}, formaPago: ${formaPago.length}`
    );
    if (
      fecha.length > 0 &&
      nDocumento > 0 &&
      cliente.length > 0 &&
      formaPago.length > 0 &&
      idVendedor.length > 0 &&
      total !== 0
    ) {
      this.setState({ direccion: "home" });
    } else {
      this.setState({ direccion: "recibos" });
    }
  };

  confirmar = () => {
    let {
      fecha,
      documento,
      ventaCobro,
      nDocumento,
      formaPago,
      idVendedor,
      vendedor,
      total,
      estadoCuenta,
      tipoDocumento,
    } = this.state;

    let totalRecibo = total * -1;

    let { cliente, clienteNombre, clienteDatos } = this.props.estado;

    let cabezera = {
      ...clienteDatos,
      cliente: cliente,
      clienteNombre: clienteNombre,
      fecha: fecha,
      documento: documento,
      ventaCobro: ventaCobro,
      nDocumento: nDocumento,
      formaPago: formaPago,
      idVendedor: idVendedor,
      vendedor: vendedor,
      total: totalRecibo,
      anulado: false,
      estadoCuenta: estadoCuenta,
    };

    this.setState({ cargandoConfirmar: true }, () => {
      db.collection("vendedores")
        .doc(idVendedor)
        .update({ [tipoDocumento]: parseFloat(nDocumento) + 1 });

      let deudaActual = 0;
      db.collection("clientes")
        .doc(cliente)
        .get()
        .then((doc) => {
          if (doc.data().deudaActual) {
            deudaActual = parseFloat(doc.data().deudaActual);
          }
        })
        .then(() => {
          deudaActual = deudaActual + totalRecibo;
          db.collection("clientes")
            .doc(cliente)
            .update({
              deudaActual: deudaActual,
            })
            .then(() => {
              console.log("deuda corregida");
            })
            .catch((error) => console.log(error));
        })
        .catch((error) => console.log(error));

      db.collection("ventasCabezera")
        .add(cabezera)
        .then(() => {
          console.log("recibo subido");
          this.traerNumDoc();
          this.setState({
            documento: "Recibo",
            nDocumento: "",
            totalAdeudado: "",
            formaPago: "",
            total: 0,
            estadoCuenta: true,
            recibo: "",
            tipoDocumento: "recibo",
            direccion: "recibos",
            cargandoConfirmar: false,
            show: false
          });
        })
        .then(() => this.props.estadoCuenta())
        .catch((error) => console.log(error));
    });
  };

  imprimir = () => {
    let doc = new jsPDF("p", "mm", [58, 180]);
    let { fecha, documento, nDocumento, vendedor, total } = this.state;

    let { clienteNombre, rut } = this.props.estado;

    const { empresa, telefono } = this.props.empresa;

    doc.setFontSize(5);
    doc.text(6, 2, `Distri ${empresa}`);
    doc.setFontSize(2);
    doc.text(10, 3.5, `Tel: ${telefono}`);

    doc.setFontSize(2.5);
    doc.text(0.3, 5, `Fecha: ${fecha}`);
    doc.text(0.3, 6, `Documento: ${documento}`);
    doc.text(15, 6, `Nº: ${nDocumento}`);
    doc.text(0.3, 8, `Cliente: ${clienteNombre}`);
    doc.text(0.3, 9, `Rut: ${rut}`);
    doc.text(0.3, 10, `Vendedor: ${vendedor}`);

    doc.text(8, 13, `Ticket SIN VALOR OFICIAL`);

    doc.setFontSize(2.5);
    doc.text(2, 16, `Recibí de: `);
    doc.text(1, 17, `${clienteNombre}, `);
    doc.text(1, 18, `la cantidad de:`);
    doc.setFontSize(3);
    doc.text(7, 18, `$${total.toLocaleString("de-DE")}`);

    doc.save(`${clienteNombre}-${nDocumento}.pdf`);
  };

  miniPrinterRecibo = () => {
    let {
      fecha,
      documento,
      nDocumento,
      formaPago,
      vendedor,
      total,
    } = this.state;

    let { clienteNombre, rut } = this.props.estado;

    const { empresa, telefono } = this.props.empresa;

    let salto = ` <BR>`;

    let cabezera = "";

    cabezera = `<BIG><CENTER>Distri ${empresa}<BR>
    <RIGHT><SMALL>Tel: ${telefono}<BR>
    ${salto}
    <LEFT>Fecha: ${fecha}<BR>
    <LEFT>Documento: ${documento}<BR>
    <RIGHT>Nro: ${nDocumento}<BR>
    ${salto}
    <LEFT>Cliente: <BOLD>${clienteNombre}<BR>
    <LEFT>Rut: <BOLD>${rut}<BR>
    ${salto}
    ${salto}
    <LEFT>Vendedor: ${vendedor}
    ${salto}
    ${salto}
    <RIGHT>Ticket SIN VALOR OFICIAL<BR>
    ${salto + salto}`;

    let pie = "";

    pie = `${salto}
    <RIGHT>Recibi de: <BOLD>${clienteNombre} en: <BOLD>${formaPago}<BR>
    ${salto}
    <RIGHT><BIG>La Cantidad de: $${total.toLocaleString("de-DE")}`;

    let viaEmpresa = `
      ${salto}
      <RIGHT>Via Empresa<BR>
      ${salto}
      <DLINE>
      ${salto}
    `;

    let viaCliente = `
      ${salto}
      <RIGHT>Via Cliente<BR>
      ${salto}<CUT>
    `;

    var text = cabezera + pie + viaEmpresa + cabezera + pie + viaCliente;

    var textEncoded = encodeURI(text);
    window.location.href = "quickprinter://" + textEncoded;
  };
  
  handleMiniPrinter = () => {
    this.setState({ show: true });
  }

  confirmarRecibo = async () => {
    let imprimir = await this.miniPrinterRecibo();
    let confirmar = await this.confirmar();
  }
  
  handleClose = () => {
    this.setState({ show: false }, ()=> {
      this.confirmar();
    });
  };

  render() {
    const {
      fecha,
      documento,
      nDocumento,
      formaPago,
      total,
      idVendedor,
      vendedor,
      vendedores,
    } = this.state;

    const { cliente, clienteNombre, rut } = this.props.estado;

    return this.state.cargando ? (
      <div
        style={{
          width: "100%",
          height: "200px",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Spinner animation="grow" variant="primary" />
      </div>
    ) : (
      <div
        className={this.state.direccion === "home" ? "table-success" : null}
        style={{ padding: "15px" }}
      >
        <h1 style={{ padding: "25px" }}>Recibos</h1>
        <div className="container" style={{ marginTop: "15px" }}>
          <div>
            <Form>
              <Form.Group>
                <Form.Label
                  style={!fecha ? { color: "red", fontWeight: "bold" } : null}
                >
                  Fecha
                </Form.Label>
                <input
                  name="fecha"
                  className="form-control text-center"
                  type="date"
                  placeholder="Fecha"
                  onChange={(e) => this.handleChange(e)}
                  value={this.state.fecha}
                  disabled={!this.props.vendedorNombre.permisosVendedor}
                />
              </Form.Group>
              <Form.Group>
                <Form.Label
                  style={
                    !nDocumento ? { color: "red", fontWeight: "bold" } : null
                  }
                >
                  Nº
                </Form.Label>
                <input
                  name="nDocumento"
                  className="form-control"
                  type="number"
                  placeholder="Nº Documento"
                  onChange={(e) => this.handleChange(e)}
                  onBlur={this.comprobarCompleto}
                  value={this.state.nDocumento}
                />
              </Form.Group>
              <Form.Group>
                <Form.Label
                  style={
                    !formaPago ? { color: "red", fontWeight: "bold" } : null
                  }
                >
                  Forma de pago
                </Form.Label>
                {this.props.vendedorNombre.permisosVendedor ? (
                  <select
                    className="form-control"
                    onChange={(e) => this.handleChange(e)}
                    name="formaPago"
                    value={this.state.formaPago}
                    aria-label="Small"
                  >
                    <option value="">Seleccione forma de Pago...</option>
                    <option value="Efectivo">Efectivo</option>
                    <option value="Cheque">Cheque</option>
                    <option value="Transferencia">Transferencia</option>
                    <option value="otros">otros...</option>
                  </select>
                ) : (
                  <select
                    className="form-control"
                    onChange={(e) => this.handleChange(e)}
                    name="formaPago"
                    value={this.state.formaPago}
                    aria-label="Small"
                  >
                    <option value="">Seleccione forma de Pago...</option>
                    <option value="Efectivo">Efectivo</option>
                    <option value="Cheque">Cheque</option>
                  </select>
                )}
              </Form.Group>
              <Form.Group>
                <Form.Label
                  style={
                    !idVendedor ? { color: "red", fontWeight: "bold" } : null
                  }
                >
                  Vendedor
                </Form.Label>
                {this.props.vendedorNombre.permisosVendedor ? (
                  <select
                    className="form-control"
                    onChange={(e) => this.handleChange(e)}
                    name="idVendedor"
                    value={idVendedor}
                    aria-label="Small"
                  >
                    {Object.keys(vendedores).map((id) => {
                      return (
                        <option value={id}>{vendedores[id].vendedor}</option>
                      );
                    })}
                  </select>
                ) : (
                  <p>
                    <strong>{vendedor}</strong>
                  </p>
                )}
              </Form.Group>
              <Form.Group>
                <Form.Label
                  style={
                    total === 0 ? { color: "red", fontWeight: "bold" } : null
                  }
                >
                  Importe
                </Form.Label>
                <input
                  name="total"
                  className="form-control"
                  type="number"
                  placeholder="Importe"
                  onChange={(e) => this.handleChange(e)}
                  value={this.state.total}
                />
              </Form.Group>
            </Form>
          </div>
          <Row style={{ textAlign: "left" }}>
            <Col>
              <p style={!fecha ? { color: "red", fontWeight: "bold" } : null}>
                Fecha: <strong>{fecha}</strong>
              </p>
            </Col>
          </Row>
          <Row style={{ textAlign: "left" }}>
            <Col>
              <p style={!cliente ? { color: "red", fontWeight: "bold" } : null}>
                Cliente: <strong>{clienteNombre}</strong>
              </p>
            </Col>
          </Row>
          <Row style={{ textAlign: "left" }}>
            <Col>
              <p>
                Rut: <strong>{rut}</strong>
              </p>
            </Col>
          </Row>
          <Row style={{ textAlign: "left" }}>
            <Col>
              <p
                style={!documento ? { color: "red", fontWeight: "bold" } : null}
              >
                Documento: <strong>{documento}</strong>
              </p>
            </Col>
            <Col style={{ textAlign: "right" }}>
              <p
                style={
                  !nDocumento ? { color: "red", fontWeight: "bold" } : null
                }
              >
                Nº: <strong>{nDocumento}</strong>
              </p>
            </Col>
          </Row>
          <Row style={{ textAlign: "left" }}>
            <Col>
              <p
                style={
                  !idVendedor ? { color: "red", fontWeight: "bold" } : null
                }
              >
                Vendedor: <strong>{vendedor}</strong>
              </p>
            </Col>
          </Row>
          <Row style={{ textAlign: "left" }}>
            <Col>
              <p
                style={!formaPago ? { color: "red", fontWeight: "bold" } : null}
              >
                Forma de Pago: <strong>{formaPago}</strong>
              </p>
            </Col>
          </Row>
          <Row style={{ textAlign: "left" }}>
            <Col>
              <p
                style={
                  total === 0 ? { color: "red", fontWeight: "bold" } : null
                }
              >
                TOTAL: <strong>{total.toLocaleString("de-DE")}</strong>
              </p>
            </Col>
          </Row>
          <Row>
            <Col style={{ textAlign: "right" }}>
              {this.state.direccion === "home" ? (
                this.state.cargandoConfirmar ? (
                  <Button
                    variant="outline-success"
                    disabled
                    style={{ height: "42px", marginTop: "29px" }}
                  >
                    <Spinner
                      as="span"
                      animation="grow"
                      size="sm"
                      role="status"
                      aria-hidden="true"
                    />
                    Cargando...
                  </Button>
                ) : (
                  <Button variant="outline-success" onClick={this.handleMiniPrinter}>
                    Confirmar
                  </Button>
                )
              ) : null}
            </Col>
          </Row>
        </div>
        <Modal show={this.state.show} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>ATENCIÓN!!!</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            Quieres IMPRIMIR este documento?!
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleClose}>
              NO
            </Button>
            <Button
              variant="outline-success"
              onClick={() => {
                this.confirmarRecibo();
              }}
            >
              <FaPrint size="1em"/> IMPRIMIR
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

export default Recibos;
