import React, { Component } from "react";
import { Spinner, Tab, Nav } from "react-bootstrap";
import Arqueo from "./Arqueo";
import Guias from "./Guias";
import ListadoVentas from "./ListadoVentas";
import ListadoVentasProductos from "./ListadoVentasProductos";
import ListadoCompras from "./ListadoCompras";
import ListadoComprasProductos from "./ListadoComprasProductos";
import ListadoRemitos from "./ListadoRemitos";
import ListadoSalarios from "./ListadoSalarios";
import ListadoPedidos from "./ListadoPedidos";
import ListadoAprontePedidos from "./ListadoAprontePedidos";
import ListadoCreditos from "./ListadoCreditos";
import Stock from "./Stock";
import Cortes from "./Cortes";

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cargando: true,
      key: "ventas",
      clientes: [],
    };
  }

  render() {
    const { usuario, idUsuario, empresa } = this.props;
    return !usuario ? (
      <div
        style={{
          width: "100%",
          height: "200px",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Spinner animation="grow" variant="primary" />
      </div>
    ) : (
      <div className="container">
        <div style={{ padding: "15px" }}>
          <h1>Distri {empresa.empresa}</h1>
          <h3>Buena jornada {usuario.vendedor}!</h3>
        </div>
        <Tab.Container id="listado" defaultActiveKey="ventas">
          <Nav fill variant="tabs">
            {usuario.permisosVendedor ? (
              <Nav.Item>
                <Nav.Link eventKey="ventas">Ventas</Nav.Link>
              </Nav.Item>
            ) : null}
            {usuario.permisosVendedor ? (
              <Nav.Item>
                <Nav.Link eventKey="compras">Compras</Nav.Link>
              </Nav.Item>
            ) : null}
            {!usuario.permisosLimitados ? (
              <Nav.Item>
                <Nav.Link eventKey="arqueo">Arqueo</Nav.Link>
              </Nav.Item>
            ) : null}
            {!usuario.permisosLimitados ? (
              <Nav.Item>
                <Nav.Link eventKey="pedidos">Pedidos</Nav.Link>
              </Nav.Item>
            ) : null}
            <Nav.Item>
              <Nav.Link eventKey="pedidos-aprontar">Pedidos Aprontar</Nav.Link>
            </Nav.Item>
            {usuario.permisosVendedor ? (
              <Nav.Item>
                <Nav.Link eventKey="guias">Guías</Nav.Link>
              </Nav.Item>
            ) : null}
            {usuario.permisosVendedor ? (
              <Nav.Item>
                <Nav.Link eventKey="e-facturas">e-Facturas</Nav.Link>
              </Nav.Item>
            ) : null}
            {usuario.permisosVendedor ? (
              <Nav.Item>
                <Nav.Link eventKey="salarios">Salarios</Nav.Link>
              </Nav.Item>
            ) : null}
            {usuario.permisosVendedor ? (
              <Nav.Item>
                <Nav.Link eventKey="creditos">Créditos</Nav.Link>
              </Nav.Item>
            ) : null}
            {usuario.permisosVendedor ? (
              <Nav.Item>
                <Nav.Link eventKey="stock">Stock</Nav.Link>
              </Nav.Item>
            ) : null}
            {usuario.permisosVendedor ? (
              <Nav.Item>
                <Nav.Link eventKey="cortes">Cortes</Nav.Link>
              </Nav.Item>
            ) : null}
          </Nav>
          <Tab.Content>
            {usuario.permisosVendedor ? (
              <Tab.Pane eventKey="ventas">
                <div style={{ marginTop: "30px" }}>
                  <Tab.Container
                    id="listados-ventas"
                    defaultActiveKey="ventas-documentos"
                  >
                    <Nav fill variant="tabs">
                      <Nav.Item>
                        <Nav.Link eventKey="ventas-documentos">
                          Ventas Documentos
                        </Nav.Link>
                      </Nav.Item>
                      <Nav.Item>
                        <Nav.Link eventKey="ventas-productos">
                          Ventas Productos
                        </Nav.Link>
                      </Nav.Item>
                    </Nav>
                    <Tab.Content>
                      <Tab.Pane eventKey="ventas-documentos">
                        {usuario.permisosVendedor ? (
                          <ListadoVentas empresa={this.props.empresa} />
                        ) : (
                          "ListadoDocumentos"
                        )}
                      </Tab.Pane>
                      <Tab.Pane eventKey="ventas-productos">
                        {usuario.permisosVendedor ? (
                          <ListadoVentasProductos />
                        ) : (
                          "ListadoProductos"
                        )}
                      </Tab.Pane>
                    </Tab.Content>
                  </Tab.Container>
                </div>
              </Tab.Pane>
            ) : null}
            <Tab.Pane eventKey="compras">
              <div style={{ marginTop: "30px" }}>
                <Tab.Container
                  id="listados-compras"
                  defaultActiveKey="compras-documentos"
                >
                  <Nav fill variant="tabs">
                    <Nav.Item>
                      <Nav.Link eventKey="compras-documentos">
                        Compras Documentos
                      </Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                      <Nav.Link eventKey="compras-productos">
                        Compras Productos
                      </Nav.Link>
                    </Nav.Item>
                  </Nav>
                  <Tab.Content>
                    <Tab.Pane eventKey="compras-documentos">
                      {usuario.permisosVendedor ? (
                        <ListadoCompras />
                      ) : (
                        "ListadoComprasDocumentos"
                      )}
                    </Tab.Pane>
                    <Tab.Pane eventKey="compras-productos">
                      {usuario.permisosVendedor ? (
                        <ListadoComprasProductos />
                      ) : (
                        "ListadoComprasProductos"
                      )}
                    </Tab.Pane>
                  </Tab.Content>
                </Tab.Container>
              </div>
            </Tab.Pane>

            <Tab.Pane eventKey="arqueo">
              <Arqueo vendedor={usuario} idVendedor={idUsuario} />
            </Tab.Pane>
            <Tab.Pane eventKey="pedidos">
              <ListadoPedidos
                vendedor={usuario}
                idVendedor={idUsuario}
                empresa={this.props.empresa}
              />
            </Tab.Pane>
            <Tab.Pane eventKey="pedidos-aprontar">
              <ListadoAprontePedidos
                vendedor={usuario}
                idVendedor={idUsuario}
              />
            </Tab.Pane>
            {usuario.permisosVendedor ? (
              <Tab.Pane eventKey="guias">
                <Guias vendedor={usuario} idVendedor={idUsuario} />
              </Tab.Pane>
            ) : null}
            {usuario.permisosVendedor ? (
              <Tab.Pane eventKey="e-facturas">
                <ListadoRemitos vendedor={usuario} idVendedor={idUsuario} />
              </Tab.Pane>
            ) : null}
            {usuario.permisosVendedor ? (
              <Tab.Pane eventKey="salarios">
                <ListadoSalarios vendedor={usuario} idVendedor={idUsuario} />
              </Tab.Pane>
            ) : null}
            {usuario.permisosVendedor ? (
              <Tab.Pane eventKey="creditos">
                <ListadoCreditos vendedor={usuario} idVendedor={idUsuario} />
              </Tab.Pane>
            ) : null}
            {usuario.permisosVendedor ? (
              <Tab.Pane eventKey="stock">
                <Stock vendedor={usuario} idVendedor={idUsuario} />
              </Tab.Pane>
            ) : null}
            {usuario.permisosVendedor ? (
              <Tab.Pane eventKey="cortes">
                <Cortes vendedor={usuario} idVendedor={idUsuario} />
              </Tab.Pane>
            ) : null}
          </Tab.Content>
        </Tab.Container>
      </div>
    );
  }
}

export default Home;
