import React, { Component } from "react";
import {
  Button,
  Form,
  Container,
  Row,
  Col,
  ListGroup,
  Spinner,
} from "react-bootstrap";
import { db } from "../firebaseConfig";

class ListadoSalarios extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cargando: false,
      salarios: [],
      vendedores: [],
      salariosVendedor: [],
      desde: "",
      hasta: "",
      totalSalarios: 0,
      banderaVendedor: "",
      cargandoSalariosVendedor: false,
    };
  }

  traerSalarios = () => {
    const { desde, hasta } = this.state;
    let totalSalarios = 0;
    if (desde && hasta) {
      this.setState({ cargando: true }, async () => {
        let snapSalarios = await db
          .collection("salarios")
          .where("fecha", ">=", desde)
          .where("fecha", "<=", hasta)
          .get();
        let salarios = [];
        snapSalarios.forEach((doc) => {
          salarios[doc.id] = doc.data();
          totalSalarios = totalSalarios + parseFloat(salarios[doc.id].importe);
        });

        let snapVendedores = await db
          .collection("vendedores")
          .orderBy("vendedor")
          .get();
        let vendedores = [];
        snapVendedores.forEach((doc) => {
          vendedores[doc.id] = doc.data();
          vendedores[doc.id].totalSalarioVendedor = 0;
        });

        Object.keys(vendedores).forEach((id) => {
          Object.keys(salarios).forEach((key) => {
            if (id === salarios[key].vendedor) {
              vendedores[id].totalSalarioVendedor =
                vendedores[id].totalSalarioVendedor +
                parseFloat(salarios[key].importe);
            }
          });
        });

        this.setState({
          vendedores,
          salarios,
          totalSalarios,
          cargando: false,
        });
      });
    }
  };

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  traerSalariosVendedor = (id) => {
    let { salarios, banderaVendedor } = this.state;
    if (banderaVendedor === id) {
      this.setState({ banderaVendedor: "" });
    } else {
      this.setState(
        { banderaVendedor: id, cargandoSalariosVendedor: true },
        () => {
          let salariosVendedor = [];
          Object.keys(salarios).forEach((key) => {
            if (salarios[key].vendedor === id) {
              salariosVendedor[key] = { ...salarios[key] };
            }
          });
          this.setState({ salariosVendedor, cargandoSalariosVendedor: false });
        }
      );
    }
  };

  render() {
    const { vendedores, salariosVendedor, totalSalarios } = this.state;
    return (
      <div>
        <Container>
          <Row>
            <Col>
              <h1 style={{ padding: "15px" }}>Listado Salarios</h1>
            </Col>
          </Row>
          <Row>
            <Col>
              <Form.Group>
                <Form.Label>Desde</Form.Label>
                <input
                  name="desde"
                  className="form-control text-center"
                  type="date"
                  onChange={(e) => this.handleChange(e)}
                  value={this.state.desde}
                />
              </Form.Group>
            </Col>
            <Col>
              <Form.Group>
                <Form.Label>Hasta</Form.Label>
                <input
                  name="hasta"
                  className="form-control text-center"
                  type="date"
                  onChange={(e) => this.handleChange(e)}
                  value={this.state.hasta}
                />
              </Form.Group>
            </Col>
          </Row>
          <Row>
            <Col>
              <Button
                variant="success"
                onClick={() => {
                  this.traerSalarios();
                }}
              >
                Buscar
              </Button>
            </Col>
          </Row>
          {this.state.cargando ? (
            <Row>
              <div
                style={{
                  width: "100%",
                  height: "200px",
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <Spinner animation="grow" variant="primary" />
              </div>
            </Row>
          ) : (
            <div>
              <Row style={{ marginTop: "20px", marginBottom: "20px" }}>
                <Col xs={7}>
                  <strong>Vendedores</strong>
                </Col>
                <Col xs={5}>
                  <strong>Total Salarios</strong>
                </Col>
              </Row>

              <ListGroup>
                {Object.keys(vendedores).map((id) => {
                  return (
                    <ListGroup.Item
                      action
                      variant="success"
                      onClick={() => this.traerSalariosVendedor(id)}
                    >
                      <Row>
                        <Col xs={7} style={{ textAlign: "left" }}>
                          {vendedores[id].vendedor}
                        </Col>
                        <Col xs={5} style={{ textAlign: "right" }}>
                          <strong>
                            {`$${vendedores[
                              id
                            ].totalSalarioVendedor.toLocaleString("de-DE")}`}
                          </strong>
                        </Col>
                      </Row>
                      {this.state.banderaVendedor === id ? (
                        this.state.cargandoSalariosVendedor ? (
                          <div>
                            <Spinner animation="border" variant="warning" />
                          </div>
                        ) : (
                          <ListGroup variant="flush">
                            {Object.keys(salariosVendedor).map((key) => {
                              return (
                                <ListGroup.Item variant="success">
                                  <Row>
                                    <Col xs={8}>
                                      {salariosVendedor[key].fecha}
                                    </Col>
                                    <Col xs={4}>
                                      {`$${salariosVendedor[
                                        key
                                      ].importe.toLocaleString("de-DE")}`}
                                    </Col>
                                  </Row>
                                </ListGroup.Item>
                              );
                            })}
                          </ListGroup>
                        )
                      ) : null}
                    </ListGroup.Item>
                  );
                })}
                <ListGroup.Item variant="success">
                  <Row>
                    <Col style={{ textAlign: "right" }}>
                      <strong>
                        Total Salarios: ${" "}
                        {totalSalarios.toLocaleString("de-DE")}
                      </strong>
                    </Col>
                  </Row>
                </ListGroup.Item>
              </ListGroup>
            </div>
          )}
        </Container>
      </div>
    );
  }
}

export default ListadoSalarios;
