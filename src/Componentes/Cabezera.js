import React, { Component } from "react";
import { fire } from "../firebaseConfig";
import { Tabs, Tab } from "react-bootstrap";
import Home from "./Home";
import VisitaCliente from "./VisitaCliente";
import Salidas from "./Salidas";
import Entradas from "./Entradas";
import Clientes from "./Clientes";
import Productos from "./Productos";
import VisitaProveedor from "./VisitaProveedor";
import Salarios from "./Salarios";
import { FiLogOut } from "react-icons/fi";

class Cabezera extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cargando: true,
      key: "home",
    };
  }

  salir = () => {
    fire
      .auth()
      .signOut()
      .then(function () {
        console.log("estamos fuera");
      })
      .catch(function (error) {
        console.log(error);
      });
    this.props.desloguear();
  };

  handleSelect = (eventKey) => {
    this.setState({ tab: eventKey });
  };

  render() {
    let { usuario, idUsuario, empresa } = this.props;
    return (
      <Tabs
        defaultActiveKey="home"
        id="cabezera"
        style={{ justifyContent: "space-around" }}
      >
        <Tab eventKey="home" title="Home">
          <Home usuario={usuario} idUsuario={idUsuario} empresa={empresa} />
        </Tab>
        <Tab eventKey="salidas" title="Salidas">
          <Salidas usuario={usuario} idUsuario={idUsuario} empresa={empresa} />
        </Tab>
        {!usuario.permisosLimitados ? (
          <Tab eventKey="entradas" title="Entradas">
            <Entradas
              usuario={usuario}
              idUsuario={idUsuario}
              empresa={empresa}
            />
          </Tab>
        ) : null}
        {!usuario.permisosLimitados ? (
          <Tab eventKey="productos" title="Productos">
            <Productos usuario={usuario} idUsuario={idUsuario} />
          </Tab>
        ) : null}
        {!usuario.permisosLimitados ? (
          <Tab eventKey="clientes" title="Clientes">
            <Clientes usuario={usuario} idUsuario={idUsuario} />
          </Tab>
        ) : null}
        {usuario.permisosVendedor ? (
          <Tab eventKey="visita-proveedor" title="Visita Proveedor">
            <VisitaProveedor
              usuario={usuario}
              idUsuario={idUsuario}
              empresa={empresa}
            />
          </Tab>
        ) : null}
        {!usuario.permisosLimitados ? (
          <Tab
            eventKey="visita-cliente"
            title={<strong>Visita Cliente</strong>}
          >
            <VisitaCliente
              usuario={usuario}
              idUsuario={idUsuario}
              empresa={empresa}
            />
          </Tab>
        ) : null}
        {usuario.permisosVendedor ? (
          <Tab eventKey="salarios" title="Salarios">
            <Salarios
              usuario={usuario}
              idUsuario={idUsuario}
              empresa={empresa}
            />
          </Tab>
        ) : null}
        <Tab
          eventKey="salir"
          title={<FiLogOut color="red" size="2em" onClick={this.salir} />}
        />
      </Tabs>
    );
  }
}

export default Cabezera;
