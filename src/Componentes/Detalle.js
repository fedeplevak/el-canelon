import React, { Component } from "react";
import { db } from "../firebaseConfig";
import { Table, Spinner } from "react-bootstrap";
import jsPDF from "jspdf";
import { FaPrint, FaFilePdf } from "react-icons/fa";

class Detalle extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cargandoProductos: true,
      productos: [],
    };
  }

  componentDidMount() {
    this.traerProductos();
  }

  traerProductos = () => {
    const key = this.props.idFactura;
    console.log(key);
    db.collection("ventasProductos")
      .where("idCabezera", "==", key)
      .orderBy("orden")
      .onSnapshot((snapShot) => {
        let productos = [];
        snapShot.forEach((doc) => {
          productos[doc.id] = doc.data();
        });
        this.setState({ productos, cargandoProductos: false });
        console.log(productos);
      });
  };

  miniPrinter = () => {
    let {
      clienteNombre,
      rut,
      nCarpeta,
      fecha,
      documento,
      nDocumento,
      formaPago,
      vendedor,
      total,
      iva,
      subtotal,
    } = this.props.documentos;
    const { empresa, telefono } = this.props.empresa;

    if (documento === "Recibo") {
      total = total * -1;
      let salto = ` <BR>`;

      let cabezera = "";

      cabezera = `<BIG><CENTER>Distri ${empresa}<BR>
    <RIGHT><SMALL>Tel: ${telefono}<BR>
    ${salto}
    <LEFT>Fecha: ${fecha}<BR>
    <LEFT>Documento: ${documento}<BR>
    <RIGHT>Nro: ${nDocumento}<BR>
    ${salto}
    <LEFT>Cliente: <BOLD>${clienteNombre}<BR>
    <LEFT>Rut: <BOLD>${rut}<BR>
    ${salto}
    ${salto}
    <LEFT>Vendedor: ${vendedor}
    ${salto}
    ${salto}
    <RIGHT>Ticket SIN VALOR OFICIAL<BR>
    ${salto + salto}`;

      let pie = "";

      pie = `${salto}
    <RIGHT>Recibi de: <BOLD>${clienteNombre} en: <BOLD>${formaPago}<BR>
    ${salto}
    <RIGHT><BIG>La Cantidad de: $${total.toLocaleString("de-DE")}`;

      let viaEmpresa = `
      ${salto}
      <RIGHT>Via Empresa<BR>
      ${salto}
      <DLINE>
      ${salto}
    `;

      let viaCliente = `
      ${salto}
      <RIGHT>Via Cliente<BR>
      ${salto}<CUT>
    `;

      var text = cabezera + pie + viaEmpresa + cabezera + pie + viaCliente;
    } else {
      let productosFinal = this.state.productos;

      let salto = ` <BR>`;

      let cabezera = "";

      documento === "Boleta Contado" || documento === "Boleta Crédito"
        ? (cabezera = `<BIG><CENTER>Distri ${empresa}<BR>
    <RIGHT><SMALL>Tel: ${telefono}<BR>
    ${salto}
    <LEFT>Fecha: ${fecha}<BR>
    <LEFT>Documento: ${documento}<BR>
    <RIGHT>Nro: ${nDocumento}<BR>
    ${salto}
    <LEFT>Cliente: <BOLD>${clienteNombre}<BR>
    ${salto}
    ${salto}
    <LEFT>Vendedor: ${vendedor}
    ${salto}
    ${salto}
    <RIGHT>Ticket SIN VALOR OFICIAL<BR>
    ${salto + salto}`)
        : (cabezera = `<BIG><CENTER>Distri ${empresa}<BR>
    <RIGHT><SMALL>Tel: ${telefono}<BR>
    ${salto}
    <LEFT>Fecha: ${fecha}<BR>
    <LEFT>Documento: ${documento}<BR>
    <RIGHT>Nro: ${nDocumento}<BR>
    ${salto}
    <LEFT>Cliente: <BOLD>${clienteNombre}<BR>
    <LEFT>Rut: <BOLD>${rut}<BR>
    <LEFT>Nro. Carperta: <BOLD>${nCarpeta}<BR>
    ${salto}
    ${salto}
    <LEFT>Vendedor: ${vendedor}
    ${salto}
    ${salto}
    <RIGHT>Ticket SIN VALOR OFICIAL<BR>
    ${salto + salto}`);

      let lineas =
        "<UNDERLINE><BOLD>CANT;;PRODUCTO;;PRECIO;;IMPORTE<BR>" +
        Object.keys(productosFinal).map((id) => {
          return `${productosFinal[id].cantidad};;${
            productosFinal[id].producto
          };;${productosFinal[id].precio.toLocaleString(
            "de-DE"
          )};;${productosFinal[id].importe.toLocaleString("de-DE")}<BR>`;
        }) +
        salto;

      let pie = "";

      documento === "Boleta Contado" || documento === "Boleta Crédito"
        ? (pie = `${salto}
    <RIGHT><BIG>TOTAL: $${total.toLocaleString("de-DE")}<CUT>`)
        : (pie = `${salto}
    <RIGHT>Subtotal: $${subtotal.toLocaleString("de-DE")}<BR>
    ${salto}
    <RIGHT>IVA: $${iva.toLocaleString("de-DE")}<BR>
    ${salto}
    <RIGHT><BIG>TOTAL: $${total.toLocaleString("de-DE")}<CUT>`);

      let viaEmpresa = `
      ${salto}
      <RIGHT>Via Empresa<BR>
      ${salto}
      <DLINE>
      ${salto}
    `;

      let viaCliente = `
      ${salto}
      <RIGHT>Via Cliente<BR>
      ${salto}
    `;

      var text =
        cabezera +
        lineas +
        pie +
        viaEmpresa +
        cabezera +
        lineas +
        pie +
        viaCliente;
    }

    var textEncoded = encodeURI(text);
    window.location.href = "quickprinter://" + textEncoded;
  };

  imprimir = () => {
    let doc = new jsPDF("p", "mm", [58, 180]);
    let {
      clienteNombre,
      rut,
      fecha,
      documento,
      nDocumento,
      vendedor,
      total,
      iva,
      subtotal,
    } = this.props.documentos;
    const { empresa, telefono } = this.props.empresa;

    if (documento === "Recibo") {
      total = total * -1;
      doc.setFontSize(5);
      doc.text(6, 2, `Distri ${empresa}`);
      doc.setFontSize(2);
      doc.text(10, 3.5, `Tel: ${telefono}`);

      doc.setFontSize(2.5);
      doc.text(0.3, 5, `Fecha: ${fecha}`);
      doc.text(0.3, 6, `Documento: ${documento}`);
      doc.text(15, 6, `Nº: ${nDocumento}`);
      doc.text(0.3, 8, `Cliente: ${clienteNombre}`);
      doc.text(0.3, 9, `Rut: ${rut}`);
      doc.text(0.3, 10, `Vendedor: ${vendedor}`);

      doc.text(8, 13, `Ticket SIN VALOR OFICIAL`);

      doc.setFontSize(2.5);
      doc.text(2, 16, `Recibí de: `);
      doc.text(1, 17, `${clienteNombre}, `);
      doc.text(1, 18, `la cantidad de:`);
      doc.setFontSize(3);
      doc.text(7, 18, `$${total.toLocaleString("de-DE")}`);

      doc.save(`${clienteNombre}-${nDocumento}.pdf`);
    } else {
      let productosFinal = this.state.productos;

      doc.setFontSize(5);
      doc.text(6, 2, `Distri ${empresa}`);
      doc.setFontSize(2);
      doc.text(10, 3.5, `Tel: ${telefono}`);

      doc.setFontSize(2.5);
      doc.text(0.3, 5, `Fecha: ${fecha}`);
      doc.text(0.3, 6, `Documento: ${documento}`);
      doc.text(15, 6, `Nº: ${nDocumento}`);
      doc.text(0.3, 8, `Cliente: ${clienteNombre}`);
      if (documento === "Factura Contado" || documento === "Factura Crédito") {
        doc.text(0.3, 9, `Rut: ${rut}`);
      }
      doc.text(0.3, 10, `Vendedor: ${vendedor}`);

      doc.text(8, 13, `Ticket SIN VALOR OFICIAL`);

      let co = 16;

      doc.setFontSize(2.3);
      doc.text(0.1, co, `Cant`);
      doc.text(6, co, `Producto`);
      doc.text(14.5, co, `Precio`);
      doc.text(17.4, co, `Importe`);
      co = co + 0.2;
      doc.setLineWidth(0.08);
      doc.line(0.3, co, 20, co);
      doc.setFontSize(2.3);
      Object.keys(productosFinal).forEach((id) => {
        co = co + 1.3;
        doc.text(0.25, co, `${productosFinal[id].cantidad}`);
        doc.text(2.9, co, `${productosFinal[id].producto}`);
        doc.text(
          14.9,
          co,
          `${productosFinal[id].precio.toLocaleString("de-DE")}`
        );
        doc.text(
          17.8,
          co,
          `${productosFinal[id].importe.toLocaleString("de-DE")}`
        );
      });

      doc.setFontSize(2.5);
      co = co + 3;
      if (documento === "Factura Contado" || documento === "Factura Crédito") {
        doc.text(11.5, co, `Subtotal: `);
        doc.text(15.5, co, subtotal.toLocaleString("de-DE"));
        co = co + 1.5;
        doc.text(13, co, `IVA: `);
        doc.text(15.5, co, iva.toLocaleString("de-DE"));
      }

      doc.setFontSize(3);
      co = co + 1.5;
      doc.text(11.5, co, `TOTAL: `);
      doc.text(15.5, co, total.toLocaleString("de-DE"));

      doc.save(`${clienteNombre}-${nDocumento}.pdf`);
    }
  };

  render() {
    const { productos } = this.state;
    const { documento } = this.props.documentos;

    return this.state.cargandoProductos ? (
      <tr>
        <td colSpan="7">
          <div
            style={{
              width: "100%",
              height: "200px",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Spinner animation="grow" variant="primary" />
          </div>
        </td>
      </tr>
    ) : documento === "Recibo" ? (
      <tr
        style={{ width: "100%", overflow: "scroll" }}
        className={this.props.claseDoc}
      >
        <td colSpan="7">
          <tr>
            <th>
              <FaPrint
                size="1.5em"
                onClick={() => {
                  this.miniPrinter();
                }}
              />
            </th>
            <th colSpan="5">
              Click en el botón para volver a imprimir el Recibo
            </th>
            <th>
              <FaFilePdf
                size="1.5em"
                onClick={() => {
                  this.imprimir();
                }}
              />
            </th>
          </tr>
        </td>
      </tr>
    ) : (
      <tr
        style={{ width: "100%", overflow: "scroll" }}
        className={this.props.claseDoc}
      >
        <td colSpan="8">
          <Table striped bordered condensed hover responsive>
            <thead>
              <tr>
                <th>
                  <FaPrint
                    size="1.5em"
                    onClick={() => {
                      this.miniPrinter();
                    }}
                  />
                </th>
                <th>Cantidad</th>
                <th>Producto</th>
                <th>Precio</th>
                <th>Importe</th>
                <th>
                  <FaFilePdf
                    size="1.5em"
                    onClick={() => {
                      this.imprimir();
                    }}
                  />
                </th>
              </tr>
            </thead>
            <tbody>
              {Object.keys(productos).map((id) => {
                let importe = productos[id].importe;
                importe ? (importe = parseFloat(importe)) : (importe = 0);
                return (
                  <tr key={id}>
                    <td />
                    <td>{productos[id].cantidad}</td>
                    <td>{productos[id].producto}</td>
                    <td>{productos[id].precio}</td>
                    <td>
                      <strong>
                        {productos[id].importe.toLocaleString("de-DE")}
                      </strong>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </Table>
        </td>
      </tr>
    );
  }
}

export default Detalle;
