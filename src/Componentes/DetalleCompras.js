import React, { Component } from "react";
import { db } from "../firebaseConfig";
import { Table, Button, Spinner } from "react-bootstrap";

class DetalleCompras extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cargandoProductos: true,
      productos: [],
    };
  }

  componentDidMount() {
    this.traerProductos();
  }

  traerProductos = () => {
    const key = this.props.idFactura;
    console.log(key);
    db.collection("comprasProductos")
      .where("idCabezera", "==", key)
      .orderBy("orden")
      .onSnapshot((snapShot) => {
        let productos = [];
        snapShot.forEach((doc) => {
          productos[doc.id] = doc.data();
        });
        this.setState({ productos, cargandoProductos: false });
        console.log(productos);
      });
  };

  render() {
    const { productos } = this.state;
    const { documento } = this.props.documentos;

    return this.state.cargandoProductos ? (
      <tr>
        <td colSpan="7">
          <div
            style={{
              width: "100%",
              height: "200px",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Spinner animation="grow" variant="primary" />
          </div>
        </td>
      </tr>
    ) : documento === "Recibo" ? (
      <tr
        style={{ width: "100%", overflow: "scroll" }}
        className={this.props.claseDoc}
      >
        <td colSpan="7">
          <tr>
            <th>
              <Button
                variant="info"
                onClick={() => {
                  this.imprimir();
                }}
              >
                Imprimir
              </Button>
            </th>
            <th colSpan="6">
              Click en el botón para volver a imprimir el Recibo
            </th>
          </tr>
        </td>
      </tr>
    ) : (
      <tr
        style={{ width: "100%", overflow: "scroll" }}
        className={this.props.claseDoc}
      >
        <td colSpan="7">
          <Table striped bordered condensed hover responsive>
            <thead>
              <tr>
                <th>
                  <Button
                    variant="info"
                    onClick={() => {
                      this.imprimir();
                    }}
                  >
                    Imprimir
                  </Button>
                </th>
                <th>Cantidad</th>
                <th>Producto</th>
                <th>Precio</th>
                <th>Importe</th>
              </tr>
            </thead>
            <tbody>
              {Object.keys(productos).map((id) => {
                let importe = productos[id].importe;
                importe ? (importe = parseFloat(importe)) : (importe = 0);
                return (
                  <tr key={id}>
                    <td />
                    <td>{productos[id].cantidad}</td>
                    <td>{productos[id].producto}</td>
                    <td>{productos[id].precio}</td>
                    <td>
                      <strong>
                        {productos[id].importe.toLocaleString("de-DE")}
                      </strong>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </Table>
        </td>
      </tr>
    );
  }
}

export default DetalleCompras;
