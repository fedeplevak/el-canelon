import React, { Component } from "react";
import { db } from "../firebaseConfig";
import {
  Form,
  Col,
  Button,
  Tab,
  Nav,
  ToggleButton,
  ToggleButtonGroup,
  Spinner,
} from "react-bootstrap";
import Facturas from "./Facturas";
import Recibos from "./Recibos";
import Movimientos from "./Movimientos";
import EstadoCuenta from "./EstadoCuenta";
import Pedidos from "./Pedidos";

class VisitaCliente extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cargandoClientes: true,
      cargandoMovimientos: false,
      cargandoEstadoCuenta: false,
      documentos: [],
      deudaInicial: 0,
      deudaActual: "",
      clientesVer: "activos",
      clientes: [],
      cliente: "",
      clienteNombre: "",
      rut: "",
      totalAdeudado: "",
      clienteDatos: {},
      movimientos: [],
      cargandoProductos: true,
      productosVer: "activos",
      productos: [],
      buscador: "",
      buscadorClientes: "",
      productosMostrar: [],
      clientesMostrar: [],
    };
  }

  componentDidMount() {
    this.traerClientes();
  }

  handleActivoClientes = (val) => {
    this.setState({ clientesVer: val }, () => {
      this.traerClientes();
    });
  };

  traerClientes = () => {
    let zona = this.props.usuario.reparto;
    if (this.state.clientesVer === "activos") {
      this.setState({ cargandoClientes: true }, () => {
        db.collection("clientes")
          .where("activo", "==", true)
          .where("zona", "==", zona)
          .orderBy("nombre")
          .onSnapshot((snapShot) => {
            const clientes = [];
            snapShot.forEach((doc) => {
              clientes[doc.id] = doc.data();
            });
            this.setState({ clientes });
            this.checkBuscadorClientes(clientes);
          });
      });
    }
    if (this.state.clientesVer === "todos") {
      this.setState({ cargandoClientes: true }, () => {
        db.collection("clientes")
          .orderBy("zona")
          .orderBy("nombre")
          .onSnapshot((snapShot) => {
            const clientes = [];
            snapShot.forEach((doc) => {
              clientes[doc.id] = doc.data();
            });
            this.setState({ clientes });
            this.checkBuscadorClientes(clientes);
          });
      });
    }
  };

  actualizarDatosCliente = async () => {
    let { cliente } = this.state;
    if (cliente) {
      let snapCliente = await db.collection("clientes").doc(cliente).get();
      let datos = snapCliente.data();
      this.setState({ clienteDatos: datos });
    }
  };

  handleChange = (e) => {
    if (e.target.name === "cliente") {
      if (e.target.value) {
        this.movimientos(e.target.value);
        this.estadoCuenta(e.target.value);
        let listaClientes = this.state.clientes;
        let clienteSeleccionado = e.target.value;
        let clienteNombre = e.target.selectedOptions[0].textContent;
        let totalAdeudado = "";
        let rut = "";
        let clienteDatos = {};

        Object.keys(listaClientes).forEach((id) => {
          if (id === clienteSeleccionado) {
            clienteDatos = listaClientes[id];
            rut = listaClientes[id].rut;
            totalAdeudado = listaClientes[id].totalAdeudado;
            if (totalAdeudado) {
              totalAdeudado = parseFloat(totalAdeudado);
            } else {
              totalAdeudado = 0;
            }
            this.setState(
              {
                cliente: clienteSeleccionado,
                clienteNombre,
                rut,
                totalAdeudado,
                clienteDatos,
              },
              () => {
                this.traerProductos();
              }
            );
          }
        });
      }
    } else {
      this.setState(
        {
          [e.target.name]: e.target.value,
        },
        () => this.comprobarCompleto()
      );
    }
  };

  movimientos = (cliente) => {
    this.setState({ cargandoMovimientos: true }, () => {
      db.collection("ventasCabezera")
        .where("cliente", "==", cliente)
        .orderBy("fecha", "desc")
        .limit(10)
        .onSnapshot((snap) => {
          let movimientos = [];
          snap.forEach((doc) => {
            movimientos[doc.id] = doc.data();
          });
          this.setState({ movimientos, cargandoMovimientos: false });
        });
    });
  };

  estadoCuenta = (cliente) => {
    this.setState({ cargandoEstadoCuenta: true }, async () => {
      let snapCliente = await db.collection("clientes").doc(cliente).get();
      let deudaActual = snapCliente.data().deudaActual;
      if (!deudaActual) {
        deudaActual = 0;
      }
      let snap = await db
        .collection("ventasCabezera")
        .where("cliente", "==", cliente)
        .where("estadoCuenta", "==", true)
        .orderBy("fecha", "desc")
        .orderBy("documento", "desc")
        .orderBy("nDocumento", "desc")
        .limit(10)
        .get();
      let documentos = [];
      snap.forEach((doc) => {
        documentos.splice(0, 0, doc.data());
      });

      let deudaInicial = await this.deudaInicial(documentos, deudaActual);

      let docs = await this.saldo(documentos, deudaInicial);

      this.setState({
        deudaInicial,
        deudaActual,
        documentos: docs,
        cargandoEstadoCuenta: false,
      });
    });
  };

  saldo = (documentos, deudaInicial) => {
    let saldo = deudaInicial;
    Object.keys(documentos).forEach((id) => {
      let total = parseFloat(documentos[id].total);
      saldo = saldo + total;
      documentos[id].saldo = saldo;
    });
    return documentos;
  };

  deudaInicial = (documentos, deudaActual) => {
    let deudaInicial = deudaActual;
    Object.keys(documentos).forEach((id) => {
      let total = parseFloat(documentos[id].total);
      deudaInicial = deudaInicial - total;
    });
    return deudaInicial;
  };

  handleActivoProductos = (val) => {
    console.log(val);
    this.setState({ productosVer: val }, () => {
      this.traerProductos();
    });
  };

  traerProductosCliente = async () => {
    let { cliente } = this.state;
    let snapProductos = await db
      .collection("clientes")
      .doc(cliente)
      .collection("listaPrecios")
      .get();
    let productos = [];
    snapProductos.forEach((doc) => {
      productos[doc.id] = doc.data();
    });
    return productos;
  };

  traerProductos = () => {
    let { cliente } = this.state;
    if (this.state.productosVer === "activos") {
      this.setState({ cargandoProductos: true }, async () => {
        let snapProductosActivos = await db
          .collection("productos")
          .where("activo", "==", true)
          .orderBy("orden")
          .orderBy("producto")
          .get();

        let pro = [];
        snapProductosActivos.forEach((doc) => {
          pro[doc.id] = doc.data();
          pro[doc.id].precio = 0;
        });
        if (cliente) {
          let products = await this.traerProductosCliente();
          Object.keys(pro).forEach((id) => {
            Object.keys(products).forEach((key) => {
              if (id === key) {
                pro[id].idProducto = products[key].idProducto;
                pro[id].precio = products[key].precio;
              }
            });
          });
        }
        this.checkBuscador(pro);
        this.setState({ productos: pro });
      });
    }

    if (this.state.productosVer === "todos") {
      this.setState({ cargandoProductos: true }, async () => {
        let snapProductosTodos = await db
          .collection("productos")
          .orderBy("orden")
          .orderBy("producto")
          .get();
        let pro = [];
        snapProductosTodos.forEach((doc) => {
          pro[doc.id] = doc.data();
          pro[doc.id].precio = 0;
        });

        if (cliente) {
          let products = await this.traerProductosCliente();
          Object.keys(pro).forEach((id) => {
            Object.keys(products).forEach((key) => {
              if (id === key) {
                pro[id].idProducto = products[key].idProducto;
                pro[id].precio = products[key].precio;
              }
            });
          });
        }

        this.setState({ productos: pro, cargandoProductos: false });
      });
    }
  };

  movimientos = (cliente) => {
    this.setState({ cargandoMovimientos: true }, () => {
      db.collection("ventasCabezera")
        .where("cliente", "==", cliente)
        .orderBy("fecha", "desc")
        .orderBy("documento")
        .orderBy("nDocumento")
        .limit(10)
        .onSnapshot((snap) => {
          let movimientos = [];
          snap.forEach((doc) => {
            movimientos[doc.id] = doc.data();
          });
          this.setState({ movimientos, cargandoMovimientos: false });
        });
    });
  };

  checkBuscador = (productos) => {
    let { buscador } = this.state;
    let productosMostrar = [];
    buscador = buscador.toLocaleLowerCase();

    Object.keys(productos).forEach((id) => {
      if (
        productos[id].producto.toLowerCase().indexOf(buscador) >= 0 ||
        productos[id].codigo.toLowerCase().indexOf(buscador) >= 0 ||
        buscador === ""
      ) {
        productosMostrar[id] = { ...productos[id] };
      }
    });
    this.setState({ productosMostrar, cargandoProductos: false });
  };

  checkBuscadorClientes = (clientes) => {
    let { buscadorClientes } = this.state;
    let clientesMostrar = [];
    buscadorClientes = buscadorClientes.toLocaleLowerCase();

    Object.keys(clientes).forEach((id) => {
      if (
        clientes[id].nombre.toLowerCase().indexOf(buscadorClientes) >= 0 ||
        clientes[id].razonSocial.toLowerCase().indexOf(buscadorClientes) >= 0 ||
        buscadorClientes === ""
      ) {
        clientesMostrar[id] = { ...clientes[id] };
      }
    });
    this.setState({ clientesMostrar, cargandoClientes: false });
  };

  handleChangeBuscador = (e) => {
    let buscador = e.target.value;
    let { productos } = this.state;

    this.setState({ buscador }, () => {
      this.checkBuscador(productos);
    });
  };

  handleChangeBuscadorClientes = (e) => {
    let buscadorClientes = e.target.value;
    let { clientes } = this.state;

    this.setState({ buscadorClientes }, () => {
      this.checkBuscadorClientes(clientes);
    });
  };

  render() {
    const { cargandoClientes, clientesMostrar, cliente } = this.state;

    return this.state.cargando ? (
      <div
        style={{
          width: "100%",
          height: "200px",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Spinner animation="grow" variant="primary" />
      </div>
    ) : (
      <div style={{ maxWidth: "100vw" }}>
        <h1 style={{ padding: "25px" }}>Visita Cliente</h1>
        <div>
          <Form style={{ display: "flex", justifyContent: "space-around" }}>
            <Form.Group as={Col} controlId="cliente">
              <Form.Label
                style={!cliente ? { color: "red", fontWeight: "bold" } : null}
              >
                Cliente
              </Form.Label>
              <input
                className="form-control"
                value={this.state.buscadorMostrar}
                onChange={this.handleChangeBuscadorClientes}
                type="text"
                name="buscadorClientes"
                placeholder="Buscador..."
              />
              <select
                style={{ marginTop: "10px" }}
                className="form-control"
                onChange={(e) => this.handleChange(e)}
                name="cliente"
                value={cliente}
                disable={cargandoClientes}
              >
                <option value="">Seleccione Cliente...</option>
                {Object.keys(clientesMostrar).map((id) => {
                  return (
                    <option value={id}>
                      {clientesMostrar[id].nombre} (
                      {clientesMostrar[id].razonSocial})
                    </option>
                  );
                })}
              </select>
            </Form.Group>
            {this.state.cargandoClientes ? (
              <Button
                variant="outline-primary"
                disabled
                style={{
                  height: "42px",
                  marginTop: "29px",
                  paddingRight: "15px",
                }}
              >
                <Spinner
                  as="span"
                  animation="grow"
                  size="sm"
                  role="status"
                  aria-hidden="true"
                />
                Cargando...
              </Button>
            ) : (
              <ToggleButtonGroup
                style={{
                  height: "42px",
                  marginTop: "29px",
                  paddingRight: "15px",
                }}
                type="radio"
                name="clientesVer"
                value={this.state.clientesVer}
                onChange={this.handleActivoClientes}
              >
                <ToggleButton
                  variant="outline-success"
                  name="radio"
                  value="activos"
                >
                  Activos
                </ToggleButton>
                <ToggleButton
                  variant="outline-danger"
                  name="radio"
                  value="todos"
                >
                  Todos
                </ToggleButton>
              </ToggleButtonGroup>
            )}
          </Form>
        </div>
        <div className="container" style={{ marginTop: "15px" }}>
          <Tab.Container id="listado" defaultActiveKey="facturas">
            <Nav fill variant="tabs">
              <Nav.Item>
                <Nav.Link eventKey="facturas">Facturas</Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link eventKey="estado-de-cuenta">
                  Estado de Cuenta
                </Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link eventKey="movimientos">Movimientos</Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link eventKey="recibo">Recibo</Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link eventKey="pedidos">Pedidos</Nav.Link>
              </Nav.Item>
            </Nav>
            <Tab.Content>
              <Tab.Pane eventKey="facturas">
                <Facturas
                  estado={this.state}
                  handleActivoProductos={this.handleActivoProductos}
                  vendedor={this.props.idUsuario}
                  vendedorNombre={this.props.usuario}
                  estadoCuenta={() => this.estadoCuenta(cliente)}
                  empresa={this.props.empresa}
                  actualizarDatosCliente={() => this.actualizarDatosCliente()}
                  handleChangeBuscador={this.handleChangeBuscador}
                />
              </Tab.Pane>
              <Tab.Pane eventKey="estado-de-cuenta">
                <EstadoCuenta
                  estado={this.state}
                  vendedorNombre={this.props.usuario}
                  empresa={this.props.empresa}
                />
              </Tab.Pane>
              <Tab.Pane eventKey="movimientos">
                <Movimientos
                  estado={this.state}
                  documentos={this.state.movimientos}
                  cargando={this.state.cargandoMovimientos}
                  estadoCuenta={() => this.estadoCuenta(cliente)}
                  vendedor={this.props.usuario}
                  empresa={this.props.empresa}
                />
              </Tab.Pane>
              <Tab.Pane eventKey="recibo">
                <Recibos
                  estado={this.state}
                  handleActivoProductos={this.handleActivoProductos}
                  vendedor={this.props.idUsuario}
                  vendedorNombre={this.props.usuario}
                  estadoCuenta={() => this.estadoCuenta(cliente)}
                  empresa={this.props.empresa}
                />
              </Tab.Pane>
              <Tab.Pane eventKey="pedidos">
                <Pedidos
                  estado={this.state}
                  handleActivoProductos={this.handleActivoProductos}
                  vendedor={this.props.idUsuario}
                  vendedorNombre={this.props.usuario}
                  empresa={this.props.empresa}
                  actualizarDatosCliente={() => this.actualizarDatosCliente()}
                />
              </Tab.Pane>
            </Tab.Content>
          </Tab.Container>
        </div>
      </div>
    );
  }
}

export default VisitaCliente;
