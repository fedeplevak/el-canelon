import React, { Component } from "react";
import {
  Button,
  Form,
  Container,
  Row,
  Col,
  Spinner,
  ListGroup,
} from "react-bootstrap";
import { db } from "../firebaseConfig";

class ListadoAprontePedidos extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cargando: false,
      cargandoProductos: false,
      lugares: [],
      productosPedidos: [],
      productosTotales: [],
      desde: "",
      hasta: "",
      idLugar: "",
      nombreLugar: "",
      zona: "",
    };
  }

  componentDidMount() {
    this.hoy();
    this.traerLugares();
    this.traerProductos();
  }

  traerLugares = async () => {
    let snapLugares = await db.collection("lugares").get();
    let lugares = [];
    snapLugares.forEach((doc) => {
      lugares[doc.id] = doc.data();
    });
    this.setState({ lugares });
  };

  traerProductos = async () => {
    let snapProductos = await db.collection("productos").orderBy("orden").get();
    let productos = [];
    snapProductos.forEach((doc) => {
      productos[doc.id] = doc.data();
      productos[doc.id].cantidadTotal = 0;
    });
    this.setState({ productos });
  };

  hoy = () => {
    let hoy = new Date();

    let año = hoy.getFullYear();
    let mes =
      hoy.getMonth() + 1 > 9 ? hoy.getMonth() + 1 : `0${hoy.getMonth() + 1}`;
    let dia = hoy.getDate() > 9 ? hoy.getDate() : `0${hoy.getDate()}`;

    const fecha = `${año}-${mes}-${dia}`;
    this.setState({ fecha });
  };

  handleChange = (e) => {
    if (e.target.name === "desde") {
      this.setState({ desde: e.target.value }, () => {
        this.traerPedidos();
      });
    }
    if (e.target.name === "hasta") {
      this.setState({ hasta: e.target.value }, () => {
        this.traerPedidos();
      });
    }
    if (e.target.name === "idLugar") {
      let idLugar = e.target.value;
      let nombreLugar = e.target.selectedOptions[0].textContent;
      this.setState({ idLugar, nombreLugar }, () => {
        this.traerPedidos();
      });
    }
    if (e.target.name === "zona") {
      this.setState({ zona: e.target.value }, () => {
        this.traerPedidos();
      });
    } else {
      this.setState({ [e.target.name]: e.target.value });
    }
  };

  traerPedidos = () => {
    let { desde, hasta, idLugar, zona } = this.state;
    if (desde && hasta && idLugar) {
      this.setState({ cargandoProductos: true }, async () => {
        if (!zona) {
          let snap = await db
            .collection("pedidosProductos")
            .where("cabezera.fechaEntrega", ">=", desde)
            .where("cabezera.fechaEntrega", "<=", hasta)
            .where("cabezera.idLugar", "==", idLugar)
            .where("cabezera.pendienteEntregado", "==", false)
            .get();
          let productosPedidos = [];
          snap.forEach((doc) => {
            productosPedidos[doc.id] = doc.data();
          });
          this.setState({ productosPedidos }, () => {
            this.productosTotales();
          });
        } else {
          let snap = await db
            .collection("pedidosProductos")
            .where("cabezera.fechaEntrega", ">=", desde)
            .where("cabezera.fechaEntrega", "<=", hasta)
            .where("cabezera.idLugar", "==", idLugar)
            .where("cabezera.zona", "==", zona)
            .where("cabezera.pendienteEntregado", "==", false)
            .get();
          let productosPedidos = [];
          snap.forEach((doc) => {
            productosPedidos[doc.id] = doc.data();
          });
          this.setState({ productosPedidos }, () => {
            this.productosTotales();
          });
        }
      });
    }
  };

  productosTotales = () => {
    let { productos, productosPedidos } = this.state;
    let productosTotales = [];

    Object.keys(productos).forEach((id) => {
      productosTotales[id] = { ...productos[id] };
      productosTotales[id].obs = false;
    });

    Object.keys(productosTotales).forEach((id) => {
      Object.keys(productosPedidos).forEach((key) => {
        if (id === productosPedidos[key].idProducto) {
          productosTotales[id].cantidadTotal =
            productosTotales[id].cantidadTotal +
            parseFloat(productosPedidos[key].cantidad);
          if (productosPedidos[key].observaciones) {
            productosTotales[id].obs = true;
          }
        }
      });
    });
    Object.keys(productosTotales).forEach((id) => {
      if (productosTotales[id].cantidadTotal === 0) {
        delete productosTotales[id];
      }
    });
    this.setState({ productosTotales, cargandoProductos: false });
  };

  render() {
    const {
      lugares,
      productosPedidos,
      productosTotales,
      desde,
      hasta,
      idLugar,
      zona,
    } = this.state;
    return (
      <div style={{ width: "100%" }}>
        <Container>
          <h3 style={{ marginTop: "15px" }}>Productos para Aprontar</h3>
          <Row>
            <Col>
              <Form.Group>
                <Form.Label style={{ color: desde ? null : "red" }}>
                  Desde
                </Form.Label>
                <input
                  name="desde"
                  className="form-control text-center"
                  type="date"
                  onChange={(e) => this.handleChange(e)}
                  value={desde}
                />
              </Form.Group>
            </Col>
            <Col>
              <Form.Group>
                <Form.Label style={{ color: hasta ? null : "red" }}>
                  Hasta
                </Form.Label>
                <input
                  name="hasta"
                  className="form-control text-center"
                  type="date"
                  onChange={(e) => this.handleChange(e)}
                  value={hasta}
                />
              </Form.Group>
            </Col>
          </Row>
          <Row>
            <Col>
              <Form.Group>
                <Form.Label style={{ color: idLugar ? null : "red" }}>
                  Lugar:
                </Form.Label>
                <select
                  className="form-control"
                  onChange={(e) => this.handleChange(e)}
                  name="idLugar"
                  value={this.state.idLugar}
                  aria-label="Small"
                >
                  <option value="">Seleccionar Lugar...</option>
                  {Object.keys(lugares).map((id) => {
                    return <option value={id}>{lugares[id].nombre}</option>;
                  })}
                </select>
              </Form.Group>
            </Col>
          </Row>
          <Row>
            <Col>
              <Form.Group>
                <Form.Label style={{ color: zona ? null : "red" }}>
                  Reparto:
                </Form.Label>
                <select
                  className="form-control"
                  onChange={(e) => this.handleChange(e)}
                  name="zona"
                  value={this.state.zona}
                  aria-label="Small"
                >
                  <option value="">Todos...</option>
                  <option value="Montevideo">Montevideo</option>
                  <option value="La Costa">La Costa</option>
                </select>
              </Form.Group>
            </Col>
          </Row>

          {desde && hasta && idLugar ? (
            <Row>
              <Col>
                <Button
                  variant="success"
                  onClick={() => {
                    this.traerPedidos();
                  }}
                >
                  Buscar
                </Button>
              </Col>
            </Row>
          ) : null}

          <h3 style={{ marginTop: "15px" }}>Total de Productos</h3>
          <h3 style={{ marginTop: "15px" }}>{this.state.nombreVendedor}</h3>
          {this.state.cargandoProductos ? (
            <div
              style={{
                width: "100%",
                height: "200px",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Spinner animation="grow" variant="primary" />
            </div>
          ) : Object.keys(productosTotales).length > 0 ? (
            <div>
              <ListGroup variant="flush" style={{ marginTop: "15px" }}>
                <ListGroup.Item>
                  <Row>
                    <Col xs={4}>
                      <strong>Cantidad</strong>
                    </Col>
                    <Col xs={8}>
                      <strong>Producto</strong>
                    </Col>
                  </Row>
                </ListGroup.Item>
                {Object.keys(productosTotales).map((id) => {
                  return (
                    <ListGroup.Item
                      variant="success"
                      style={productosTotales[id].obs ? { color: "red" } : null}
                    >
                      <Row>
                        <Col xs={4}>
                          {productosTotales[id].cantidadTotal.toLocaleString(
                            "de-DE"
                          )}
                        </Col>
                        {productosTotales[id].obs ? (
                          <Col xs={8} style={{ textAlign: "left" }}>
                            {`${productosTotales[id].producto}*`}
                          </Col>
                        ) : (
                          <Col xs={8} style={{ textAlign: "left" }}>
                            {productosTotales[id].producto}
                          </Col>
                        )}
                      </Row>
                    </ListGroup.Item>
                  );
                })}
              </ListGroup>

              <h3 style={{ marginTop: "15px" }}>Productos /Pedido</h3>
              <ListGroup variant="flush" style={{ marginTop: "15px" }}>
                <ListGroup.Item
                  style={{ paddingLeft: "0px", paddingRight: "0px" }}
                >
                  <Row>
                    <Col xs={2}>
                      <strong>Cantidad</strong>
                    </Col>
                    <Col xs={3}>
                      <strong>Producto</strong>
                    </Col>
                    <Col xs={3}>
                      <strong>Cliente</strong>
                    </Col>
                    <Col xs={4}>
                      <strong>Observaciones</strong>
                    </Col>
                  </Row>
                </ListGroup.Item>
                {Object.keys(productosPedidos).map((id) => {
                  return (
                    <ListGroup.Item
                      variant={
                        productosPedidos[id].observaciones ? "danger" : null
                      }
                    >
                      <Row>
                        <Col xs={2}>
                          {productosPedidos[id].cantidad.toLocaleString(
                            "de-DE"
                          )}
                        </Col>
                        <Col xs={3} style={{ textAlign: "left" }}>
                          {productosPedidos[id].producto}
                        </Col>
                        <Col xs={3}>{productosPedidos[id].cabezera.nombre}</Col>
                        {productosPedidos[id].observaciones ? (
                          <Col xs={4}>
                            {`*${productosPedidos[id].observaciones}`}
                          </Col>
                        ) : null}
                      </Row>
                    </ListGroup.Item>
                  );
                })}
              </ListGroup>
            </div>
          ) : (
            <ListGroup variant="flush" style={{ marginTop: "15px" }}>
              <p>No se encontraron resultados</p>
            </ListGroup>
          )}
        </Container>
      </div>
    );
  }
}

export default ListadoAprontePedidos;
