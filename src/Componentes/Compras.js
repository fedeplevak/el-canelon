import React, { Component } from "react";
import { db } from "../firebaseConfig";
import {
  Table,
  Button,
  Form,
  Row,
  Col,
  Tab,
  Nav,
  ToggleButton,
  ToggleButtonGroup,
  Spinner,
} from "react-bootstrap";

class Compras extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fecha: "",
      documento: "",
      compraPago: "compra",
      documentoClase: "",
      nDocumento: "",
      rut: "",
      totalAdeudado: "",
      formaPago: "",
      cantidadTotal: 0,
      subtotal: 0,
      iva: 0,
      total: 0,
      estadoCuenta: "",
      productosFinal: [],
      tipoDocumento: "",
      cantidad: "",
      producto: "",
      precio: "",
      precioIva: "",
      direccion: "compras",
      cargandoConfirmar: false,
      percepcion: "",
      banderaPercepcion: true,
      lugar: "",
      lugarNombre: ""
    };
  }

  componentDidMount() {
    this.hoy();
  }

  hoy = () => {
    let hoy = new Date();

    let año = hoy.getFullYear();
    let mes =
      hoy.getMonth() + 1 > 9 ? hoy.getMonth() + 1 : `0${hoy.getMonth() + 1}`;
    let dia = hoy.getDate() > 9 ? hoy.getDate() : `0${hoy.getDate()}`;

    const fecha = `${año}-${mes}-${dia}`;
    this.setState({ fecha });
  };

  handleChange = (e) => {
    if (e.target.name === "documento") {
      let documentoClase = "";
      let aOb = "";
      let estadoCuenta = "";
      let tipoDocumento = "";
      if (e.target.value === "Factura Contado") {
        documentoClase = "table-danger";
        aOb = true;
        estadoCuenta = false;
        tipoDocumento = "facturaContado";
      }
      if (e.target.value === "Boleta Contado") {
        documentoClase = "table-warning";
        aOb = false;
        estadoCuenta = false;
        tipoDocumento = "boleta";
      }
      if (e.target.value === "Factura Crédito") {
        documentoClase = "table-active";
        aOb = true;
        estadoCuenta = true;
        tipoDocumento = "facturaCredito";
      }
      if (e.target.value === "Boleta Crédito") {
        documentoClase = "table-info";
        aOb = false;
        estadoCuenta = true;
        tipoDocumento = "boleta";
      }
      this.setState(
        {
          [e.target.name]: e.target.value,
          documentoClase,
          aOb,
          estadoCuenta,
          tipoDocumento,
        },
        () => this.comprobarCompleto()
      );
    }
    if (e.target.name === "nDocumento") {
      let num = parseFloat(e.target.value);
      this.setState({ [e.target.name]: num }, () => this.comprobarCompleto());
    }
    if (e.target.name === "producto") {
      let pro = this.props.estado.productos;
      let precio = "";
      let id = e.target.value;
      let precioIva = "";
      if (id && pro[id].precio) {
        precio = parseFloat(pro[id].precio);
        precioIva = parseFloat(precio + (precio * pro[id].iva) / 100);
      }
      this.setState({ producto: id, precio, precioIva }, () =>
        this.comprobarCompleto()
      );
    }
    if (e.target.name === "percepcion") {
      let { subtotal, iva } = this.state;
      let percepcion = e.target.value;
      if (percepcion) {
        percepcion = parseFloat(percepcion);
      } else {
        percepcion = 0;
      }
      let total = subtotal + iva + percepcion;
      this.setState({ percepcion, total }, () => this.comprobarCompleto());
    }
    if (e.target.name === "lugar") {
      let lugarNombre = e.target.selectedOptions[0].textContent;
      this.setState({ 
        [e.target.name]: e.target.value,
        lugarNombre
       }, () => this.comprobarCompleto());
    }
    
    else {
      this.setState(
        {
          [e.target.name]: e.target.value,
        },
        () => this.comprobarCompleto()
      );
    }
  };

  comprobarCompleto = () => {
    let {
      fecha,
      documento,
      nDocumento,
      formaPago,
      cantidadTotal,
      productosFinal,
      lugar
    } = this.state;
    let { proveedor } = this.props.estado;
    console.log(
      `fecha: ${fecha.length} documento: ${documento.length} nDocumento ${nDocumento} proveedor: ${proveedor.length}, formaPago: ${formaPago.length}, cantidadTotal: ${cantidadTotal}`
    );
    if (
      fecha.length > 0 &&
      documento.length > 0 &&
      nDocumento > 0 &&
      proveedor.length > 0 &&
      lugar.length > 0 &&
      Object.keys(productosFinal).length > 0
    ) {
      if (documento === "Factura Contado" || documento === "Boleta Contado") {
        if (formaPago.length > 0) {
          this.setState({ direccion: "home" });
        } else {
          this.setState({ direccion: "compras" });
        }
      } else {
        this.setState({ direccion: "home" });
      }
    } else {
      this.setState({ direccion: "compras" });
    }
  };

  agregarProducto = () => {
    let proFinal = this.state.productosFinal;
    let pro = this.props.estado.productos;
    let { cantidad, producto, precio, percepcion } = this.state;
    if (percepcion) {
      percepcion = parseFloat(percepcion);
    } else {
      percepcion = 0;
    }
    pro[producto].cantidad = parseFloat(cantidad);
    pro[producto].precio = parseFloat(precio);
    pro[producto].importe = cantidad * precio;
    pro[producto].idProducto = producto;
    let product = { ...pro[producto] };
    let total = 0;
    let iva = 0;
    let subtotal = 0;
    let cantidadTotal = 0;
    proFinal.push(product);
    Object.keys(proFinal).forEach((id) => {
      let imp = parseFloat("0." + proFinal[id].iva);
      subtotal = subtotal + proFinal[id].importe;
      iva = iva + proFinal[id].importe * imp;
      total = subtotal + iva + percepcion;
      cantidadTotal = cantidadTotal + parseFloat(proFinal[id].cantidad);
    });
    this.setState(
      {
        productosFinal: proFinal,
        total,
        iva,
        subtotal,
        cantidadTotal,
        cantidad: "",
        producto: "",
        precio: "",
        precioIva: "",
      },
      () => {
        this.comprobarCompleto();
      }
    );
  };

  eliminarLinea = (id) => {
    let proFinal = this.state.productosFinal;
    let { percepcion } = this.state;
    if (percepcion) {
      percepcion = parseFloat(percepcion);
    } else {
      percepcion = 0;
    }
    let total = 0;
    let iva = 0;
    let subtotal = 0;
    let cantidadTotal = 0;
    delete proFinal[id];
    Object.keys(proFinal).forEach((id) => {
      let imp = parseFloat("0." + proFinal[id].iva);
      subtotal = subtotal + proFinal[id].importe;
      iva = iva + proFinal[id].importe * imp;
      total = subtotal + iva + percepcion;
      cantidadTotal = cantidadTotal + parseFloat(proFinal[id].cantidad);
    });
    this.setState(
      {
        productosFinal: proFinal,
        total,
        iva,
        subtotal,
        cantidadTotal,
      },
      () => {
        this.comprobarCompleto();
      }
    );
  };

  confirmar = () => {
    let {
      fecha,
      documento,
      compraPago,
      nDocumento,
      formaPago,
      total,
      iva,
      subtotal,
      percepcion,
      productosFinal,
      aOb,
      estadoCuenta,
      lugar,
      lugarNombre
    } = this.state;

    let { proveedor, proveedorNombre, proveedorDatos } = this.props.estado;

    if (documento === "Factura Crédito" || documento === "Boleta Crédito") {
      formaPago = "";
    }

    let cabezera = {
      ...proveedorDatos,
      proveedor: proveedor,
      proveedorNombre: proveedorNombre,
      fecha: fecha,
      documento: documento,
      compraPago: compraPago,
      nDocumento: nDocumento,
      formaPago: formaPago,
      total: total,
      iva: iva,
      subtotal: subtotal,
      percepcion: percepcion,
      anulado: false,
      aOb: aOb,
      estadoCuenta: estadoCuenta,
      lugar: lugar,
      lugarNombre: lugarNombre
    };
    this.setState({ cargandoConfirmar: true }, async () => {
      if (estadoCuenta) {
        let deudaActual = 0;
        db.collection("proveedores")
          .doc(proveedor)
          .get()
          .then((doc) => {
            if (doc.data().deudaActual) {
              deudaActual = parseFloat(doc.data().deudaActual);
            }
          })
          .then(() => {
            deudaActual = deudaActual + total;
            db.collection("proveedores")
              .doc(proveedor)
              .update({
                deudaActual: deudaActual,
              })
              .then(() => console.log("deuda corregida"))
              .catch((error) => console.log(error));
          })
          .catch((error) => console.log(error));
      }

      let snapStock = await db.collection("lugares").doc(lugar).collection("productos").get();
      let stock = [];
      snapStock.forEach(doc => {
        stock[doc.id] = doc.data();
      });

      Object.keys(productosFinal).forEach((id) => {
        productosFinal[id].stock = parseFloat(productosFinal[id].cantidad); 
        let pro = { ...productosFinal[id] };
        let idProducto = productosFinal[id].idProducto;
        db.collection("proveedores")
          .doc(proveedor)
          .collection("listaPrecios")
          .doc(idProducto)
          .set(pro)
          .then(() => console.log("precio actualizado"))
          .catch((error) => console.log(error));

          Object.keys(stock).forEach(key => {
            if(key === idProducto) {
              productosFinal[id].stock = parseFloat(stock[key].stock) + parseFloat(productosFinal[id].cantidad);
            }
          })
      });

      Object.keys(productosFinal).forEach(id => {
        let idProducto = productosFinal[id].idProducto;
        db.collection("lugares").doc(lugar).collection("productos")
        .doc(idProducto).set({
          ...productosFinal[id]
        })
        .then(()=> console.log("producto stock actualizado"))
        .catch(error => console.log(error));
      })

      db.collection("comprasCabezera")
        .add(cabezera)
        .then((ref) => {
          let idCabezera = ref.id;
          Object.keys(productosFinal).forEach((id) => {
            let producto = productosFinal[id];
            db.collection("comprasProductos")
              .add({
                idCabezera: idCabezera,
                ...cabezera,
                ...producto,
              })
              .then(() => {
                this.props.estadoCuenta();
                console.log("linea subida con éxito");
              })
              .catch((error) => console.log(error));
          });
        })
        .then(() => {
          this.setState({
            documento: "",
            documentoClase: "",
            nDocumento: "",
            formaPago: "",
            cantidadTotal: 0,
            subtotal: 0,
            iva: 0,
            total: 0,
            percepcion: "",
            estadoCuenta: "",
            productosFinal: [],
            boleta: "",
            facturaContado: "",
            facturaCredito: "",
            tipoDocumento: "",
            cantidad: "",
            producto: "",
            precio: "",
            direccion: "compras",
            cargandoConfirmar: false,
          });
        })
        .catch((error) => console.log(error));
    });
  };

  handlePercepcion = () => {
    let { subtotal, iva, percepcion, banderaPercepcion } = this.state;
    if (percepcion) {
      percepcion = parseFloat(percepcion);
    } else {
      percepcion = 0;
    }
    if (banderaPercepcion) {
      this.setState({ banderaPercepcion: false });
    } else {
      let total = subtotal + iva + percepcion;
      this.setState({ total, banderaPercepcion: true });
    }
  };

  render() {
    const {
      documentoClase,
      fecha,
      documento,
      nDocumento,
      formaPago,
      cantidad,
      producto,
      precio,
      precioIva,
      cantidadTotal,
      subtotal,
      iva,
      percepcion,
      total,
      productosFinal,
      lugar
    } = this.state;

    const {
      productos,
      cargandoProductos,
      proveedorNombre,
      proveedor,
      rut,
      lugares
    } = this.props.estado;

    return this.state.cargando ? (
      <div
        style={{
          width: "100%",
          height: "200px",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Spinner animation="grow" variant="primary" />
      </div>
    ) : (
      <div className={`container ${documentoClase}`}>
        <h1 style={{ padding: "25px" }}>Compras</h1>
        <div className="container" style={{ marginTop: "15px" }}>
          <Tab.Container id="documento">
            <Nav variant="tabs" defaultActiveKey="cabezera">
              <Nav.Item>
                <Nav.Link eventKey="cabezera">Cabezera</Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link eventKey="productos">Productos</Nav.Link>
              </Nav.Item>
            </Nav>
            <Tab.Content>
              <Tab.Pane eventKey="cabezera">
                <div>
                  <Form>
                    <Form.Group>
                      <Form.Label
                        style={
                          !fecha ? { color: "red", fontWeight: "bold" } : null
                        }
                      >
                        Fecha
                      </Form.Label>
                      <input
                        name="fecha"
                        className="form-control text-center"
                        type="date"
                        placeholder="Fecha"
                        onChange={(e) => this.handleChange(e)}
                        value={this.state.fecha}
                        disabled={!this.props.vendedorNombre.permisosVendedor}
                      />
                    </Form.Group>
                    <Form.Group>
                      <Form.Label
                        style={
                          !documento
                            ? { color: "red", fontWeight: "bold" }
                            : null
                        }
                      >
                        Documento
                      </Form.Label>
                      <select
                        className="form-control"
                        onChange={(e) => this.handleChange(e)}
                        name="documento"
                        value={this.state.documento}
                        aria-label="Small"
                      >
                        <option value="">Seleccione documento...</option>
                        <option value="Factura Contado">Factura Contado</option>
                        <option value="Boleta Contado">Boleta Contado</option>
                        <option value="Factura Crédito">Factura Crédito</option>
                        <option value="Boleta Crédito">Boleta Crédito</option>
                      </select>
                    </Form.Group>
                    <Form.Group>
                      <Form.Label
                        style={
                          !nDocumento
                            ? { color: "red", fontWeight: "bold" }
                            : null
                        }
                      >
                        Nº
                      </Form.Label>
                      <input
                        name="nDocumento"
                        className="form-control"
                        type="number"
                        placeholder="Nº Documento"
                        onChange={(e) => this.handleChange(e)}
                        onBlur={this.comprobarCompleto}
                        value={this.state.nDocumento}
                      />
                    </Form.Group>
                    {this.state.documento === "Factura Contado" ||
                    this.state.documento === "Boleta Contado" ? (
                      <Form.Group>
                        <Form.Label
                          style={
                            !formaPago
                              ? { color: "red", fontWeight: "bold" }
                              : null
                          }
                        >
                          Forma de pago
                        </Form.Label>
                        <select
                          className="form-control"
                          onChange={(e) => this.handleChange(e)}
                          name="formaPago"
                          value={this.state.formaPago}
                          aria-label="Small"
                        >
                          <option value="">Seleccione forma de Pago...</option>
                          <option value="Efectivo">Efectivo</option>
                          <option value="Cheque">Cheque</option>
                          <option value="Transferencia">Transferencia</option>
                          <option value="otros">otros...</option>
                        </select>
                      </Form.Group>
                    ) : null}
                    <Form.Group>
                        <Form.Label
                          style={
                            !lugar
                              ? { color: "red", fontWeight: "bold" }
                              : null
                          }
                        >
                          Lugar
                        </Form.Label>
                        <select
                          className="form-control"
                          onChange={(e) => this.handleChange(e)}
                          name="lugar"
                          value={this.state.lugar}
                          aria-label="Small"
                        >
                          <option value="">Seleccione lugar...</option>
                          {Object.keys(lugares).map(id => {
                            return (
                            <option value={id}>{lugares[id].nombre}</option>
                            )
                          })}
                        </select>
                      </Form.Group>
                  </Form>
                </div>
              </Tab.Pane>
              <Tab.Pane eventKey="productos">
                {cargandoProductos ? (
                  <div
                    style={{
                      width: "100%",
                      height: "200px",
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <Spinner animation="grow" variant="primary" />
                  </div>
                ) : (
                  <div>
                    <Row style={{ marginBottom: "15px" }}>
                      <Col>
                        <h3 style={{ marginTop: "28px" }}>Productos</h3>
                      </Col>
                      <Col>
                        {this.state.cargandoProductos ? (
                          <Button
                            variant="outline-primary"
                            disabled
                            style={{ height: "42px", marginTop: "29px" }}
                          >
                            <Spinner
                              as="span"
                              animation="grow"
                              size="sm"
                              role="status"
                              aria-hidden="true"
                            />
                            Cargando...
                          </Button>
                        ) : (
                          <ToggleButtonGroup
                            style={{ height: "42px", marginTop: "29px" }}
                            type="radio"
                            name="productosVer"
                            value={this.props.estado.productosVer}
                            onChange={(val) =>
                              this.props.handleActivoProductos(val)
                            }
                          >
                            <ToggleButton
                              variant="outline-success"
                              name="radio"
                              value="activos"
                            >
                              Activos
                            </ToggleButton>
                            <ToggleButton
                              variant="outline-danger"
                              name="radio"
                              value="todos"
                            >
                              Todos
                            </ToggleButton>
                          </ToggleButtonGroup>
                        )}
                      </Col>
                    </Row>
                    <Row>
                      <Form style={{ width: "100%" }}>
                        <Form.Group>
                          <Form.Label>Cantidad</Form.Label>
                          <input
                            name="cantidad"
                            className="form-control text-center"
                            type="number"
                            placeholder="Cantidad"
                            onChange={(e) => this.handleChange(e)}
                            value={this.state.cantidad}
                          />
                        </Form.Group>
                        <Form.Group>
                          <Form.Label>Producto</Form.Label>
                          <select
                            className="form-control"
                            onChange={(e) => this.handleChange(e)}
                            name="producto"
                            value={this.state.producto}
                            aria-label="Small"
                          >
                            <option value="">Seleccione producto...</option>
                            {Object.keys(productos).map((id) => {
                              return (
                                <option value={id}>
                                  {productos[id].producto}
                                </option>
                              );
                            })}
                          </select>
                        </Form.Group>
                        <Form.Group>
                          <Form.Label>Precio</Form.Label>
                          <p>{precioIva.toLocaleString("de-DE")}</p>
                          <input
                            name="precio"
                            className="form-control"
                            type="number"
                            placeholder="Precio"
                            onChange={(e) => this.handleChange(e)}
                            value={this.state.precio}
                          />
                        </Form.Group>
                      </Form>
                    </Row>
                    <Row>
                      {cantidad !== 0 && producto.length > 0 && precio !== 0 ? (
                        <Button
                          variant="outline-success"
                          style={{ marginBottom: "15px" }}
                          onClick={this.agregarProducto}
                        >
                          Agregar
                        </Button>
                      ) : null}
                    </Row>
                  </div>
                )}
              </Tab.Pane>
            </Tab.Content>
          </Tab.Container>
          <Row style={{ textAlign: "left" }}>
            <Col>
              <p style={!fecha ? { color: "red", fontWeight: "bold" } : null}>
                Fecha: <strong>{fecha}</strong>
              </p>
            </Col>
          </Row>
          <Row style={{ textAlign: "left" }}>
            <Col>
              <p
                style={!proveedor ? { color: "red", fontWeight: "bold" } : null}
              >
                Proveedor: <strong>{proveedorNombre}</strong>
              </p>
            </Col>
          </Row>
          <Row style={{ textAlign: "left" }}>
            <Col>
              <p>
                Rut: <strong>{rut}</strong>
              </p>
            </Col>
          </Row>
          <Row style={{ textAlign: "left" }}>
            <Col>
              <p
                style={!documento ? { color: "red", fontWeight: "bold" } : null}
              >
                Documento: <strong>{documento}</strong>
              </p>
            </Col>
            <Col style={{ textAlign: "right" }}>
              <p
                style={
                  !nDocumento ? { color: "red", fontWeight: "bold" } : null
                }
              >
                Nº: <strong>{nDocumento}</strong>
              </p>
            </Col>
          </Row>
          {this.state.documento === "Factura Contado" ||
          this.state.documento === "Boleta Contado" ? (
            <Row style={{ textAlign: "left" }}>
              <Col>
                <p
                  style={
                    !formaPago ? { color: "red", fontWeight: "bold" } : null
                  }
                >
                  Forma de Pago: <strong>{formaPago}</strong>
                </p>
              </Col>
            </Row>
          ) : null}
        </div>
        <div style={{ width: "100%", overflow: "scroll" }}>
          <Table
            striped
            bordered
            condensed
            hover
            responsive
            style={
              cantidadTotal === 0 ? { color: "red", fontWeight: "bold" } : null
            }
          >
            <thead>
              <tr>
                <th>Cantidad</th>
                <th>Producto</th>
                <th>Precio</th>
                <th>Importe</th>
              </tr>
            </thead>
            <tbody>
              {Object.keys(productosFinal).map((id) => {
                return (
                  <tr>
                    <td>
                      {productosFinal[id].cantidad.toLocaleString("de-DE")}
                    </td>
                    <td>{productosFinal[id].producto}</td>
                    <td>{productosFinal[id].precio.toLocaleString("de-DE")}</td>
                    <td>
                      {productosFinal[id].importe.toLocaleString("de-DE")}
                    </td>
                    <td>
                      <Button
                        variant="outline-danger"
                        onClick={() => this.eliminarLinea(id)}
                      >
                        X
                      </Button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
            <tbody>
              {this.state.documento === "Factura Contado" ||
              this.state.documento === "Factura Crédito" ? (
                <tr>
                  <td />
                  <td />
                  <td>Subtotal</td>
                  <td>{subtotal.toLocaleString("de-DE")}</td>
                </tr>
              ) : null}
            </tbody>
            <tbody>
              {this.state.documento === "Factura Contado" ||
              this.state.documento === "Factura Crédito" ? (
                <tr>
                  <td />
                  <td />
                  <td>IVA</td>
                  <td>{iva.toLocaleString("de-DE")}</td>
                </tr>
              ) : null}
            </tbody>
            <tbody>
              <tr>
                <td />
                <td />
                <td>Percepción</td>
                {this.state.banderaPercepcion ? (
                  <td onClick={this.handlePercepcion}>
                    {percepcion.toLocaleString("de-DE")}
                  </td>
                ) : (
                  <input
                    style={{ marginTop: "6px" }}
                    name="percepcion"
                    className="form-control"
                    type="number"
                    placeholder="Percepcion"
                    onBlur={this.handlePercepcion}
                    onChange={(e) => this.handleChange(e)}
                    value={percepcion}
                  />
                )}
              </tr>
            </tbody>
            <tbody>
              <tr>
                <td>
                  {this.state.direccion === "home" ? (
                    this.state.cargandoConfirmar ? (
                      <Button
                        variant="outline-success"
                        disabled
                        style={{ height: "42px", marginTop: "29px" }}
                      >
                        <Spinner
                          as="span"
                          animation="grow"
                          size="sm"
                          role="status"
                          aria-hidden="true"
                        />
                        Cargando...
                      </Button>
                    ) : (
                      <Button
                        variant="outline-success"
                        onClick={this.confirmar}
                      >
                        Confirmar
                      </Button>
                    )
                  ) : null}
                </td>
                <td />
                <td>
                  <strong>Total</strong>
                </td>
                <td>
                  <strong>{total.toLocaleString("de-DE")}</strong>
                </td>
              </tr>
            </tbody>
          </Table>
        </div>
      </div>
    );
  }
}

export default Compras;
