import firebase from "firebase";

const config = {
  apiKey: "AIzaSyDaqxo9ZdIODv80ebqboTuDfkYkSAxzEIg",
  authDomain: "elcanelon-d997d.firebaseapp.com",
  projectId: "elcanelon-d997d",
  storageBucket: "elcanelon-d997d.appspot.com",
  messagingSenderId: "891137499203",
  appId: "1:891137499203:web:77340fb103e4193b7b74f6",
  measurementId: "G-W55FTWPTJ5"
};

const otherConfig = {
  apiKey: "AIzaSyCbDmJC6deSosAkT3RMhSFTpj7M7O6YZlQ",
  authDomain: "crud-b8931.firebaseapp.com",
  databaseURL: "https://crud-b8931.firebaseio.com",
  projectId: "crud-b8931",
  storageBucket: "crud-b8931.appspot.com",
  messagingSenderId: "32622371777",
  appId: "1:32622371777:web:342cad1375956edc"
};

const fire = firebase.initializeApp(config);
const fireLago = firebase.initializeApp(otherConfig, "other");
const db = firebase.firestore();
const dbLago = fireLago.firestore();
db.settings({
  timestampsInSnapshots: true
});

export { fire, db, fireLago, dbLago };
